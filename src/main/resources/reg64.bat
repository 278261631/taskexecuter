@echo off
title  注册WSC脚本部件
 
echo. *****************************************
echo.  支持x64系统（请以管理员身份运行）
echo.  Write By Mingyue
echo.  E-mail: mingyue@brightmoon.cn
echo. *****************************************
 
set /p dest="请输入WSC文件夹路径:"
 
:do
echo.
echo.***************请选择操作方式********************
echo.--1、注册全部WSC文件
echo.--2、通过WSC文件名注册文件
echo.--3、卸载WSC文件
echo.--#、退出
 
set /p type="输入:"
if "%type%"=="1" goto regall
if "%type%"=="2" goto redo
if "%type%"=="#" goto end
goto end
 
:uninstall
echo.
set /p file="输入需要卸载的WSC名称（输入#时退出）:"
if "%file%"=="#" goto end
echo 卸载:%dest%\%file%
C:\Windows\System32\regsvr32 /u:"%dest%\%file%" C:\Windows\System32\scrobj.dll
goto do
 
:regall
echo.
echo.*************请选择是否显示消息框****************
echo.--1、显示
echo.--2、不显示
echo.--3、返回
set /p show="输入:"
if "%show%"=="1" goto regall1
if "%show%"=="2" goto regall2
goto do
 
:regall1
echo.
for /r "%dest%" %%f in (*.wsc) do C:\Windows\SysWOW64\regsvr32 /i:"%%f" C:\Windows\System32\scrobj.dll
goto end
 
:regall2
echo.
for /r "%dest%" %%f in (*.wsc) do C:\Windows\SysWOW64\regsvr32 /s:"%%f"
goto end
 
:redo
echo.
set /p file="输入需要注册的WSC名称（输入#时退出）:"
if "%file%"=="#" goto end
echo 注册:%dest%\%file%
C:\Windows\SysWOW64\regsvr32 /i:"%dest%\%file%" C:\Windows\SysWOW64\scrobj.dll
goto redo
 
:end
echo 退出...
 
pause