package javascope;


public class TestConvert {

	public static void main(String[] args) {
		
		astroTime as=astroTime.getInstance();
		convertTrig ct=new convertTrig("test");
		as.calcSidT();
		System.out.println(as.sidT.hr);
		System.out.println(as.sidT.stringHMS);
		System.out.println(as.sidT.rad);
		System.out.println(as.JD);
//		ct.p.ra.calcRad(); 
//		ct.p.dec.calcRad();
//		ct.p.getRaCorrectedForNutationAnnualAberration();
//		ct.test();
        ct.p.ra.hr=5;
        ct.p.ra.min=35;
        ct.p.ra.sec=16;
        ct.p.ra.sign='+';
        ct.p.dec.deg=5;
        ct.p.dec.min=23;
        ct.p.dec.sec=23;
        ct.p.dec.sign='-';
        ct.p.ra.calcRad();
        ct.p.dec.calcRad();
        ct.p.showCoord();
        ct.p.showCoordDeg();
        System.out.println(ct.p.ra.rad);
        System.out.println(ct.p.dec.rad);
//		ct.getAltaz_java();

	    Double lat=43.4710372,longt=87.1775888;
		
	  	astroTime.getInstance().sidT.calcRad();
    	double local_lst=astroTime.getInstance().sidT.rad;
        double longitude_time = degrees_to_hours(longt);
        System.out.println("===  gmdt  "+astroTime.getInstance().sidT.rad/(units.HR_TO_RAD)+"---longitude_time---" + longitude_time);
	    local_lst = astroTime.getInstance().sidT.rad/(units.HR_TO_RAD) + longitude_time;
	    local_lst %= 24;

		System.out.println("----local_lst---" + local_lst);    
    	
//	    double ha = (angles.hours_to_radians(lst) - right_ascension)
		System.out.println("Math.toRadians(local_lst)    "+Math.toRadians(local_lst)+"   ra    "+ct.p.ra.rad);
		double ha = (Math.toRadians(hours_to_degrees(local_lst))-ct.p.ra.rad);
	    ha %= 2 * Math.PI;
	    System.out.println("----ha  =  "+ha);
//	    slat = sin(radians(latitude))
//	    clat = cos(radians(latitude))
	    
	    double slat = Math.sin(Math.toRadians(lat));
	    double clat = Math.cos(Math.toRadians(lat));
//	    sha = sin(ha)
//	    cha = cos(ha)
	    double sha = Math.sin(ha);
	    double cha = Math.cos(ha);
	    double sdec = Math.sin(ct.p.dec.rad);
	    double cdec = Math.cos(ct.p.dec.rad);
//    	
	    
	    double altitude = Math.asin((sdec * slat) + (cdec * clat * cha));
//	     System.out.println( "高度 in radians   " + altitude);
	     System.out.println( "高度 in degrees   " + Math.toDegrees(altitude));
	    double Azimuth = Math.acos((sdec - (slat * Math.sin(altitude))) / (clat * Math.cos(altitude)));
	    if (sha > 0){
	    	Azimuth = 2 * Math.PI - Azimuth;	    	
	    }
//	     System.out.println("方位 in radians    " + Azimuth);
	     System.out.println("方位 in degrees    " + Math.toDegrees(Azimuth));
	     altitude = ((Math.PI / 2. - altitude)+Math.PI)%(2*Math.PI)-Math.PI;
	    Azimuth = ((Math.PI / 2. - Azimuth)+Math.PI)%(2*Math.PI)-Math.PI;
	    
	    System.out.println(altitude);
	    System.out.println(Azimuth);

	}
	
	static double degrees_to_hours(double degree){
		return degree/15;
	}
	
	static double hours_to_degrees(double degree){
		return degree*15;
	}
}
