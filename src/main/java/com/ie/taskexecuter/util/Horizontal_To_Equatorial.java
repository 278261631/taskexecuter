package com.ie.taskexecuter.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class  Horizontal_To_Equatorial {

	public static double[] Convert_H2E(double altitude_deg,double Azimuth_deg,double lat_deg,double longt_deg,Calendar calendar) {
//		double lst = clock.gps_to_lst(time.time(), 87.1775888);
//		double	altitudeInDeg=40;
//		double	azimuthIndeg_east=175;
//		double	azimuthIndeg_west=185;
		double altitude_rad=Math.toRadians(altitude_deg);
		double Azimuth_rad=Math.toRadians(Azimuth_deg);
		double longt_rad=Math.toRadians(longt_deg);
		double lat_rad=Math.toRadians(lat_deg);
//	    ha, dec = horizontal_to_hadec(latitude, altitude, Azimuth)
//	    		#     print repr(ha)+","+repr(dec);
//	    		    ra = ha_to_ra(ha, lst)
		
	  	astroTime astime=new astroTime();
	  	double local_lst = astime.calc_lst_local(longt_deg, calendar);
//		System.out.println(">>>>local_lst---" + local_lst);   
	  	double slat = Math.sin(lat_rad);
	  	double clat = Math.cos(lat_rad);
	  	double sazi = Math.sin(Azimuth_rad);
	  	double cazi = Math.cos(Azimuth_rad);
	  	double salt = Math.sin(altitude_rad);
	  	double calt = Math.cos(altitude_rad);
//	  	System.out.println("lat -----"+lat_rad+"  --  ");
//	  	System.out.println("altitude     "+altitude_rad);
	  	
//	    System.out.println( "salt ------"+(salt));
//	    System.out.println( "slat ------"+(slat));
//	    System.out.println( "calt ------"+(calt));
//	    System.out.println( "clat ------"+(clat));
//	    System.out.println( "clat ------"+(cazi));
	  	
	    double dec = Math.asin((salt * slat) + (calt * clat * cazi));
//	    System.out.println("dec----------"+dec);
//	    # Round to prevent value beyond allowed range for arccos.
	    double cha = (salt - (slat * Math.sin(dec))) / (clat * Math.cos(dec));
	    double ha = Math.acos(cha);
//	    System.out.println("Cha     "+cha);
//	    System.out.println("ha     "+ha);
	    if (sazi > 0){
	        ha = 2 * Math.PI - ha;
	    }
//	    System.out.println("ha  >>>> "+ha);
	    double ra = Math.toRadians((hours_to_degrees(local_lst))) - ha;
//	    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" +Math.toRadians(hours_to_degrees(local_lst)) + ">>>>>>>>>>"+ ha);
	    ra %= 2 * Math.PI;
//	  	System.out.println(" ++++ ra       "+ra + "  dec         " +dec+"   local_lst   " +local_lst);
//	  	System.out.println(Math.toDegrees(ra));
//	  	System.out.println(degrees_to_hours(Math.toDegrees(ra)));
//	  	System.out.println(Math.toDegrees(dec));
	  	
		hmsm hmsm_ra=new hmsm(ra);
		dms dms_dec=new  dms(dec);
//		System.out.println(hmsm_ra.getStringHMS());
//		System.out.println(dms_dec.getStringDMS());
	  	
//		double ha = (Math.toRadians(hours_to_degrees(local_lst))-hmsm_ra.rad);
//	    ha %= 2 * Math.PI;
////	    System.out.println("----ha  =  "+ha);
//	    
//	    double slat = Math.sin(Math.toRadians(lat));
//	    double clat = Math.cos(Math.toRadians(lat));
//	    double sha = Math.sin(ha);
//	    double cha = Math.cos(ha);
//	    double sdec = Math.sin(dms_dec.rad);
//	    double cdec = Math.cos(dms_dec.rad);
////    	
//	    
//	    double altitude = Math.asin((sdec * slat) + (cdec * clat * cha));
////	     System.out.println( "ALT  in degrees   " + Math.toDegrees(altitude));
//	    double Azimuth = Math.acos((sdec - (slat * Math.sin(altitude))) / (clat * Math.cos(altitude)));
//	    if (sha > 0){
//	    	Azimuth = 2 * Math.PI - Azimuth;	    	
//	    }
////	     System.out.println("AZ in degrees    " + Math.toDegrees(Azimuth));
////	     altitude = ((Math.PI / 2. - altitude)+Math.PI)%(2*Math.PI)-Math.PI;
////	    Azimuth = ((Math.PI / 2. - Azimuth)+Math.PI)%(2*Math.PI)-Math.PI;
//	    
////	    System.out.println(altitude);
////	    System.out.println(Azimuth);
	    
	    return new double[]{ra,dec};
	}
	
	public static double degrees_to_hours(double degree){
		return degree/15.;
	}
	
	public static double hours_to_degrees(double degree){
		return degree*15.;
	}
}
