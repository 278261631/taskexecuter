package com.ie.taskexecuter.util;

public class AcpStatus {

    boolean acpRunning;

    public boolean getAcpRunning() {
        return acpRunning;
    }

    public void setAcpRunning(boolean acpRunning) {
        this.acpRunning = acpRunning;
    }

    String eqp_status      ;
    String s_sm_obsStat    ;
    String s_sm_camStat    ;
    String s_sm_guideStat  ;
    String s_sm_plnTgt_acp6 ;

    //acp 5
//    String s_sm_actTgt_acp5;
            //=resultMap.get("s_sm_actTgt");
    //acp 6+
    //TODO  sm_plnTitle  应该使用这个  这个出现的要早
//    String s_sm_plnTgt_acp6;
                    //resultMap.get("s_sm_plnTgt");
//    if (s_sm_plnTgt_acp6 != null ){
//        targetName = s_sm_plnTgt_acp6;
//        log.info(data.get("taskIDFromTime")+ " "+"ACP6 Plan Name    "+ s_sm_plnTgt_acp6);
//    }else {
//        log.info(data.get("taskIDFromTime")+ " "+"ACP5 Plan Name    "+ s_sm_actTgt_acp5);
//        targ etName = s_sm_actTgt_acp5;
//    }

    boolean obsOffLine;
    boolean acpScriptActive;
    String scriptVERSION;

    public String getScriptVERSION() {
        return scriptVERSION;
    }

    public void setScriptVERSION(String scriptVERSION) {
        this.scriptVERSION = scriptVERSION;
    }

    public boolean getAcpScriptActive() {
        return acpScriptActive;
    }

    public void setAcpScriptActive(boolean acpScriptActive) {
        this.acpScriptActive = acpScriptActive;
    }

    String span_local_time_  ;
    String span_utc_time_    ;
    String span_SiderealTime_    ;

    String span_scope_status_;
    String span_scope_ra_    ;
    String span_scope_dec_   ;
    String span_scope_az_    ;
    String span_scope_alt_   ;
    String span_ccd_status_  ;
    String span_sm_trkGraphX_ ;
    String span_sm_trkGraphY_ ;
    String span_sm_trkGraphXY_;
    String span_ccd_filter_ ;
    String span_ccd_bin_    ;

    String scriptPath;
    String scriptName;
    String planPath;
    String planName;
    String consoleText;

    String planProgress;
    String planRepeat;
    String planFilter;
    String planCount;

    public String getPlanProgress() {
        return planProgress;
    }

    public void setPlanProgress(String planProgress) {
        this.planProgress = planProgress;
    }

    public String getPlanRepeat() {
        return planRepeat;
    }

    public void setPlanRepeat(String planRepeat) {
        this.planRepeat = planRepeat;
    }

    public String getPlanFilter() {
        return planFilter;
    }

    public void setPlanFilter(String planFilter) {
        this.planFilter = planFilter;
    }

    public String getPlanCount() {
        return planCount;
    }

    public void setPlanCount(String planCount) {
        this.planCount = planCount;
    }

    public String getPalnTarget() {
        return palnTarget;
    }

    public void setPalnTarget(String palnTarget) {
        this.palnTarget = palnTarget;
    }

    String palnTarget;

    public String getLastFWHM() {
        return lastFWHM;
    }

    public void setLastFWHM(String lastFWHM) {
        this.lastFWHM = lastFWHM;
    }

    String lastFWHM;

    public String getCameraCooler() {
        return cameraCooler;
    }

    public void setCameraCooler(String cameraCooler) {
        this.cameraCooler = cameraCooler;
    }

    String cameraCooler;

    public String getConsoleText() {
        return consoleText;
    }

    public void setConsoleText(String consoleText) {
        this.consoleText = consoleText;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getScriptPath() {
        return scriptPath;
    }

    public void setScriptPath(String scriptPath) {
        this.scriptPath = scriptPath;
    }

    public String getPlanPath() {
        return planPath;
    }

    public void setPlanPath(String planPath) {
        this.planPath = planPath;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getEqp_status() {
        return eqp_status;
    }

    public void setEqp_status(String eqp_status) {
        this.eqp_status = eqp_status;
    }

    public String getS_sm_obsStat() {
        return s_sm_obsStat;
    }

    public void setS_sm_obsStat(String s_sm_obsStat) {
        this.s_sm_obsStat = s_sm_obsStat;
    }

    public String getS_sm_camStat() {
        return s_sm_camStat;
    }

    public void setS_sm_camStat(String s_sm_camStat) {
        this.s_sm_camStat = s_sm_camStat;
    }

    public String getS_sm_guideStat() {
        return s_sm_guideStat;
    }

    public void setS_sm_guideStat(String s_sm_guideStat) {
        this.s_sm_guideStat = s_sm_guideStat;
    }

    public String getS_sm_plnTgt_acp6() {
        return s_sm_plnTgt_acp6;
    }

    public void setS_sm_plnTgt_acp6(String s_sm_plnTgt_acp6) {
        this.s_sm_plnTgt_acp6 = s_sm_plnTgt_acp6;
    }

    public boolean getObsOffLine() {
        return obsOffLine;
    }

    public void setObsOffLine(boolean obsOffLine) {
        this.obsOffLine = obsOffLine;
    }

    public String getSpan_utc_time_() {
        return span_utc_time_;
    }

    public void setSpan_utc_time_(String span_utc_time_) {
        this.span_utc_time_ = span_utc_time_;
    }

    public String getSpan_SiderealTime_() {
        return span_SiderealTime_;
    }

    public void setSpan_SiderealTime_(String span_SiderealTime_) {
        this.span_SiderealTime_ = span_SiderealTime_;
    }

    public String getSpan_local_time_() {
        return span_local_time_;
    }

    public void setSpan_local_time_(String span_local_time_) {
        this.span_local_time_ = span_local_time_;
    }

    public String getSpan_scope_status_() {
        return span_scope_status_;
    }

    public void setSpan_scope_status_(String span_scope_status_) {
        this.span_scope_status_ = span_scope_status_;
    }

    public String getSpan_scope_ra_() {
        return span_scope_ra_;
    }

    public void setSpan_scope_ra_(String span_scope_ra_) {
        this.span_scope_ra_ = span_scope_ra_;
    }

    public String getSpan_scope_dec_() {
        return span_scope_dec_;
    }

    public void setSpan_scope_dec_(String span_scope_dec_) {
        this.span_scope_dec_ = span_scope_dec_;
    }

    public String getSpan_scope_az_() {
        return span_scope_az_;
    }

    public void setSpan_scope_az_(String span_scope_az_) {
        this.span_scope_az_ = span_scope_az_;
    }

    public String getSpan_scope_alt_() {
        return span_scope_alt_;
    }

    public void setSpan_scope_alt_(String span_scope_alt_) {
        this.span_scope_alt_ = span_scope_alt_;
    }

    public String getSpan_ccd_status_() {
        return span_ccd_status_;
    }

    public void setSpan_ccd_status_(String span_ccd_status_) {
        this.span_ccd_status_ = span_ccd_status_;
    }

    public String getSpan_sm_trkGraphX_() {
        return span_sm_trkGraphX_;
    }

    public void setSpan_sm_trkGraphX_(String span_sm_trkGraphX_) {
        this.span_sm_trkGraphX_ = span_sm_trkGraphX_;
    }

    public String getSpan_sm_trkGraphY_() {
        return span_sm_trkGraphY_;
    }

    public void setSpan_sm_trkGraphY_(String span_sm_trkGraphY_) {
        this.span_sm_trkGraphY_ = span_sm_trkGraphY_;
    }

    public String getSpan_sm_trkGraphXY_() {
        return span_sm_trkGraphXY_;
    }

    public void setSpan_sm_trkGraphXY_(String span_sm_trkGraphXY_) {
        this.span_sm_trkGraphXY_ = span_sm_trkGraphXY_;
    }

    public String getSpan_ccd_filter_() {
        return span_ccd_filter_;
    }

    public void setSpan_ccd_filter_(String span_ccd_filter_) {
        this.span_ccd_filter_ = span_ccd_filter_;
    }

    public String getSpan_ccd_bin_() {
        return span_ccd_bin_;
    }

    public void setSpan_ccd_bin_(String span_ccd_bin_) {
        this.span_ccd_bin_ = span_ccd_bin_;
    }



    public AcpStatus(boolean isObsOffLine , String span_local_time_, String span_utc_time_, String span_SiderealTime_
            , String span_scope_status_, String scriptPath, String scriptName,String planPath,String planName, String cameraStatus, String span_sm_trkGraphX_
            , String span_sm_trkGraphY_, String span_sm_trkGraphXY_, String span_ccd_filter_, String span_ccd_bin_, boolean acpScriptActive
            ,String scriptVERSION , boolean acpRunning,String consoleText){
        this.obsOffLine=isObsOffLine;
        this.span_local_time_=span_local_time_;
        this.span_utc_time_=span_utc_time_;
        this.span_SiderealTime_=span_SiderealTime_;
        this.span_scope_status_=span_scope_status_;
        this.scriptPath = scriptPath;
        this.scriptName = scriptName;
        this.planPath = planPath;
        this.planName = planName;
        this.span_ccd_status_=cameraStatus;
        this.span_sm_trkGraphX_=span_sm_trkGraphX_;
        this.span_sm_trkGraphY_=span_sm_trkGraphY_;
        this.span_sm_trkGraphXY_=span_sm_trkGraphXY_;
        this.span_ccd_filter_=span_ccd_filter_;
        this.span_ccd_bin_=span_ccd_bin_;
        this.acpScriptActive=acpScriptActive;
        this.scriptVERSION = scriptVERSION;
        this.acpRunning = acpRunning;
        this.consoleText=consoleText;
    }



}
