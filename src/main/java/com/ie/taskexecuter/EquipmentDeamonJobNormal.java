package com.ie.taskexecuter;

import com.ie.plangen.remoteobs.asputil.plangen.PlanCheckExeption;
import com.ie.plangen.remoteobs.asputil.plangen.PlanFileContent;
import com.ie.plangen.remoteobs.asputil.plangen.plantarget.NormalPlan;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.ie.sysmessage.TaskData;
import com.ie.taskexecuter.util.*;
import jnr.ffi.Struct;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class EquipmentDeamonJobNormal implements Job{
	
	private Logger log = LoggerFactory.getLogger(EquipmentDeamonJobNormal.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat sdf_sss=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-SSS");

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();
	    
//	    TaskCommandEnum taskCommand=(TaskCommandEnum) data.getOrDefault("SysConfig.task_name", TaskCommandEnum.ReportStatus);
	    String pre_task_name  = data.getOrDefault("pre_task_name", "default").toString();
	    String next_task_name = data.get("next_task_name").toString();
	    data.put("taskIDFromTime" ,"[[[[["+sdf_sss.format(new Date())+"]]]]]");
	    String pre_task_level = data.getOrDefault("pre_task_level", "0").toString();
	    String next_task_level= data.getOrDefault("next_task_level", "0").toString();
//	    data.put("xxxx", "xxxx");
//	    data.put("xxxx","yyyyy");
//	    JobDataMap data_merge = context.getJobDetail().getJobDataMap();
//		System.out.println(">>>>>>>>>>>>>>>>>>"+next_task_name);
//		System.out.println(">>>>>>>>>>>>>>>>>>"+data_merge.get("next_task_name").toString());
//		System.out.println(">>>>>>>>>>>>>>>>>>"+data_merge.get("xxxx"));
	    
		String equipment_lastPlanDir=	   data.get("equipment_lastPlanDir").toString();
		Boolean equipment_isChainLast=    data.get("equipment_isChainLast") == null ? false :(Boolean) data.get("equipment_isChainLast");
		String equipment_latitude=	       data.get("equipment_latitude").toString();
		String equipment_longitude=	       data.get("equipment_longitude").toString();
		String equipment_User=	           data.get("equipment_User").toString();
		String equipment_Pass=	           data.get("equipment_Pass").toString();
		String equipment_LimitAngle_all=   data.get("equipment_LimitAngle_all").toString();
		String equipment_Url=              data.get("equipment_Url").toString();
		String equipment_PlanPath=	       data.get("equipment_PlanPath").toString();
		String equipment_filter=	       data.get("equipment_filter").toString();
		String acpFitsRoot=	       data.get("acpFitsRoot").toString();
		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();

		Boolean is_chain_stoped_plan=	       data.get("is_chain_stoped_plan") == null ? false : (Boolean) data.get("is_chain_stoped_plan");

		String equipment_safe_east_alt=	       data.get("equipment_safe_east_alt").toString();
		String equipment_safe_east_az=	       data.get("equipment_safe_east_az").toString();
		String equipment_safe_west_alt=	       data.get("equipment_safe_west_alt").toString();
		String equipment_safe_west_az=	       data.get("equipment_safe_west_az").toString();
		boolean Equipment_isHMT_SafePoint= 		Boolean.parseBoolean(data.get("Equipment_isHMT_SafePoint").toString());

//		Map<String, String> resultMap = AcpStatusUpdater.getSystemStatus(equipment_Url,equipment_User,equipment_Pass);
		AcpStatus acpStatus = null;
		try {
			acpStatus = AcpComControl.GetStatus(device_name);
		} catch (Exception e) {
			e.printStackTrace();
			acpStatus = new AcpStatus(false,"","","","","","","",""
			,"","","","","","",false,"error",true,"");
		}

		DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name ,device_name , "运行中 "+pre_task_name + "  " +  !acpStatus.getObsOffLine()
				, sdf.format(new Date()),device_color , MessageColor.OK, acpStatus.getSpan_utc_time_(), acpStatus.getSpan_local_time_(), acpStatus.getSpan_scope_status_()
				, acpStatus.getSpan_scope_ra_(), acpStatus.getSpan_scope_dec_(), acpStatus.getSpan_scope_az_(), acpStatus.getSpan_scope_alt_()
				, acpStatus.getSpan_ccd_status_(), acpStatus.getSpan_ccd_filter_(), acpStatus.getSpan_ccd_bin_(),equipment_filter);
		deviceStatus.setSpan_sm_trkGraphX_(acpStatus.getSpan_sm_trkGraphX_());
		deviceStatus.setSpan_sm_trkGraphY_(acpStatus.getSpan_sm_trkGraphY_());
		deviceStatus.setSpan_sm_trkGraphXY_(acpStatus.getSpan_sm_trkGraphXY_());
		deviceStatus.setTaskName(acpStatus.getPlanName());
		deviceStatus.setConsoleText(acpStatus.getConsoleText());
		deviceStatus.setCameraCooler((acpStatus.getCameraCooler()));
		deviceStatus.setGuideFWHM(acpStatus.getLastFWHM());
		deviceStatus.setGuideCameraStatus(acpStatus.getS_sm_guideStat());
		deviceStatus.setS_sm_guideStat(acpStatus.getS_sm_guideStat());
		deviceStatus.setPlanProcess(acpStatus.getPlanProgress());
		deviceStatus.setPlanRepeate(acpStatus.getPlanRepeat());
		deviceStatus.setPlanFilter(acpStatus.getPlanFilter());
		deviceStatus.setPlanCount(acpStatus.getPlanCount());
		deviceStatus.setPlanTarget(acpStatus.getPalnTarget());
		deviceStatus.setObsOffLine(acpStatus.getObsOffLine());
//		deviceStatus.
		AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
		if(true){
			return ;
		}


		//todo  判定 未连接
//		if (resultMap == null){
		if (acpStatus.getObsOffLine()){
//			data.put("equipment_status_result_map", resultMap);
			log.info(data.get("taskIDFromTime")+ " "+" XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ");
//			log.info(device_name +" 无法获取到设备状态 " +equipment_Url+"@" +equipment_User  );
			log.info(device_name +" 设备不在线 "  );
			log.info(data.get("taskIDFromTime")+ " "+" XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ");
			return;
		}

//		log.debug(resultMap);
//		log.info(data.get("taskIDFromTime")+ " "+resultMap.get("s_sm_scopeStat"));
//		log.debug(resultMap.get("s_sm_obsStat"));
//		log.debug(resultMap.get("s_sm_plnTitle"));
//		log.debug(resultMap.get("s_sm_az"));
//		log.debug(resultMap.get("s_sm_alt"));
//		log.debug(resultMap.get("p_sm_expProg"));
//		log.debug(resultMap.get("c_value"));
//		log.debug(resultMap.get("v_sm_imgTempRow"));
//		String eqp_status=resultMap.get("s_sm_scopeStat");
//		String s_sm_obsStat=resultMap.get("s_sm_obsStat");
//		String s_sm_camStat=resultMap.get("s_sm_camStat");
//		String s_sm_guideStat=resultMap.get("s_sm_guideStat");
//		String targetName = "";
//		//acp 5
//		String s_sm_actTgt_acp5=resultMap.get("s_sm_actTgt");
//		//acp 6+
//		//TODO  sm_plnTitle  应该使用这个  这个出现的要早
//		String s_sm_plnTgt_acp6=resultMap.get("s_sm_plnTgt");
//		if (s_sm_plnTgt_acp6 != null ){
//			targetName = s_sm_plnTgt_acp6;
//			log.info(data.get("taskIDFromTime")+ " "+"ACP6 Plan Name    "+ s_sm_plnTgt_acp6);
//		}else {
//			log.info(data.get("taskIDFromTime")+ " "+"ACP5 Plan Name    "+ s_sm_actTgt_acp5);
//			targetName = s_sm_actTgt_acp5;
//		}

//		boolean isObsOffLine = AcpStatusUpdater.isObsOffLine(resultMap );
//		log.info("--" + resultMap.get("s_sm_obsStat"));
		log.info("--" + acpStatus.getObsOffLine());
		String span_utc_time_    	= acpStatus.getSpan_utc_time_();
		String span_local_time_  	= acpStatus.getSpan_local_time_();
		String span_scope_status_	= acpStatus.getSpan_scope_status_();
		String span_scope_ra_    	= acpStatus.getSpan_scope_ra_();
		String span_scope_dec_   	= acpStatus.getSpan_scope_dec_();
		String span_scope_az_    	= acpStatus.getSpan_scope_az_();
		String span_scope_alt_   	= acpStatus.getSpan_scope_alt_();
		String span_ccd_status_  	= acpStatus.getSpan_ccd_status_();
		String span_sm_trkGraphX_  	= acpStatus.getSpan_sm_trkGraphX_();
		String span_sm_trkGraphY_ 	= acpStatus.getSpan_sm_trkGraphY_();
		String span_sm_trkGraphXY_  = acpStatus.getSpan_sm_trkGraphXY_();
//		String span_utc_time_    =resultMap.get("s_sm_utc");
//		String span_local_time_  =resultMap.get("s_sm_lst");
//		String span_scope_status_=resultMap.get("s_sm_scopeStat");
//		String span_scope_ra_    =resultMap.get("s_sm_ra");
//		String span_scope_dec_   =resultMap.get("s_sm_dec");
//		String span_scope_az_    =resultMap.get("s_sm_az");
//		String span_scope_alt_   =resultMap.get("s_sm_alt");
//		String span_ccd_status_  =resultMap.get("s_sm_camStat");
//		String span_sm_trkGraphX_  =resultMap.get("s_sm_trkGraphX").replace("%2C",",").replace("%3A",":");
//		String span_sm_trkGraphY_  =resultMap.get("s_sm_trkGraphY").replace("%2C",",").replace("%3A",":");
//		String span_sm_trkGraphXY_  =resultMap.get("s_sm_trkGraphXY").replace("%2C",",").replace("%3A",":");



//		String span_ccd_filter_  =acpStatus.getSpan_ccd_filter_();
//		String span_ccd_bin_     =acpStatus.getSpan_ccd_bin_();
//		span_utc_time_     = span_utc_time_     .replace("@an","").replace("@wn","").replace("@in","").replace("%3A",":").replace("@an","");
//		span_local_time_   = span_local_time_   .replace("@an","").replace("@wn","").replace("@in","").replace("%3A",":");
//		span_scope_status_ = span_scope_status_ .replace("@an","").replace("@wn","").replace("@in","").replace("@af","").replace("%20"," ").replace("%B0","°");
//		s_sm_guideStat = s_sm_guideStat .replace("@an","").replace("@wn","").replace("@in","").replace("@af","").replace("%20"," ").replace("%B0","°");
//		span_scope_ra_     = span_scope_ra_     .replace("@an","").replace("@wn","").replace("@in","").replace("%3A",":");
//		span_scope_dec_    = span_scope_dec_    .replace("@an","").replace("@wn","").replace("@in","").replace("%3A",":").replace("%3F","°").replace("%27","'").replace("%22","''").replace("%B0","°");
//		span_scope_az_     = span_scope_az_     .replace("@an","").replace("@wn","").replace("@in","").replace("%B0","°");
//		span_scope_alt_    =span_scope_alt_     .replace("@an","").replace("@wn","").replace("@in","").replace("%B0","°");
//
//		span_ccd_status_  = span_ccd_status_ .replace("@an","").replace("@wn","").replace("@in","").replace("@af","").replace("%20"," ");
//		span_ccd_filter_  = span_ccd_filter_ .replace("@an","").replace("@wn","").replace("@in","");
//		span_ccd_bin_     = span_ccd_bin_    .replace("@an","").replace("@wn","").replace("@in","").replace("%3A","/");
//
//		targetName     = targetName    .replace("@an","").replace("@wn","").replace("@in","").replace("@af","")
//				.replace("%3A","/").replace("%20"," ").replace("%28","(").replace("%29",")");

//		log.info(data.get("taskIDFromTime")+ " "+span_ccd_status_ + "      --     " + span_ccd_filter_ + "    ---    " + eqp_status +"     "+isObsOffLine  );
//		TaskData taskData_x = (TaskData) data.get("task_data");
		TaskData taskData = (TaskData) data.get("task_data");
		//Stop Script 只能执行一次
		Boolean  isStopScritpRun =  data.get("isStopScritpRun") ==  null ? false : (Boolean) data.get("isStopScritpRun");
		if(taskData == null){
			taskData = new TaskData();
			taskData.setTaskName("");
			taskData.setTask_status("stopped");
		}
		log.info(acpStatus.getPlanName());
//		if(targetName != null && targetName.contains(" (")){
//			targetName = targetName.substring(0,targetName.indexOf(" ("));
//			log.info("ACP6 Plan Name    "+ targetName);
//		}
//		if (taskData !=  null){
//			log.info(taskData.getTaskName().toUpperCase().replace("@in",""));
//		}

//		resultMap.put("taskIDFromTime",data.get("taskIDFromTime").toString() );
//		data.put("equipment_status", eqp_status);
//		data.put("equipment_status_result_map", resultMap);
//		data.put("span_scope_status_", span_scope_status_);
//		data.put("span_ccd_status_", span_ccd_status_);
//		data.put("s_sm_obsStat", s_sm_obsStat);
//		data.put("acp_target_name", targetName);
//		log.debug("-----"+eqp_status);
		//todo check if obs offline
		log.info(data.get("taskIDFromTime")+ " "+"检测设备连接状态 " );
//	   if(eqp_status == null){
//		   log.info(data.get("taskIDFromTime")+ " "+"设备错误 " );
//		   DeviceStatus deviceStatus = new DeviceStatus("设备错误" , device_name ," 无法获取到设备状态 " +equipment_Url+"@" +equipment_User
//				   , sdf.format(new Date()) ,device_color , MessageColor.Error, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_,equipment_filter);
//		   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//		   log.info(device_name +" 无法获取到设备状态 " +equipment_Url+"@" +equipment_User  );
//		   return;
//	   }

		log.info(data.get("taskIDFromTime")+ " "+"检测设备在线状态 " );
//	   if( acpStatus.isObsOffLine() ){
//		   DeviceStatus deviceStatus = new DeviceStatus("设备离线" , device_name ," 设备离线 赤道仪" +eqp_status +" 相机 " +s_sm_camStat
//				   , sdf.format(new Date()) ,device_color , MessageColor.Error, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_,equipment_filter);
//		   log.info("设备离线 " );
//		   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//		   log.info(device_name +" 设备离线 赤道仪" +eqp_status +" 相机 " +s_sm_camStat  );
//		   return;
//	   }

//	   if(isRunning){}
//	   target
		log.info(data.get("taskIDFromTime")+ " "+"检测当前是否有任务 " );
	   // ACP 6 会有 @afAcquireImages 的情况
//	   if(targetName != null && (!"".equals(targetName)) && (!targetName.startsWith("n/a" )) && targetName.length()>0){
//		   log.info(data.get("taskIDFromTime")+ " "+"已有任务运行");
//		   String statusMessage=eqp_status;
//			log.info(data.get("taskIDFromTime")+ " "+"ACP: " + targetName + "      Task:  " +taskData.getTaskName());
//
//		   if ( (!targetName.equals(taskData.getTaskName()))  && taskData.getTaskName().length()>0){
//		   	if (targetName.startsWith("GRB")){
//				log.info(data.get("taskIDFromTime")+ " "+"GRB 任务执行中");
//				DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name , "运行中 "+pre_task_name + "  " +statusMessage , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_,equipment_filter);
//				deviceStatus.setTaskName(targetName);
//				AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//				return;
////			}else if(targetName.equals("AcquireImages")){
////		   		log.debug("ACP 6.x 的执行目标不能在此处获得       TODO");
////				DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name , "运行中 "+pre_task_name + "  " +statusMessage , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_);
////				AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
////				return;
//			}else{
//
//				log.info(data.get("taskIDFromTime")+ " "+"ACP: " + targetName + "      Task:  " +taskData.getTaskName() );
//				//stop old
//				   log.info(data.get("taskIDFromTime")+ " "+"任务覆盖");
//					if (!isStopScritpRun){
//					   DeviceStatus deviceStatus = new DeviceStatus("任务消息" , device_name ,"任务覆盖 "+pre_task_name+" < "+next_task_name , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_,equipment_filter);
//					   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//					   log.info(data.get("taskIDFromTime")+ " "+">>>>>>>>>>>>>>>>>>>>>>stop old");
//					   String stopResult = AcpControl.stopRunPlan(equipment_Url, equipment_User, equipment_Pass);
//					   log.info(data.get("taskIDFromTime")+ " "+stopResult);
//						data.put("isStopScritpRun" ,true);
//						log.info(data.get("taskIDFromTime")+ " "+ "equipment_isChainLast " + equipment_isChainLast);
//						data.put("is_chain_stoped_plan" , equipment_isChainLast);
//
//					}else {
//						log.info(data.get("taskIDFromTime")+ " "+"stopped once  已经执行过停止脚本 等待执行新脚本");
//					}
//					return;
//			}
//		   }else {
//		   		taskData.setTask_status("running");
//			   log.info(data.get("taskIDFromTime")+ " "+">>>>>>>>>>>>>>>>>>>>>>running");
//			   DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name , "运行中 "+pre_task_name + "  " +statusMessage , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_,equipment_filter);
//			   deviceStatus.setSpan_sm_trkGraphX_(span_sm_trkGraphX_);
//			   deviceStatus.setSpan_sm_trkGraphY_(span_sm_trkGraphY_);
//			   deviceStatus.setSpan_sm_trkGraphXY_(span_sm_trkGraphXY_);
//			   deviceStatus.setTaskName(targetName);
//			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//			   return ;
//		   }
//
//	   }

		log.info(data.get("taskIDFromTime")+ " "+"当前无任务正在执行 " );
//		data.put("isStopScritpRun" ,false);
//		if (AcpStatusUpdater.isReadyToNext(resultMap)) { //todo stoped or inuse?
//			log.info(data.get("taskIDFromTime")+ " "+">>>>>>>>>>>>>>>>>>>>>>avilable");
//			DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name ,"空闲" , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_,equipment_filter);
//			AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
////			    TODO release ACP Auto Release
//			if(taskData != null){
//				if(taskData.getTask_status().equals("init")){
//					log.info(data.get("taskIDFromTime")+ " "+"有新任务执行");
//				}else {
//					taskData.setTask_status("stopped");
//					log.info(data.get("taskIDFromTime")+ " "+"clean task");
//					taskData.setTaskName("");
//					taskData.setTask_level("0");
//					return;
//				}
//			}else {
//				return;
//			}
//			log.info(data.get("taskIDFromTime")+ " "+"执行新任务");
//		}else{
//			log.info(data.get("taskIDFromTime")+ " "+">>>>>>>>>>>>>>>>>>>>>>need set auto release  ");
//			log.info(data.get("taskIDFromTime")+ " "+">>>>>>>>>>>>>>>>>>>>>> 需要设置自动释放 或者安全原因判定无法执行");
//			return;
//		}


//	   run new
//	   if (next_task_name.equals("")) {
//
////		   if (eqp_status.equals("@anStopped")) { //todo stoped or inuse?
//		   if (AcpStatusUpdater.isReadyToNext(resultMap)) { //todo stoped or inuse?
//			   log.debug(">>>>>>>>>>>>>>>>>>>>>>avilable");
////			   String jsonEquipmentMessage="{\"type\":\"update\",\"value\":\""+eqp_status+"\"}";
////			   sendMessage(data, msg_name_equipment_status,jsonEquipmentMessage);
//			   DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name ,"空闲" , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_);
//			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//			   //todo relase obs and init pre_task_name and next_task_name
////			   if("ownner".equals("Free")){relase}
//			    data.put("pre_task_name", "default");
//			    data.put("pre_task_level", "0");
////			    TODO release
//			   return;
//		   }else{
////			   String statusMessage=eqp_status;
//
////			   log.debug(">>>>>>>>>>>>>>>>>>>>>>running");
//////			   String jsonEquipmentMessage="{\"type\":\"update\",\"value\":"+eqp_status+"  "+pre_task_name+"\"}";
//////			   sendMessage(data, msg_name_equipment_status,jsonEquipmentMessage);
////			   DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name , "运行中 "+pre_task_name + "  " +statusMessage , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_);
////			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
////			   return ;
//		   }
//	   }else{
//		   if (Integer.parseInt(next_task_level)>Integer.parseInt(pre_task_level)) {
//			   String jsonEquipmentMessage="{\"type\":\"info\",\"value\": \"cover task "+pre_task_name+"<"+next_task_level+"\"}";
//			   sendMessage(data, msg_name_equipment_status,jsonEquipmentMessage);
//			   DeviceStatus deviceStatus = new DeviceStatus("任务消息" , device_name ,"任务覆盖 "+pre_task_name+" < "+next_task_name , sdf.format(new Date()),device_color , MessageColor.OK, span_utc_time_, span_local_time_, span_scope_status_, span_scope_ra_, span_scope_dec_, span_scope_az_, span_scope_alt_, span_ccd_status_, span_ccd_filter_, span_ccd_bin_);
//			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//			   if (eqp_status.equals("@anStopped") || eqp_status.equals("@anReady")) {
				   log.info(data.get("taskIDFromTime")+ " "+">>>>>>>>>>>>>>>>>>>>>>run new");
//					TODO add extra task before run
//			TODO  release obs if it used by others / Or  every one use this to run	   
					
	

//
//					hmsm hmsm_ra=new hmsm(Math.toRadians(taskData.getTask_Ra_deg()));
//					dms dms_dec=new  dms(Math.toRadians(taskData.getTask_Dec_deg()));
					Double lat=Double.parseDouble(equipment_latitude),longt=Double.parseDouble(equipment_longitude);
					Calendar calendar=new GregorianCalendar();
//					System.out.println(calendar);
//					System.out.println(hmsm_ra);
//					System.out.println(dms_dec);
//					System.out.println(lat);
//					System.out.println(longt);

//					double[] altAz_rad= Equatorial_To_Horizontal.Convert_E2H(hmsm_ra, dms_dec,lat,longt,calendar);
//					double[] altAz_deg=new double[]{Math.toDegrees(altAz_rad[0]),Math.toDegrees(altAz_rad[1])};
//					System.out.println("-------"+altAz_deg[0]);
//					System.out.println("-------"+altAz_deg[1]);

//					double s_sm_az,s_sm_alt;
//					try {
//						String s_sm_az_eqp  =URLDecoder.decode(resultMap.get("s_sm_az"),"UTF-8");
//						String s_sm_alt_eqp =URLDecoder.decode(resultMap.get("s_sm_alt"),"UTF-8");
//						String regexp_double = "[\\d\\.]+";
//				        Pattern pattern_double = Pattern.compile(regexp_double);
//						Matcher matcher_double_az = pattern_double.matcher(s_sm_az_eqp);
//						Matcher matcher_double_alt = pattern_double.matcher(s_sm_alt_eqp);
//				        if (matcher_double_az.find()) {s_sm_az_eqp = matcher_double_az.group(0);}
//				        if (matcher_double_alt.find()) {s_sm_alt_eqp = matcher_double_alt.group(0);}
//				        s_sm_az =  Double.parseDouble(s_sm_az_eqp);
//				        s_sm_alt = Double.parseDouble(s_sm_alt_eqp);
//					} catch (NumberFormatException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						stopJob(data ,next_task_name ,next_task_level);
//						return;
//					} catch (UnsupportedEncodingException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						stopJob(data ,next_task_name ,next_task_level);
//						return;
//					}
//				   log.info(data.get("taskIDFromTime")+ " "+"s_sm_az:  "+s_sm_az);
//				   log.info(data.get("taskIDFromTime")+ " "+"s_sm_alt: "+s_sm_alt);



					
					//TODO create task file
					
//
//				   log.info(data.get("taskIDFromTime")+ " "+resultMap.get("s_sm_az"));
//				   log.info(data.get("taskIDFromTime")+ " "+resultMap.get("s_sm_alt"));
				   //TODO go back to safe position before run chain
				   //run new //todo chain last plan 
				   //TODO chain task will run as high task level ,may be cause  conflict,low level to high level; 

//					Random ra=new Random();//19:25:12.00	-01° 04' 48.0"  ISO 8859-1
//					String safe_dec_string=safe_dms_dec.getDeg()+"° "+safe_dms_dec.getMin()+"' "+safe_dms_dec.getSec()+"\"";
//					String safe_dec_string=String.valueOf(Math.toDegrees(safe_dec_rad));
//					//19:25:12.00
////					String safe_ra_string=safe_hmsm_ra.buildStringHMSM(':');
//					String safe_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(Math.toDegrees(safe_ra_rad)));
					
					String task_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(((taskData.getTask_Ra_deg()%360)+360)%360));
					String task_dec_string=String.valueOf(taskData.getTask_Dec_deg());
					
					PlanFileContent planFile=new PlanFileContent();

					if(taskData.getTaskName().startsWith("GRB")){
//						double safe_ra_rad,safe_dec_rad;
//						if (s_sm_az>=180) {
//							double [] radec_w=Horizontal_To_Equatorial.Convert_H2E(Double.parseDouble(equipment_safe_west_alt),
//									Double.parseDouble(equipment_safe_west_az), lat, longt, calendar);
//							safe_ra_rad=radec_w[0];
//							safe_dec_rad =radec_w[1];
//						}else {
//							double [] radec_e=Horizontal_To_Equatorial.Convert_H2E(Double.parseDouble(equipment_safe_east_alt),
//									Double.parseDouble(equipment_safe_east_az), lat, longt, calendar);
//							safe_ra_rad=radec_e[0];
//							safe_dec_rad=radec_e[1];
//						}
//						log.info(data.get("taskIDFromTime")+ " "+"safe_hmsm_ra   "+safe_ra_rad);
//						log.info(data.get("taskIDFromTime")+ " "+"safe_dms_dec   "+safe_dec_rad);
//						String safe_dec_string=String.valueOf(Math.toDegrees(safe_dec_rad));
//						String safe_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(Math.toDegrees(safe_ra_rad)));


						//Last task file
						String HMTlastPlanFilePath="";
						File lastPlanDir=new File(equipment_lastPlanDir);
						Long nowLong = System.currentTimeMillis();
						log.info(data.get("taskIDFromTime")+ " "+nowLong.toString());
						log.info(data.get("taskIDFromTime")+ " "+equipment_lastPlanDir);
						if (lastPlanDir.isDirectory()&&lastPlanDir.exists() && is_chain_stoped_plan) {
							File[] planFiles=lastPlanDir.listFiles();
							log.info(data.get("taskIDFromTime")+ " "+lastPlanDir.getAbsolutePath());
							long lastModifyTime=0;
							for (File file : planFiles) {
								log.info(data.get("taskIDFromTime")+ " "+file.getName() + "   :    " + nowLong + " - " + file.lastModified());
								if (Math.abs(nowLong - file.lastModified())>1800000){
									log.info(data.get("taskIDFromTime")+ " "+"需要链接的文件需要在临近的半小时被编辑过");
									log.info(data.get("taskIDFromTime")+ " "+"The file need to be modified in half an hour");
									continue;
								}else {
									log.info(data.get("taskIDFromTime")+ " "+file.getAbsoluteFile() + " -------------Chain--------------");
								}
								if (file.isFile()&&file.getName().toLowerCase().endsWith(".txt")&&file.lastModified()>lastModifyTime) {
									lastModifyTime=file.lastModified();
									HMTlastPlanFilePath=file.getAbsolutePath();
								}
							}
						}
						//todo  lastModify 没有限定时间区域 可能很久的计划也会被执行 now - lastmodifyTime < 30分钟



//						planFile = PlanGeneratePlanFile_HMT(Double.parseDouble(task_ra_string) , Double.parseDouble(task_dec_string) ,Double.parseDouble(safe_ra_string) ,
//								Double.parseDouble(safe_dec_string),equipment_filter ,Equipment_isHMT_SafePoint ,is_chain_stoped_plan ,HMTlastPlanFilePath);
					}else if(taskData.getTaskName().startsWith("GW")){
//						String gwTaskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
//						try {
//							FileUtils.writeStringToFile(new File(gwTaskPlanFilePath),taskData.getTask_plan_text(),"UTF-8");
//						} catch (IOException e) {
//							log.error("--",e);
//							e.printStackTrace();
//						}
					}else{
	//					NormalPlan plan_safe=new NormalPlan("SafePoint"+"-time",safe_ra_string,safe_dec_string);
	//					plan_safe.setSubSequenTar_BINNING(new int []{4});
	//					plan_safe.setSubSequenTar_INTERVAL(new int []{2});
	//					planFile.getTargetList().add(plan_safe);
					   // TODO: 2017/10/1  ra 生成了一个负的

						NormalPlan plan_4_1=new NormalPlan(taskData.getTaskName() ,task_ra_string,task_dec_string);
//						plan_4_1.setSubSequenTar_DIR("Web_Normal");
//						plan_4_1.setSubSequenTar_BINNING(new int []{2});
					   org.joda.time.format.DateTimeFormatter parserISO = ISODateTimeFormat.dateTimeNoMillis();
	//				   Date start_time = parserISO.parseDateTime(taskData.getTask_start_time()).toDate();
					   log.info(data.get("taskIDFromTime")+ " "+taskData.getTask_start_time());
					   log.info(data.get("taskIDFromTime")+ " "+taskData.getTask_end_time());
	//				   int intervalSecond = (int) ((parserISO.parseDateTime(taskData.getTask_end_time()).getMillis()
	//						   -parserISO.parseDateTime(taskData.getTask_start_time()).getMillis())/1000);
					   double intervalSecond ;
//					   if (taskData.getInterval_second()==0){
//							intervalSecond=30;// TODO: 2018/1/30  暂时指定
//					   }else {
//						   intervalSecond=taskData.getInterval_second();
//					   }
//					   log.warn("interval = 30s      默认指定interval是30秒 ");
//					   log.info(data.get("taskIDFromTime")+ " "+"interval = " +intervalSecond);

						log.info(taskData.getFilterBinningIntervalCount().getClass().toString());
//						log.info(taskData.getFilterBinningIntervalCount().get(0).getClass().toString());
						List<String> filterBinExpCount = taskData.getFilterBinningIntervalCount();
					   String param_filters[] = new String[filterBinExpCount.size()];
					   int param_bins[] =new int[filterBinExpCount.size()];
					   double param_exp[] =new double[filterBinExpCount.size()];
					   int param_count[] =new int[filterBinExpCount.size()];
						for (int i_param =0 ;i_param<filterBinExpCount.size() ;i_param++) {
//							log.info(filterBinExpCount.get(i_param)[0].getClass().toString());
//							log.info(filterBinExpCount.get(i_param)[1].getClass().toString());
//							log.info(filterBinExpCount.get(i_param)[2].getClass().toString());
//							log.info(filterBinExpCount.get(i_param)[3].getClass().toString());
							log.info(filterBinExpCount.get(i_param).getClass().toString());
							String[] filterItem = filterBinExpCount.get(i_param).split(",");
							param_filters[i_param] = filterItem[0];
							param_bins[i_param] = Integer.parseInt(filterItem[1]);
							param_exp[i_param] = Double.parseDouble(filterItem[2]);
							param_count[i_param] = Integer.parseInt(filterItem[3]);
						}

						if (param_filters.length>0 && param_filters[0].length()>0){
							plan_4_1.setSubSequenTar_FILTER(param_filters);
						}
						plan_4_1.setSubSequenTar_BINNING(param_bins);
						plan_4_1.setSubSequenTar_COUNT(param_count);
						plan_4_1.setSubSequenTar_INTERVAL(param_exp);



						plan_4_1.setSubSequenTar_DIR(acpFitsRoot);
//						if (null != equipment_filter && equipment_filter.length() > 0){
//							plan_4_1.setSubSequenTar_FILTER(new String []{equipment_filter});
//						}

						planFile.getTargetList().add(plan_4_1);

					}

					String taskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
					try {
						planFile.toFile(taskPlanFilePath);
						log.info(data.get("taskIDFromTime")+ " "+planFile.toOutString());
					} catch (PlanCheckExeption | IOException e) {
						e.printStackTrace();
						stopJob(data ,next_task_name ,next_task_level);
						return;
					}
				   
				   String requestResult = AcpControl.runPlan(taskPlanFilePath, equipment_Url, equipment_User, equipment_Pass);
					log.info(data.get("taskIDFromTime")+ " "+requestResult);
					//成功执行
					if(requestResult.contains("Run Started successfully")){
						taskData.setTask_status("running");
					}else {
						taskData.setTask_status("stopped");
						//任务执行失败
					}
				   //init pre_task and next task
				   
				    data.put("pre_task_name", next_task_name);
				    data.put("pre_task_level", next_task_level);
				    data.put("next_task_name", "");
				    data.put("next_task_level", "0");
				   

			   
//			}else if (Integer.parseInt(next_task_level)==Integer.parseInt(pre_task_level)){
//				//do nothing and report a warning
//				log.debug(">>>>>>>>>>>>>>>>>>>>>>conflict task");
//			   DeviceStatus deviceStatus = new DeviceStatus("任务警告" , device_name ,"任务冲突 "+pre_task_name+" = "+next_task_name
//					   , sdf.format(new Date()),device_color , MessageColor.Warning);
////				String jsonEquipmentMessage="{\"type\":\"warning\",\"value\": \"conflict task "+pre_task_name+"="+next_task_level+"\"}";
//			   AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatus).toString());
//			}else {
//				log.debug(">>>>>>>>>>>>>>>>>>>>>>ignore task");
//			   DeviceStatus deviceStatus = new DeviceStatus("任务警告" , device_name ,"任务忽略 "+pre_task_name+" 》 "+next_task_name
//					   , sdf.format(new Date()),device_color , MessageColor.Warning);
////				String jsonEquipmentMessage="{\"type\":\"info\",\"value\":\" ignore task "+pre_task_name+">"+next_task_level+"\"}";
//			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//			}
		   
//	   }
	   
	   
	   
		
	}

	//
	private void  stopJob( JobDataMap data ,String next_task_name ,String next_task_level){
		data.put("pre_task_name", next_task_name);
		data.put("pre_task_level", next_task_level);
		data.put("next_task_name", "");
		data.put("next_task_level", "0");
	}


//	private void sendMessage(JobDataMap data, String msg_name_equipment_status,String jsonMessage) {
//		ConnectionFactory connectionFactory;
//        Connection connection = null;
//        Session session;
//        Destination destination;
//        MessageProducer producer;
//        TextMessage message = null;
//        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
//                , ActiveMQConnection.DEFAULT_PASSWORD, data.getString("SysConfig.msg_url"));
//        try {
//            connection = connectionFactory.createConnection();
//            connection.start();
//            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
//            destination = session.createTopic(msg_name_equipment_status);
//            producer = session.createProducer(destination);
//            //todo use json text message
//            message=session.createTextMessage(jsonMessage);
//            producer.send(message);
//            session.commit();
//            log.debug("send AMQ " + jsonMessage);
//        } catch (JMSException e) {
//            e.printStackTrace();
//        }finally {
//            try {
//                if (null != connection){
//                    connection.close();
//                }
//            } catch (Throwable ignore) {
//            }
//        }
//	}

	PlanFileContent PlanGeneratePlanFile_HMT(Double ra_ha ,Double dec_deg ,Double ra_safe ,Double dec_safe,String equipment_filter ,boolean isHMT_SafePoint ,boolean is_chain_stoped_plan ,String HMTlastPlanFilePath){

		PlanFileContent planFile=new PlanFileContent();
		NormalPlan plan_safe=new NormalPlan("SafePoint"+"-time",ra_safe,dec_safe);
		plan_safe.setSubSequenTar_BINNING(new int []{4});
		plan_safe.setSubSequenTar_INTERVAL(new double[]{2});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_safe.setSubSequenTar_FILTER(new String []{equipment_filter});
		}
		if(isHMT_SafePoint){
			planFile.getTargetList().add(plan_safe);
		}


		NormalPlan plan_4_1=new NormalPlan("GRB-40S-1"+"-time",ra_ha,dec_deg);
		plan_4_1.setSubSequenTar_DIR("test");
		plan_4_1.setSubSequenTar_BINNING(new int []{1});
		plan_4_1.setSubSequenTar_INTERVAL(new double[]{40});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_4_1.setSubSequenTar_FILTER(new String []{equipment_filter});
		}
		NormalPlan plan_4_2=new NormalPlan("GRB-40S-2"+"-time",ra_ha,dec_deg);
		plan_4_2.setSubSequenTar_INTERVAL(new double[]{40});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_4_2.setSubSequenTar_FILTER(new String []{equipment_filter});
		}
		NormalPlan plan_4_3=new NormalPlan("GRB-40S-3"+"-time",ra_ha,dec_deg);
		plan_4_3.setSubSequenTar_INTERVAL(new double[]{40});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_4_3.setSubSequenTar_FILTER(new String []{equipment_filter});
		}

		NormalPlan plan_6_1=new NormalPlan("GRB-60S-1"+"-time",ra_ha,dec_deg);
		plan_6_1.setSubSequenTar_INTERVAL(new double[]{60});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_6_1.setSubSequenTar_FILTER(new String []{equipment_filter});
		}
		NormalPlan plan_6_2=new NormalPlan("GRB-60S-2"+"-time",ra_ha,dec_deg);
		plan_6_2.setSubSequenTar_INTERVAL(new double[]{60});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_6_2.setSubSequenTar_FILTER(new String []{equipment_filter});
		}
		NormalPlan plan_6_3=new NormalPlan("GRB-60S-3"+"-time",ra_ha,dec_deg);
		plan_6_3.setSubSequenTar_INTERVAL(new double[]{60});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_6_3.setSubSequenTar_FILTER(new String []{equipment_filter});
		}
		NormalPlan plan_6_4=new NormalPlan("GRB-60S-4"+"-time",ra_ha,dec_deg);
		plan_6_4.setSubSequenTar_INTERVAL(new double[]{60});
		if (null != equipment_filter && equipment_filter.length() > 0){
			plan_6_4.setSubSequenTar_FILTER(new String []{equipment_filter});
		}

		planFile.getTargetList().add(plan_4_1);
		planFile.getTargetList().add(plan_4_2);
		planFile.getTargetList().add(plan_4_3);
		planFile.getTargetList().add(plan_6_1);
		planFile.getTargetList().add(plan_6_2);
		planFile.getTargetList().add(plan_6_3);
		planFile.getTargetList().add(plan_6_4);

		for(int i=0;i<12;i++){
			NormalPlan plan_9s=new NormalPlan("GRB-90S-"+(i+1)+"-time",ra_ha,dec_deg);
			plan_9s.setSubSequenTar_INTERVAL(new double[]{90});
			if (null != equipment_filter && equipment_filter.length() > 0){
				plan_9s.setSubSequenTar_FILTER(new String []{equipment_filter});
			}

			planFile.getTargetList().add(plan_9s);
		}

		File stopedPlan=new File(HMTlastPlanFilePath);
		if (HMTlastPlanFilePath!=null&&HMTlastPlanFilePath.length()>0&&stopedPlan.exists()&&stopedPlan.isFile() && is_chain_stoped_plan) {
			planFile.setPaln_Chian(HMTlastPlanFilePath);
			log.info("#chain : last stoped plan file ="+HMTlastPlanFilePath);
		}
		return  planFile;
	}

}
