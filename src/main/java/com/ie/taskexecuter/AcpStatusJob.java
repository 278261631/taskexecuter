package com.ie.taskexecuter;

import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.python.util.PythonInterpreter;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.StandardWatchEventKinds.*;


@PersistJobDataAfterExecution
public class AcpStatusJob implements Job{

	private Logger log = LoggerFactory.getLogger(AcpStatusJob.class);

	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();

		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();


//		log.info(device_name+" ACP Status ");

		boolean isAtPark = false;
		try {
			ActiveXComponent pp = new ActiveXComponent("ACP.Telescope");
			Dispatch acpTelescope = (Dispatch) pp.getObject(); //生成一个对象
//			Variant result = Dispatch.call( acpTelescope, "MD5", str) ;
			Variant isConnected_String = Dispatch.get(acpTelescope ,"Connected");
			if(isConnected_String.getBoolean()){
				Variant isAtPark_String = Dispatch.get(acpTelescope ,"AtPark");
				isAtPark = isAtPark_String.getBoolean();
			}
//			log.info("ACP  连接  = " + isConnected_String.getBoolean());
//			log.info("Park       = " + isAtPark);
			DeviceStatus deviceStatus = new DeviceStatus("ParkStatus" , device_name ,device_name ,"{isACPCon:\""+isConnected_String.getBoolean()+"\",isPark:\""+isAtPark+"\"}" , sdf.format(new Date()),device_color , MessageColor.OK);

			AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatus).toString());
			isConnected_String.safeRelease();
			acpTelescope.safeRelease();
			//todo  test JProfiler increase
			pp.safeRelease();
		}catch (Exception e) {
			e.printStackTrace();
			log.info("isPark  " +e);
		}



	}

}
