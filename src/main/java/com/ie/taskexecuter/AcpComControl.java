package com.ie.taskexecuter;


import com.ie.taskexecuter.util.AcpStatus;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.SendEmail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.ie.taskexecuter.AcpControl.MyUser32.INSTANCE;
import static com.sun.jna.platform.win32.WinUser.SW_RESTORE;


/**
 * @author mars
 */
public class AcpComControl {


//	private static Log log = LogFactory.getLog(AcpComControl.class);
	private static Logger log = LoggerFactory.getLogger(AcpComControl.class);
	public static int printCount = 0;

	public static SimpleDateFormat sdf     =  new SimpleDateFormat("HH:mm:ss");
	public static SimpleDateFormat sdfUTC  =  new SimpleDateFormat("HH:mm:ss");
//	public static ActiveXComponent acpAppCom = new ActiveXComponent("ACP.Application");
//	public static ActiveXComponent acpTelescopeCom = new ActiveXComponent("ACP.Telescope");
//	public static ActiveXComponent acpUtilCom = new ActiveXComponent("ACP.Util");
//	public static Dispatch acpConsole = acpUtilCom.getProperty("Console").toDispatch();
//	public static ActiveXComponent acpConsoleCom = new ActiveXComponent("ACP.Console");
//	public static Dispatch acpTelescope = (Dispatch) acpTelescopeCom.getObject();
//	Variant Console = Dispatch.get(acpUtilCom ,"Console").getd;

	public static void RunPlan(String planPath , String scriptPath) {

		log.info("Plan File Path  " +planPath);
		log.info("Script Path  " +scriptPath);
		Dispatch acpUtil = new ActiveXComponent("ACP.Util").getObject();
//		Dispatch acpTelescope = new ActiveXComponent("ACP.Telescope").getObject();
		Dispatch acpConsole =Dispatch.get(acpUtil ,"Console").toDispatch();


		Dispatch.call(acpConsole,"Clear");
		Dispatch.call(acpUtil ,"LoadScript",scriptPath);
		Dispatch.call(acpConsole,"PrintLine" ,scriptPath);
		Dispatch.call(acpConsole,"PrintLine" ,planPath);
		Dispatch.put(acpConsole,"LogFile",planPath);
//		acputil.Console.PrintLine("E:/mayong/XTelescope.txt")
//		'这里使用的是logFile保存的执行计划的地址 这样做的原因是runScript无法传递这个参数
//		'并且 也没有想到什么好办法能从script内部获取这个参数
//		'好在 logFile在每次被使用的时候都会被重新指定路径
//		acputil.Console.LogFile = "E:/mayong/XTelescope.txt"

		Dispatch.call(acpUtil ,"RunScript");
		acpUtil.safeRelease();
	}



	public static void GetStatusIfRun() throws Exception{
		ActiveXComponent acpAPP = new ActiveXComponent("ACP.Application");
		log.info(acpAPP.getPropertyAsString("Path"));
		acpAPP.safeRelease();


	}

	public static AcpStatus GetStatus(String deviceName) throws Exception{
		List<String> acpProcessList = getPid();
		boolean isAcpRunning = false;
		if(acpProcessList.size() != 1){
//			SendEmail.sendToEmails("Error TaskExecutro",new String[]{"278261631@qq.com"},deviceName+"     Acp.exe 运行数量 = " + acpProcessList.size());
			log.info("ACP.exe 运行数量= " +acpProcessList.size());
			AcpStatus acpStatus = new AcpStatus(true,"","","","","","","","",""
					,"","","","","1",false,"99" ,isAcpRunning,"");
			return acpStatus;
		}
		isAcpRunning =true;
		Dispatch acpUtil = new ActiveXComponent("ACP.Util").getObject();
//		acpUtil.QueryInterface()
//		Dispatch acpLock = new ActiveXComponent("ACP.Lock").getObject();
		Dispatch acpTelescope = new ActiveXComponent("ACP.Telescope").getObject();
		Dispatch acpConsole =Dispatch.get(acpUtil ,"Console").toDispatch();
		Dispatch acpPrefs =Dispatch.get(acpUtil ,"Prefs").toDispatch();
		Dispatch autoguiding =Dispatch.get(acpPrefs,"Autoguiding").toDispatch();
		boolean autoguidingEnabled =Dispatch.get(autoguiding,"Enabled").getBoolean();

		boolean isTelescopeConnected = Dispatch.get(acpTelescope ,"Connected").getBoolean();
		boolean isCameraConnected  =Dispatch.get(acpUtil ,"CameraConnected").getBoolean();
		String obsLoc  =sdf.format(new Date());
		sdfUTC.setTimeZone(TimeZone.getTimeZone("GMT"));
		String obsUTC  =sdfUTC.format(new Date());
		String siderealTime =  String.valueOf(Dispatch.get(acpTelescope ,"SiderealTime").getDouble());
//		System.out.print(" ------------------------ siderealTime" + siderealTime);
		if(printCount/100 == 0){
			System.out.print("*");
		}else{
			System.out.println("*");
			printCount = 0;
		}
		printCount++;
		String obsST   =Dispatch.call(acpUtil ,"Hours_HMS",siderealTime).getString();
//		boolean isAcpLocked  =Dispatch.get(acpLock ,"Locked").getBoolean();
		String scriptPath=Dispatch.get(acpConsole,"Script").getString();
		String consoleText=Dispatch.get(acpConsole,"Text").getString();
//		log.info(consoleText);
		File scriptFile = new File(scriptPath);
		String scriptName= scriptFile.exists()? scriptFile.getName():"";


		boolean utilScriptActive =Dispatch.get(acpUtil ,"ScriptActive").getBoolean();
		Dispatch acpScript ;
		String scopeStatus="断开";
		double ExposureProgress=0;
		double ExposureInterval_Acp8=0;
		int ExposureInterval_Acp6=0;
		boolean autoFocusActive=false;
		boolean guiding=false;
		String guidingStatus="-";
		String trkGraphX="";
		String trkGraphY="";
		String trkGraphXY="";
		String plnFile = "";//需要修改AcquireImages.js 中的plnName变量位置
		String scriptVERSION = "";
		String plnName="";
		String cameraStatus="断开";
		String filterNames []=null;
		String imgTemp="关";
		String lastFWHM="-";
		int filterIndex=0;
		int cameraBinX=1;
		String nowFilterNmae="";
		String planProgress=".";
		String planRepeat=".";
		String planFilter=".";
		String planCount=".";
		String planTarget=".";

		if(utilScriptActive){

			//如果出现失败提示框
			WinDef.HWND parent = INSTANCE.FindWindow("#32770", "ACP Observatory Control Software");
			int WM_LBUTTONUP = 514;
			int WM_LBUTTONDOWN = 513;
//			log.info("parent " + (parent == null ? parent : parent.getPointer()));
			if (parent != null) {
				log.info("出现abort 失败窗口");
				WinDef.HWND btn_abortErrOk = INSTANCE.FindWindowEx(parent, null, "Button", "确定");
				log.info("btn_abortErrOk " + (btn_abortErrOk == null ? btn_abortErrOk : btn_abortErrOk.getPointer()));
				INSTANCE.ShowWindow(parent, SW_RESTORE);
				INSTANCE.SetForegroundWindow(parent);
				if (btn_abortErrOk != null) {
					log.info("按钮存在");
					WinUser.WINDOWINFO windowInfo = new WinUser.WINDOWINFO();
					boolean infoResult = INSTANCE.GetWindowInfo(btn_abortErrOk, windowInfo);
					log.info("btn_abortErrOk info:" + windowInfo.dwStyle + "-" +windowInfo.dwExStyle +"-" + windowInfo.dwWindowStatus);
					INSTANCE.SetFocus(btn_abortErrOk);
					if(windowInfo.dwStyle == 1342373889){
						log.info("windowInfo可用状态");
						WinDef.LPARAM l = new WinDef.LPARAM(5);
						WinDef.WPARAM w = new WinDef.WPARAM(5);
						INSTANCE.SendMessage(btn_abortErrOk,WM_LBUTTONDOWN , w, l);
						INSTANCE.SendMessage(btn_abortErrOk,WM_LBUTTONUP , w, l);
					}else {
						log.info("btn_abortErrOk  ?????????????");
					}
				}
			}


			acpScript = Dispatch.get(acpUtil ,"Script").toDispatch();

			Dispatch acpScriptSup = Dispatch.get(acpScript ,"SUP").toDispatch();
			scriptVERSION = Dispatch.get(acpScript, "SCRIPTVERSION").getString();
			if(scriptVERSION.startsWith("6")){}
			if(scriptVERSION.startsWith("8")){
				planProgress  = Dispatch.get(acpScript, "planProgress").getString(); //fixme 8.3 error
				planRepeat    = Dispatch.get(acpScript, "planRepeat").getString();
				planFilter    = Dispatch.get(acpScript, "planFilter").getString();
				planCount     = Dispatch.get(acpScript, "planCount").getString();
				planTarget    = Dispatch.get(acpScript, "planTarget").getString();
			}

			Variant lastFWHMDispatch = Dispatch.get(acpScript, "lastFWHM");
			short lastFWHMvt=lastFWHMDispatch.getvt();
			if(lastFWHMvt==Variant.VariantString){
				lastFWHM = lastFWHMDispatch.getString();
			}else if(lastFWHMvt==Variant.VariantShort){
				lastFWHM = String.valueOf(lastFWHMDispatch.getShort());
			}else if(lastFWHMvt==Variant.VariantInt){
				lastFWHM = String.valueOf(lastFWHMDispatch.getInt());
			}else if(lastFWHMvt==Variant.VariantEmpty || lastFWHMvt==Variant.VariantNull){
				lastFWHM = "Null";
			}else{
				log.info("lastFWHM type = " +String.valueOf(lastFWHMvt));
			}
//			try {
//				lastFWHM = Dispatch.get(acpScript, "lastFWHM").getString();
//			}catch (Exception e){log.info("lastFWHM Error",e);}
//			try {
//				lastFWHM = String.valueOf(Dispatch.get(acpScript, "lastFWHM").getShort());
//			}catch (Exception e){log.info("lastFWHM Error",e);}
//			scopeStatus = Dispatch.get(acpScriptSup, "scopeStat").getString();
			Variant autoFocusVT = Dispatch.get(acpScriptSup, "autoFocusActive");
			if(autoFocusVT.getvt() == Variant.VariantBoolean){
				autoFocusActive = autoFocusVT.getBoolean();
			}
//			Variant GuidingVT = Dispatch.get(acpScriptSup, "Guiding");
			boolean GuidingVT = false;
			try {
				GuidingVT=Dispatch.get(acpScriptSup, "Guiding").getBoolean();
			}catch (Exception e){log.info("GuidingVT Error" , e);}

			boolean ExposureActive = Dispatch.get(acpScriptSup, "ExposureActive").getBoolean();
//			if(GuidingVT.getvt() == Variant.VariantBoolean){
//				guiding = GuidingVT.getBoolean();
//			}
//			guiding = Dispatch.get(acpScriptSup, "Guiding").getBoolean();
			//ExposureProgress = Dispatch.get(acpScriptSup, "ExposureProgress").getDouble();

			if(scriptVERSION.startsWith("6")){
//				Dispatch guideErrHistX = Dispatch.get(acpScript, "guideErrHistX").toDispatch();
//				Dispatch guideErrHistY = Dispatch.get(acpScript, "guideErrHistY").toDispatch();
//				trkGraphX = Dispatch.call(guideErrHistX, "join",",").getString();
//				trkGraphY = Dispatch.call(guideErrHistY, "join",",").getString();
//				todo 可能需要合并 int double
//				ExposureInterval_Acp6 = Dispatch.get(acpScriptSup, "ExposureInterval").getInt();
			}
			if(scriptVERSION.startsWith("8")){
//				SUP.CaptureAGErrors(MAXTRKPTS);
				if(guiding){
					log.info("guiding acp8 :" ,guiding);
				}
				Dispatch.call(acpScriptSup,"CaptureAGErrors" ,79);
				trkGraphX = Dispatch.get(acpScriptSup, "AGErrorListX").getString();
				trkGraphY = Dispatch.get(acpScriptSup, "AGErrorListY").getString();
				trkGraphXY = Dispatch.get(acpScriptSup, "AGErrorListXY").getString();
//				ExposureInterval_Acp8 = Dispatch.get(acpScriptSup, "ExposureInterval").getDouble();
			}
			plnFile = Dispatch.get(acpScript, "plnFileInScript").getString();
			File plnFileObj = new File(plnFile);
			log.info("---------------------------" + plnFile);
			if(plnFileObj.exists()){plnName=plnFileObj.getName();plnName=plnName.replace(".txt","");}
//			log.info("scopeStat   " + scopeStat);
			if(isCameraConnected){
				cameraStatus = "连接";
				//MaxIm 会自己开一个新的 所以只能用 acp内置的
//				Dispatch maxdlCamera = new ActiveXComponent("MaxIm.CCDCamera").getObject();
				Dispatch maxdlCamera = Dispatch.get(acpUtil,"ScriptCamera").toDispatch();
				int maxdlCameraStatus=Dispatch.get(maxdlCamera,"CameraStatus").getInt();
				filterNames = Dispatch.get(maxdlCamera, "FilterNames").toSafeArray().toStringArray();
				List<String> result = new ArrayList<String>();
				filterIndex=Dispatch.get(maxdlCamera,"Filter").getInt();
				cameraBinX=Dispatch.get(maxdlCamera,"BinX").getInt();
				boolean CoolerOn=Dispatch.get(maxdlCamera,"CoolerOn").getBoolean();


//				Double GuiderXError=Dispatch.get(maxdlCamera,"GuiderXError").getDouble();
//				Double GuiderYError=Dispatch.get(maxdlCamera,"GuiderYError").getDouble();
//				System.out.println("GuiderXError" + GuiderXError);
//				System.out.println("GuiderYError" + GuiderYError);
				if (filterNames != null && filterNames.length>0){
					nowFilterNmae=filterNames[filterIndex];
				}

				if(autoFocusActive){
					cameraStatus="Autofocus Busy";
				}else if(scriptName.toUpperCase().equals("autoflat")){
					cameraStatus= "Short Exposures";
				}else{
					switch (maxdlCameraStatus){
						case 1:
							cameraStatus = "Error";
							break;
						case 2:
							cameraStatus = "Idle";
							break;
						case 3:
						case 12:
						case 13:
//							todo exp interval double  int  0
							if(scriptVERSION.startsWith("6")){
//								cameraStatus = "Exp " + Math.floor(ExposureProgress * ExposureInterval_Acp6) + "/" + ExposureInterval_Acp6 + " s";
							}
							if(scriptVERSION.startsWith("8")){
//								cameraStatus = "Exp " + Math.floor(ExposureProgress * ExposureInterval_Acp8) + "/" + ExposureInterval_Acp8 + " s";
							}
							//todo 完全不知道是int 还是double  感觉给出来的时候就是两种  干脆看看能不能获取String吧
							cameraStatus ="?";
//							expProg = ExposureProgress;
							break;
						case 4:
							cameraStatus = "Reading";
							break;
						case 5:
							cameraStatus = "Download";
							break;
						case 6:
							cameraStatus = "Flushing";
							break;
						case 7:
						case 8:
						case 9:
							cameraStatus = "Waiting";
							break;
						case 10:
							cameraStatus = "AutoDark";
							break;
						case 11:
							cameraStatus = "BiasFrame";
							break;
						case 15:
							cameraStatus = "FiltSwitch";
							break;
						default:
							cameraStatus = "Waiting";
							break;
					}
				}

				if(CoolerOn) {
//					Variant coolerPowerDispatch = Dispatch.get(acpScript, "CoolerPower");
//					Variant temperatureTemperatureDispatch = Dispatch.get(acpScript, "Temperature");
//					short coolerPowerDispatch=lastFWHMDispatch.getvt();
//					if(lastFWHMvt==Variant.VariantString){
//						lastFWHM = lastFWHMDispatch.getString();
//					}else if(lastFWHMvt==Variant.VariantShort){
//						lastFWHM = String.valueOf(lastFWHMDispatch.getShort());
//					}else{
//						log.info("lastFWHM type = " +String.valueOf(lastFWHMvt));
//					}

					Short p=Dispatch.get(maxdlCamera,"CoolerPower").getShort();
					Double z=Dispatch.get(maxdlCamera,"Temperature").getDouble();
						if(z == 20.0){
							imgTemp = "等待";                              // Exactly 20 during startup, wait till changed
						}
						else if(z < 100.0) {                                    // Huge numbers mean cooler off
							imgTemp = z.toString()+"ºC  / " + p + "%";  // OK, can get temp
						}
				}

				if(autoguidingEnabled){
					if(ExposureActive && GuidingVT){
						guidingStatus="导星...";
					}else {guidingStatus="空闲";}
				}else {
					guidingStatus="导星...";
				}


				maxdlCamera.safeRelease();
			}

			acpScript.safeRelease();
			acpScriptSup.safeRelease();
			lastFWHMDispatch.safeRelease();
			autoFocusVT.safeRelease();

		}
//		Dispatch.call(acpConsole ,"PrintLine","xxxxyyy");
//		log.info("-------------------------COM  ----------------------------   Telescope "  + isTelescopeConnected + "  Camera  " + isCameraConnected );
//		log.info("  Locked "  + isAcpLocked + "  Camera  " + isCameraConnected );

		boolean isObsOffLine = true;
		if (isTelescopeConnected && isCameraConnected){isObsOffLine = false;}


//		log.info( "trkGraphX =" +trkGraphX);
//		log.info( "trkGraphY =" +trkGraphY);

		AcpStatus acpStatus = new AcpStatus(isObsOffLine,obsLoc,obsUTC,obsST,scopeStatus,scriptPath,scriptName,plnFile,plnName,cameraStatus
			,trkGraphX,trkGraphY,trkGraphXY,nowFilterNmae,String.valueOf(cameraBinX),utilScriptActive,scriptVERSION ,isAcpRunning,consoleText);
		if(isTelescopeConnected){
			scopeStatus="连接";
			String az = String.valueOf(Dispatch.get(acpTelescope ,"Azimuth").getDouble());
			String alt = String.valueOf(Dispatch.get(acpTelescope ,"Altitude").getDouble());
	//		String ha = Dispatch.get(acpTelescope ,"Azimuth").getString();
			String ra = String.valueOf(Dispatch.get(acpTelescope ,"RightAscension").getDouble());
			String dec = String.valueOf(Dispatch.get(acpTelescope ,"Declination").getDouble());
			acpStatus.setSpan_scope_az_(az);
			acpStatus.setSpan_scope_alt_(alt);
			acpStatus.setSpan_scope_ra_(ra);
			acpStatus.setSpan_scope_dec_(dec);
			acpStatus.setCameraCooler(imgTemp);
			acpStatus.setLastFWHM(lastFWHM);
			acpStatus.setS_sm_guideStat(guidingStatus);
			String telescopeAlignmentMode = String.valueOf(Dispatch.get(acpTelescope ,"AlignmentMode").getInt());
			boolean telescopeSlewing =  Dispatch.get(acpTelescope ,"Slewing").getBoolean();
			boolean telescopeTracking =  Dispatch.get(acpTelescope ,"Tracking").getBoolean();
			if(telescopeSlewing){
					scopeStatus="Slewing";
			}
			if(telescopeTracking) {
					scopeStatus="Tracking";
			}
			acpStatus.setSpan_scope_status_(scopeStatus);

//			if(utilScriptActive){
//				if(telescopeSlewing){
//
//				}else{
//					if(telescopeSlewing){
//						scopeStatus="Slewing";
//					}else if(telescopeTracking){
//						scopeStatus="Sidereal Track";
//					}else {
//						scopeStatus="Stopped";
//					}
//				}
//			}else {
//				if(telescopeSlewing){
//					scopeStatus=
//				}
//			}
			String scopeGEM;
//			Software pier-side detection not initialized.
//			String telescopeSideOfPier = Dispatch.get(acpTelescope ,"SideOfPier").getString();
			boolean gemVis;
//			if (telescopeAlignmentMode=="2"){
//				 gemVis= true;
//				if (telescopeSideOfPier=="0"){
//					scopeGEM = "West";
//				}else {
//					scopeGEM = "East";
//				}
//			}

		}

		acpConsole.safeRelease();
		acpPrefs.safeRelease();
		autoguiding.safeRelease();



		acpUtil.safeRelease();
		acpTelescope.safeRelease();
		acpStatus.setPlanProgress(planProgress);
		acpStatus.setPlanRepeat(planRepeat);
		acpStatus.setPlanFilter(planFilter);
		acpStatus.setPlanCount(planCount);
		acpStatus.setPalnTarget(planTarget);
		return acpStatus;
	}

	public static void StopPlan() {
		Dispatch acpUtil = new ActiveXComponent("ACP.Util").getObject();
		Dispatch acpConsole =Dispatch.get(acpUtil ,"Console").toDispatch();

		boolean utilScriptActive =Dispatch.get(acpUtil ,"ScriptActive").getBoolean();
		if(utilScriptActive){
			Dispatch.call(acpUtil ,"AbortScript");
			Dispatch.call(acpConsole,"PrintLine" ,"Abort Executed");
		}else {
			Dispatch.call(acpConsole,"PrintLine" ,"Abort Not Executed ,No Script Running");
		}
		acpUtil.safeRelease();

	}
	public static void StopPlanHack() {
		// 本地强制点击停止按钮
		//acp6发现有网页端无法在曝光时强制停止的情况，这里加一个打开ACP页面点击Abort按钮，需要在本机环境
		WinDef.HWND parent = INSTANCE.FindWindow("ThunderRT6FormDC", "ACP Observatory Control Software");
		log.info("parent " + (parent == null ? parent : parent.getPointer()));

		if (parent != null) {
			WinDef.HWND consoleFrame = INSTANCE.FindWindowEx(parent, null, "ThunderRT6Frame", "Console");
			if(consoleFrame != null){
				log.info("consoleFrame " + (consoleFrame == null ? consoleFrame : consoleFrame.getPointer()));
				WinDef.HWND btn_abort = INSTANCE.FindWindowEx(consoleFrame, null, "ThunderRT6CommandButton", "Abort");
				log.info("btn_abort " + (btn_abort == null ? btn_abort : btn_abort.getPointer()));

				INSTANCE.ShowWindow(parent, SW_RESTORE);
				INSTANCE.SetForegroundWindow(parent);
				if (btn_abort != null) {
					WinUser.WINDOWINFO windowInfo = new WinUser.WINDOWINFO();
					boolean infoResult = INSTANCE.GetWindowInfo(btn_abort, windowInfo);
					log.info("btn_abort info:" + windowInfo.dwStyle + "-" +windowInfo.dwExStyle +"-" + windowInfo.dwWindowStatus);
					INSTANCE.SetFocus(btn_abort);
					if(windowInfo.dwStyle == 1543577600){
						log.info("abort不可用");
					}
					if(windowInfo.dwStyle == 1409359872){
						log.info("abort可用");
						final int WM_LBUTTONUP = 514;
						final int WM_LBUTTONDOWN = 513;
						WinDef.LPARAM l = new WinDef.LPARAM(5);
						WinDef.WPARAM w = new WinDef.WPARAM(5);
						INSTANCE.SendMessage(btn_abort,WM_LBUTTONDOWN , w, l);
						INSTANCE.SendMessage(btn_abort,WM_LBUTTONUP , w, l);
						log.info("已经执行abort点击");
					}

				}
			}

		}
	}
	public static void ConsoleLog(String logString) {
		Dispatch acpUtil = new ActiveXComponent("ACP.Util").getObject();
		Dispatch acpConsole =Dispatch.get(acpUtil ,"Console").toDispatch();
		Dispatch.call(acpConsole,"PrintLine" ,logString);
		acpUtil.safeRelease();
	}

	public static List<String> getPid(String name) {
		BufferedReader bufferedReader = null;
		List<String> list = new ArrayList();
		try{
			java.lang.Process process = Runtime.getRuntime().exec("tasklist /fi \"imagename eq " + name + "\" /fo list");
//			System.out.println("tasklist /fi \"imagename eq " + name + "\" /fo list");

			bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String str;
			while ((str = bufferedReader.readLine()) != null){
//				log.info("process=" +str);
				if (str.startsWith("PID")){
					String[] array = str.split(":");
					String pid = array[1].trim();
					list.add(pid);
				}
			}
			process.destroy();
		}catch (IOException e){
			log.info("getPID  ",e);
			e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
			} catch (IOException e) {
				log.info("BufferReader " , e);
//				System.out.println(e.toString());
			}
		}

		return list;
	}

	public static List<String> getPid() {
		return getPid("acp.exe");
	}


}

