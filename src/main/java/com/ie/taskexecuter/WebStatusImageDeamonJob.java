package com.ie.taskexecuter;

import com.github.sarxos.webcam.Webcam;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.ImgBase64Encoder;
import com.ie.sysmessage.MessageColor;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@PersistJobDataAfterExecution
public class WebStatusImageDeamonJob implements Job{
	
	private Log log = LogFactory.getLog(WebStatusImageDeamonJob.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();

		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();
		String statusImageRoot=	       data.get("statusImageRoot").toString();
		boolean isDeleteStatusImage=	       data.getBooleanValue("isDeleteStatusImage");

		log.info("本设备 "+device_name+" 读取监视图像 ");
		DeviceStatus deviceStatus = new DeviceStatus("WebCamera" , device_name ,device_name ,"更新摄像头影像" , sdf.format(new Date()),device_color , MessageColor.OK);

		File fileRoot = new File(statusImageRoot);
		if (!fileRoot.exists()){
			log.error("dir not exists   目录不存在  "+statusImageRoot );
			fileRoot.mkdirs();
		}
		if (fileRoot.isFile()){
			log.error(" statusImageRoot 应该是路径而不是文件  "+statusImageRoot );
			fileRoot = new File(statusImageRoot).getParentFile();
		}
		File[] imagelist = fileRoot.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File f, String fname) {
				return fname.toLowerCase().endsWith(".jpg") || fname.toLowerCase().endsWith(".jpeg") || fname.toLowerCase().endsWith(".png")
						|| fname.toLowerCase().endsWith(".webp") || fname.toLowerCase().endsWith(".bmp");
			}
		});
		if (imagelist.length<1){
			log.error("没有图像 jpg jpeg png webp bmp");
			return ;
		}

		try {
			BufferedImage buffimg = ImageIO.read(imagelist[0]);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(buffimg, "png", baos);
            byte[] bytes = baos.toByteArray();
			deviceStatus.setCameraImage(ImgBase64Encoder.encode(bytes));
			AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (imagelist.length>0 && isDeleteStatusImage){
			log.info("删除图像 jpg jpeg png webp bmp");
			for (File fileToDel: imagelist ) {
				log.info("Delete Image  删除图像 "  +fileToDel.getAbsolutePath());
				fileToDel.delete();
			}
			return ;
		}

	   
	   
		
	}

}
