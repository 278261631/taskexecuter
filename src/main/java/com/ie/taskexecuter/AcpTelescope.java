package com.ie.taskexecuter;

public class AcpTelescope{
	private boolean isConnected =false;

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean connected) {
		isConnected = connected;
	}
}
