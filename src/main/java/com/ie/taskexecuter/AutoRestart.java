package com.ie.taskexecuter;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


@PersistJobDataAfterExecution
public class AutoRestart implements Job{

	private Logger log = LoggerFactory.getLogger(AutoRestart.class);

	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();
		log.info(" >>>>>>>>>>>>>>>>>>>>>>  restart  <<<<<<<<<<<<<<<<<<<<<<< ");
		try {
			restartApplication();
		} catch (IOException e) {
			e.printStackTrace();
			log.info("restart IO:", e);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			log.info("restart URISyntax:" ,e);
		}
	}


	public void restartApplication() throws IOException, URISyntaxException {
		final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
//		final File currentJar = new File(TaskReceiver.class.getProtectionDomain().getCodeSource().getLocation().toURI());
		final File currentJar = new File(String.valueOf(TaskReceiver.class.getProtectionDomain().getCodeSource().getLocation()));
//        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("/templates" + url);

        URL jarUrl = new URL(String.valueOf(TaskReceiver.class.getProtectionDomain().getCodeSource().getLocation()));
        JarURLConnection connection = (JarURLConnection) jarUrl.openConnection();
//        File file = new File(connection.getJarFileURL().toURI())
        String jarFileString = String.valueOf(connection.getJarFile().getName());
        String jarPath = jarFileString.substring(0, jarFileString.indexOf("!"));
		log.info(jarPath);
		log.info(currentJar.getName());
		log.info(javaBin);


//		final ArrayList<String> command = new ArrayList<String>();
//		command.add("notepad");
//		command.add("D:\\java\\jdk1.8.0_101\\bin\\java -jar D:\\gitroot\\taskexecuter\\target\\taskexecuter-0.2.1b.jar");
//		command.add("D:\\gitroot\\taskexecuter\\target\\runJar.bat");
//        command.add("cmd");
//        command.add("/k");
//        command.add("title");
//        command.add("TaskExecuter");
//        command.add("D:\\java\\jdk1.8.0_101\\bin\\java");
//        command.add("-jar");
//        command.add("D:\\gitroot\\taskexecuter\\target\\taskexecuter-0.2.1b.jar");


//		command.add("^&");
//		command.add(javaBin);
//		command.add("-jar");
//		command.add("-XX:+HeapDumpOnOutOfMemoryError");
//		command.add(currentJar.getPath());

//		final ProcessBuilder builder = new ProcessBuilder(command);
//		builder.start();
        runProcess();
        System.exit(0);
	}
    SimpleDateFormat sdf_schtasks=new SimpleDateFormat("HH:mm:ss");
    public void runProcess() {

        try {
            log.info("-------");
//            Runtime.getRuntime().exec(" cmd /c  D:\\gitroot\\taskexecuter\\target\\runJar.bat");
//            Runtime.getRuntime().exec(" cmd /k  D:\\java\\jdk1.8.0_101\\bin\\java -jar D:\\gitroot\\taskexecuter\\target\\taskexecuter-0.2.1b.jar");
//            Runtime.getRuntime().exec("schtasks  /Delete  /tn  GW_GRB_Task");
            java.util.Calendar rightNow = java.util.Calendar.getInstance();
            rightNow.setTime(new Date());
            rightNow.add(Calendar.SECOND,5);
            Date dtAdd=rightNow.getTime();
            String reStr = sdf_schtasks.format(dtAdd);
            log.info(sdf.format(dtAdd));
            Runtime.getRuntime().exec("SCHTASKS /run /TN GW_GRB_Task ");
            log.info("-------");
        } catch (IOException e) {
            e.printStackTrace();
            log.info("restart: ",e);
        }
    }

}
