//package com.ie.taskexecuter;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.apache.activemq.ActiveMQConnection;
//import org.apache.activemq.ActiveMQConnectionFactory;
//import org.apache.commons.configuration2.AbstractConfiguration;
//import org.apache.commons.configuration2.Configuration;
//import org.apache.commons.configuration2.builder.fluent.Configurations;
//import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
//import org.apache.commons.configuration2.ex.ConfigurationException;
//
//import javax.annotation.PostConstruct;
//import javax.jms.*;
//import java.io.File;
//import org.springframework.stereotype.*;
//import org.springframework.beans.factory.annotation.*;
//@ConfigurationProperties
//@Component
//public class EquipmentStatusUpdater implements CommandLineRunner{
//
//    private ConnectionFactory connectionFactory;
//    private Connection connection = null;
//    private Session session;
//    private Destination destination;
//    private MessageConsumer consumer;
//	@Value("${msg_url_sp}")
//	private  String  msg_url_sp;
//	
//	public void start() throws JMSException  {
//
//	}
//	
//    @Autowired
//    public EquipmentStatusUpdater(@Value("${msg_url_sp}") String msg_url_sp) {
//        this.msg_url_sp = msg_url_sp;
//        System.out.println("================== " + msg_url_sp + "================== ");
//    }
//   
//
//	@Override
//	public void run(String... arg0) throws Exception {
//		System.out.println("-------------------------------------------------");
//		System.out.println(msg_url_sp);
//		
//		Configurations configs = new Configurations();
//        Configuration config = null;
//        try {
//            config = configs.properties(new File("sys_config.properties"));
//        } catch (ConfigurationException e) {
//            e.printStackTrace();
//        }
//        ((AbstractConfiguration) config).setListDelimiterHandler(new DefaultListDelimiterHandler(','));
//        String msg_url = config.getString("msg_url","tcp://127.0.0.1:61616");
//        String msg_name = config.getString("msg_name","loginTopic");
//        String encrypt_key = config.getString("encrypt_key","");
////        log.debug("msg_url : "  + msg_url);
////        log.debug("msg_name : "+msg_name);
////        log.debug("encrypt_key : "+encrypt_key);
//
//        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, msg_url);
//        connection = connectionFactory.createConnection();
//        connection.start();
//        session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
//        destination = session.createTopic(msg_name);
//        consumer = session.createConsumer(destination);
//
//        consumer.setMessageListener(new MessageListener() {
//            @Override
//            public void onMessage(Message message) {
//
//            System.out.println("--------接收到消息-------"+ message);
//                if (message instanceof TextMessage ) {
//                    try {
//                        String textMessage=((TextMessage) message).getText();
//                        System.out.println(textMessage);
//                    } catch (JMSException e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//            }
//        });
//		
//	}
//}
