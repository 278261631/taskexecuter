package com.ie.taskexecuter;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.ie.sysmessage.SysMessage;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;

import com.ie.sysmessage.TaskData;
import com.ie.taskexecuter.util.Equatorial_To_Horizontal;
import com.ie.taskexecuter.util.Horizontal_To_Equatorial;
import com.ie.taskexecuter.util.dms;
import com.ie.taskexecuter.util.hmsm;
import com.ie.plangen.remoteobs.asputil.plangen.PlanCheckExeption;
import com.ie.plangen.remoteobs.asputil.plangen.PlanFileContent;
import com.ie.plangen.remoteobs.asputil.plangen.plantarget.NormalPlan;



@PersistJobDataAfterExecution 
public class EquipmentDeamonJob implements Job{
	
	private Log log = LogFactory.getLog(EquipmentDeamonJob.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();
	    
//	    TaskCommandEnum taskCommand=(TaskCommandEnum) data.getOrDefault("SysConfig.task_name", TaskCommandEnum.ReportStatus);
	    String pre_task_name  = data.getOrDefault("pre_task_name", "default").toString();
	    String next_task_name = data.get("next_task_name").toString();
	    String pre_task_level = data.getOrDefault("pre_task_level", "0").toString();
	    String next_task_level= data.getOrDefault("next_task_level", "0").toString();
//	    data.put("xxxx", "xxxx");
//	    data.put("xxxx","yyyyy");
//	    JobDataMap data_merge = context.getJobDetail().getJobDataMap();
//		System.out.println(">>>>>>>>>>>>>>>>>>"+next_task_name);
//		System.out.println(">>>>>>>>>>>>>>>>>>"+data_merge.get("next_task_name").toString());
//		System.out.println(">>>>>>>>>>>>>>>>>>"+data_merge.get("xxxx"));
	    
		String equipment_lastPlanDir=	   data.get("equipment_lastPlanDir").toString();
		String equipment_latitude=	       data.get("equipment_latitude").toString();
		String equipment_longitude=	       data.get("equipment_longitude").toString();
		String equipment_User=	           data.get("equipment_User").toString();
		String equipment_Pass=	           data.get("equipment_Pass").toString();
		String equipment_LimitAngle_all=   data.get("equipment_LimitAngle_all").toString();
		String equipment_Url=              data.get("equipment_Url").toString();
		String equipment_PlanPath=	       data.get("equipment_PlanPath").toString();
		String equipment_filter=	       data.get("equipment_filter").toString();
		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();

		String equipment_safe_east_alt=	       data.get("equipment_safe_east_alt").toString();
		String equipment_safe_east_az=	       data.get("equipment_safe_east_az").toString();
		String equipment_safe_west_alt=	       data.get("equipment_safe_west_alt").toString();
		String equipment_safe_west_az=	       data.get("equipment_safe_west_az").toString();
		
		Map<String, String> resultMap = AcpStatusUpdater.getSystemStatus(equipment_Url,equipment_User,equipment_Pass);
//		log.debug(resultMap);
		log.info(resultMap.get("s_sm_scopeStat"));
//		log.debug(resultMap.get("s_sm_obsStat"));
//		log.debug(resultMap.get("s_sm_plnTitle"));
//		log.debug(resultMap.get("s_sm_az"));
//		log.debug(resultMap.get("s_sm_alt"));
//		log.debug(resultMap.get("p_sm_expProg"));
//		log.debug(resultMap.get("c_value"));
//		log.debug(resultMap.get("v_sm_imgTempRow"));
		String eqp_status=resultMap.get("s_sm_scopeStat");
		String s_sm_obsStat=resultMap.get("s_sm_obsStat");
		String s_sm_camStat=resultMap.get("s_sm_camStat");
		boolean isObsOffLine = AcpStatusUpdater.isReadyToNext(resultMap );
		data.put("equipment_status", eqp_status);
//		log.debug("-----"+eqp_status);
		//todo check if obs offline

	   if (next_task_name.equals("")) {
		   if(eqp_status == null  ){
			   DeviceStatus deviceStatus = new DeviceStatus("设备错误" , device_name ,device_name ," 无法获取到设备状态 " +equipment_Url+"@" +equipment_User
					   , sdf.format(new Date()) ,device_color , MessageColor.Error);
			   deviceStatus.setObsOffLine(isObsOffLine);
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
			   log.info(device_name +" 无法获取到设备状态 " +equipment_Url+"@" +equipment_User  );
			   return;
		   }
		   if( !isObsOffLine ){
			   DeviceStatus deviceStatus = new DeviceStatus("设备离线" , device_name, device_name ," 设备离线 赤道仪" +eqp_status +" 相机 " +s_sm_camStat
					   , sdf.format(new Date()) ,device_color , MessageColor.Error);
			   deviceStatus.setObsOffLine(isObsOffLine);
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
			   log.info(device_name +" 设备离线 赤道仪" +eqp_status +" 相机 " +s_sm_camStat  );
			   return;
		   }

		   if (eqp_status.equals("@anStopped")) { //todo stoped or inuse?
			   log.info(">>>>>>>>>>>>>>>>>>>>>>avilable");
//			   String jsonEquipmentMessage="{\"type\":\"update\",\"value\":\""+eqp_status+"\"}";
//			   sendMessage(data, msg_name_equipment_status,jsonEquipmentMessage);
			   DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name, device_name ,"  "+"空闲" , sdf.format(new Date()),device_color , MessageColor.OK);
			   deviceStatus.setObsOffLine(isObsOffLine);
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
			   //todo relase obs and init pre_task_name and next_task_name
//			   if("ownner".equals("Free")){relase}
			    data.put("pre_task_name", "default");
			    data.put("pre_task_level", "0");
//			    TODO release 
			   return;			
		   }else{
			   log.info(">>>>>>>>>>>>>>>>>>>>>>running");
//			   String jsonEquipmentMessage="{\"type\":\"update\",\"value\":"+eqp_status+"  "+pre_task_name+"\"}";
//			   sendMessage(data, msg_name_equipment_status,jsonEquipmentMessage);
			   DeviceStatus deviceStatus = new DeviceStatus("更新状态" , device_name, device_name , "运行中" , sdf.format(new Date()),device_color , MessageColor.OK);
			   deviceStatus.setObsOffLine(isObsOffLine);
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
			   return ;
		   }
	   }else{
		   if (Integer.parseInt(next_task_level)>Integer.parseInt(pre_task_level)) {
//			   String jsonEquipmentMessage="{\"type\":\"info\",\"value\": \"cover task "+pre_task_name+"<"+next_task_level+"\"}";
//			   sendMessage(data, msg_name_equipment_status,jsonEquipmentMessage);
			   DeviceStatus deviceStatus = new DeviceStatus("任务消息" , device_name, device_name ,"任务覆盖 "+pre_task_name+" < "+next_task_name , sdf.format(new Date()),device_color , MessageColor.OK);
			   deviceStatus.setObsOffLine(isObsOffLine);
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
			   if (eqp_status.equals("@anStopped")) {
				   log.info(">>>>>>>>>>>>>>>>>>>>>>run new");
//					TODO add extra task before run
//			TODO  release obs if it used by others / Or  every one use this to run	   
					
	
				   TaskData taskData = (TaskData) data.get("task_data");
//					System.out.println("-----"+sm_ra+"-x-"+stringRa+"-x-"+sm_dec+"-x-"+stringDec +"----------");
//					String task_ra_h = data.get("task_ra_h").toString();
//					String task_ra_m = data.get("task_ra_m").toString();
//					String task_ra_s = data.get("task_ra_s").toString();
//					//TODO convert need change to ignore error 
//					jsky.coords.HMS task_ra_HMS  = new jsky.coords.HMS(Double.parseDouble(task_ra_h),Integer.parseInt(task_ra_m),Double.parseDouble(task_ra_s));
//					String task_dec_d= data.get("task_dec_d").toString();
//					String task_dec_m= data.get("task_dec_m").toString();
//					String task_dec_s= data.get("task_dec_s").toString();
//					jsky.coords.DMS task_dec_DMS=new jsky.coords.DMS(Integer.parseInt(task_dec_d),Integer.parseInt(task_dec_m),Integer.parseInt(task_dec_s));
//					Calendar caNow=Calendar.getInstance();
//					jsky.coords.WorldCoords wcRaDec=new jsky.coords.WorldCoords(task_ra_HMS.getVal(), task_dec_DMS.getVal(),2000.);
//					double[] arCurrent= wcRaDec.getRaDec(caNow.get(Calendar.YEAR));
//					double[] ar1950= wcRaDec.getRaDec(1950);
//					jsky.coords.WorldCoords wcRaDecCurrent = new jsky.coords.WorldCoords(arCurrent[0],arCurrent[1]);
//					jsky.coords.WorldCoords wcRaDec1950 = new jsky.coords.WorldCoords(ar1950[0], ar1950[1]);
					
					
//					 String Event_Ra_J2000=wcRaDec.getRaDeg()+"d\t{"+wcRaDec.getRA().toString()+"}\t(J2000)";
//					 String Event_Ra_Current=wcRaDecCurrent.getRaDeg()+"d\t{"+wcRaDecCurrent.getRA().toString()+"}\t(current)";
//					 String Event_Ra_J1950=wcRaDec1950.getRaDeg()+"d\t{"+wcRaDec1950.getRA().toString()+"}\t(1950)";
//					
//					 String Event_Dec_J2000=wcRaDec.getDecDeg()+"d\t{"+wcRaDec.getDec().toString()+"}\t(J2000)";
//					 String Event_Dec_Current=wcRaDecCurrent.getDecDeg()+"d\t{"+wcRaDecCurrent.getDec().toString()+"}\t(current)";
//					 String Event_Dec_J1950=wcRaDec1950.getDecDeg()+"d\t{"+wcRaDec1950.getDec().toString()+"}\t(1950)";
					
				
//					 calculations.SimpleCoordinatesConverter converter =new calculations.SimpleCoordinatesConverter();
//					 
//					 double current_alt=converter.convertRaDecToAlt(wcRaDec.getDecDeg(), Double.parseDouble(equipment_latitude), wcRaDec.getRaDeg());;
//					 double current_az=converter.convertRaDecToAz(wcRaDec.getDecDeg(), Double.parseDouble(equipment_latitude), current_alt, wcRaDec.getRaDeg());
//					System.out.println("-------"+current_alt);
//					System.out.println("-------"+current_az);
					 
				    //  hmt plugin?       >_<
//					hmsm hmsm_ra=new hmsm(Integer.parseInt(task_ra_h),Integer.parseInt(task_ra_m),Integer.parseInt(task_ra_s),0);
//					dms dms_dec=new  dms(Integer.parseInt(task_dec_d),Integer.parseInt(task_dec_m),Integer.parseInt(task_dec_s));
					hmsm hmsm_ra=new hmsm(Math.toRadians(taskData.getTask_Ra_deg()));
					dms dms_dec=new  dms(Math.toRadians(taskData.getTask_Dec_deg()));
					Double lat=Double.parseDouble(equipment_latitude),longt=Double.parseDouble(equipment_longitude);
					Calendar calendar=new GregorianCalendar();
					System.out.println(calendar);
					System.out.println(hmsm_ra);
					System.out.println(dms_dec);
					System.out.println(lat);
					System.out.println(longt);
					
					double[] altAz_rad= Equatorial_To_Horizontal.Convert_E2H(hmsm_ra, dms_dec,lat,longt,calendar);
					double[] altAz_deg=new double[]{Math.toDegrees(altAz_rad[0]),Math.toDegrees(altAz_rad[1])};
					System.out.println("-------"+altAz_deg[0]);
					System.out.println("-------"+altAz_deg[1]);
					if (altAz_deg[0]<Double.parseDouble(equipment_LimitAngle_all)) {
						System.out.println("HMT  Will Hit The Wall : alt " +altAz_deg[0] +"  az "+altAz_deg[1]);
						stopJob(data ,next_task_name ,next_task_level);
						return;
					}
					double s_sm_az,s_sm_alt;
					try {
						String s_sm_az_eqp  =URLDecoder.decode(resultMap.get("s_sm_az"),"UTF-8");
						String s_sm_alt_eqp =URLDecoder.decode(resultMap.get("s_sm_alt"),"UTF-8");
						String regexp_double = "[\\d\\.]+";
				        Pattern pattern_double = Pattern.compile(regexp_double); 
						Matcher matcher_double_az = pattern_double.matcher(s_sm_az_eqp);
						Matcher matcher_double_alt = pattern_double.matcher(s_sm_alt_eqp);
				        if (matcher_double_az.find()) {s_sm_az_eqp = matcher_double_az.group(0);}
				        if (matcher_double_alt.find()) {s_sm_alt_eqp = matcher_double_alt.group(0);}
				        s_sm_az =  Double.parseDouble(s_sm_az_eqp);
				        s_sm_alt = Double.parseDouble(s_sm_alt_eqp);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						stopJob(data ,next_task_name ,next_task_level);
						return;
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						stopJob(data ,next_task_name ,next_task_level);
						return;
					}
				   log.info("s_sm_az:  "+s_sm_az);
				   log.info("s_sm_alt: "+s_sm_alt);
				   
//				   hmsm safe_hmsm_ra;dms safe_dms_dec;
				   double safe_ra_rad,safe_dec_rad;
					if (s_sm_az>=180) {
					  	double [] radec_w=Horizontal_To_Equatorial.Convert_H2E(Double.parseDouble(equipment_safe_west_alt),
					  			Double.parseDouble(equipment_safe_west_az), lat, longt, calendar);
//					  	safe_hmsm_ra=new hmsm(radec_w[0]);
//					  	safe_dms_dec=new  dms(radec_w[1]);
					  	safe_ra_rad=radec_w[0];
					  	safe_dec_rad =radec_w[1];
					}else {
						double [] radec_e=Horizontal_To_Equatorial.Convert_H2E(Double.parseDouble(equipment_safe_east_alt),
								Double.parseDouble(equipment_safe_east_az), lat, longt, calendar);	
//						safe_hmsm_ra=new hmsm(radec_e[0]);
//					  	safe_dms_dec=new  dms(radec_e[1]);
						safe_ra_rad=radec_e[0];
						safe_dec_rad=radec_e[1];
					}
					log.info("safe_hmsm_ra   "+safe_ra_rad);
					log.info("safe_dms_dec   "+safe_dec_rad);
				   
					//Last task file 
					String HMTlastPlanFilePath="";
					File lastPlanDir=new File(equipment_lastPlanDir);
					if (lastPlanDir.isDirectory()&&lastPlanDir.exists()) {
						File[] planFiles=lastPlanDir.listFiles();
						long lastModifyTime=0;
						for (File file : planFiles) {
							if (file.isFile()&&file.getName().toLowerCase().endsWith(".txt")&&file.lastModified()>lastModifyTime) {
								lastModifyTime=file.lastModified();
								HMTlastPlanFilePath=file.getAbsolutePath();
							}
						}
					}
					File stopedPlan=new File(HMTlastPlanFilePath);
					if (HMTlastPlanFilePath!=null&&HMTlastPlanFilePath.length()>0&&stopedPlan.exists()&&stopedPlan.isFile()) {
//						sb.append("#CHAIN "+HMTlastPlanFilePath);
//						sb.append("\r\n");			
						System.out.println("#chain : last stoped plan file ="+HMTlastPlanFilePath);
					}else {
						System.out.println("File may not exist, path ="+HMTlastPlanFilePath);
					}
					
					//TODO create task file
					
				   
				   log.info(resultMap.get("s_sm_az"));
				   log.info(resultMap.get("s_sm_alt"));
				   //TODO go back to safe position before run chain
				   //run new //todo chain last plan 
				   //TODO chain task will run as high task level ,may be cause  conflict,low level to high level; 

					Random ra=new Random();//19:25:12.00	-01° 04' 48.0"  ISO 8859-1
//					String safe_dec_string=safe_dms_dec.getDeg()+"° "+safe_dms_dec.getMin()+"' "+safe_dms_dec.getSec()+"\"";
					String safe_dec_string=String.valueOf(Math.toDegrees(safe_dec_rad));
					//19:25:12.00
//					String safe_ra_string=safe_hmsm_ra.buildStringHMSM(':');
					String safe_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(Math.toDegrees(safe_ra_rad)));
					
					String task_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(taskData.getTask_Ra_deg()));
					String task_dec_string=String.valueOf(taskData.getTask_Dec_deg());
					
					PlanFileContent planFile=new PlanFileContent();
					NormalPlan plan_safe=new NormalPlan("SafePoint"+"-time",safe_ra_string,safe_dec_string);
					plan_safe.setSubSequenTar_BINNING(new int []{4});
					plan_safe.setSubSequenTar_INTERVAL(new double []{2});
//					planFile.getTargetList().add(plan_safe);
				   // TODO: 2017/10/1  ra 生成了一个负的 
					
					NormalPlan plan_4_1=new NormalPlan("GRB-40S-1"+"-time",task_ra_string,task_dec_string);
					plan_4_1.setSubSequenTar_DIR("GRB");
					plan_4_1.setSubSequenTar_BINNING(new int []{2});
					plan_4_1.setSubSequenTar_INTERVAL(new double []{40});
					NormalPlan plan_4_2=new NormalPlan("GRB-40S-2"+"-time",task_ra_string,task_dec_string);
					plan_4_2.setSubSequenTar_INTERVAL(new double []{40});
					NormalPlan plan_4_3=new NormalPlan("GRB-40S-3"+"-time",task_ra_string,task_dec_string);
					plan_4_3.setSubSequenTar_INTERVAL(new double []{40});

					NormalPlan plan_6_1=new NormalPlan("GRB-60S-1"+"-time",task_ra_string,task_dec_string);
					plan_6_1.setSubSequenTar_INTERVAL(new double []{60});
					NormalPlan plan_6_2=new NormalPlan("GRB-60S-2"+"-time",task_ra_string,task_dec_string);
					plan_6_2.setSubSequenTar_INTERVAL(new double []{60});
					NormalPlan plan_6_3=new NormalPlan("GRB-60S-3"+"-time",task_ra_string,task_dec_string);
					plan_6_3.setSubSequenTar_INTERVAL(new double []{60});
					NormalPlan plan_6_4=new NormalPlan("GRB-60S-4"+"-time",task_ra_string,task_dec_string);
					plan_6_4.setSubSequenTar_INTERVAL(new double []{60});

					planFile.getTargetList().add(plan_4_1);
					planFile.getTargetList().add(plan_4_2);
					planFile.getTargetList().add(plan_4_3);
					planFile.getTargetList().add(plan_6_1);
					planFile.getTargetList().add(plan_6_2);
					planFile.getTargetList().add(plan_6_3);
					planFile.getTargetList().add(plan_6_4);

					String taskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
					for(int i=0;i<12;i++){
						NormalPlan plan_9s=new NormalPlan("GRB-90S-"+(i+1)+"-time",task_ra_string,task_dec_string);
						plan_9s.setSubSequenTar_INTERVAL(new double []{90});
						planFile.getTargetList().add(plan_9s);
					}
					try {
						planFile.toFile(taskPlanFilePath);
						System.out.println(planFile.toOutString());
					} catch (PlanCheckExeption | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						stopJob(data ,next_task_name ,next_task_level);
						return;
					}
				   
				   AcpControl.runPlan(taskPlanFilePath, equipment_Url, equipment_User, equipment_Pass);
				   //init pre_task and next task
				   
				    data.put("pre_task_name", next_task_name);
				    data.put("pre_task_level", next_task_level);
				    data.put("next_task_name", "");
				    data.put("next_task_level", "0");
				   
			   }else{
				   log.info(">>>>>>>>>>>>>>>>>>>>>>stop old");
				   //stop current
				   AcpControl.stopRunPlan(equipment_Url, equipment_User, equipment_Pass);
			   }
			   
			}else if (Integer.parseInt(next_task_level)==Integer.parseInt(pre_task_level)){
				//do nothing and report a warning
				log.info(">>>>>>>>>>>>>>>>>>>>>>conflict task");
			   DeviceStatus deviceStatus = new DeviceStatus("任务警告" , device_name, device_name ,"任务冲突 "+pre_task_name+" = "+next_task_name
					   , sdf.format(new Date()),device_color , MessageColor.Warning);
			   deviceStatus.setObsOffLine(isObsOffLine);
//				String jsonEquipmentMessage="{\"type\":\"warning\",\"value\": \"conflict task "+pre_task_name+"="+next_task_level+"\"}";
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatus).toString());
			}else {
				log.info(">>>>>>>>>>>>>>>>>>>>>>ignore task");
			   DeviceStatus deviceStatus = new DeviceStatus("任务警告" , device_name, device_name ,"任务忽略 "+pre_task_name+" 》 "+next_task_name
					   , sdf.format(new Date()),device_color , MessageColor.Warning);
			   deviceStatus.setObsOffLine(isObsOffLine);
//				String jsonEquipmentMessage="{\"type\":\"info\",\"value\":\" ignore task "+pre_task_name+">"+next_task_level+"\"}";
			   AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
			}
		   
	   }
	   
	   
	   
		
	}

	//
	private void  stopJob( JobDataMap data ,String next_task_name ,String next_task_level){
		data.put("pre_task_name", next_task_name);
		data.put("pre_task_level", next_task_level);
		data.put("next_task_name", "");
		data.put("next_task_level", "0");
	}

//	private void sendMessage(JobDataMap data, String msg_name_equipment_status,String jsonMessage) {
//		ConnectionFactory connectionFactory;
//        Connection connection = null;
//        Session session;
//        Destination destination;
//        MessageProducer producer;
//        TextMessage message = null;
//        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
//                , ActiveMQConnection.DEFAULT_PASSWORD, data.getString("SysConfig.msg_url"));
//        try {
//            connection = connectionFactory.createConnection();
//            connection.start();
//            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
//            destination = session.createTopic(msg_name_equipment_status);
//            producer = session.createProducer(destination);
//            //todo use json text message
//            message=session.createTextMessage(jsonMessage);
//            producer.send(message);
//            session.commit();
//            log.debug("send AMQ " + jsonMessage);
//        } catch (JMSException e) {
//            e.printStackTrace();
//        }finally {
//            try {
//                if (null != connection){
//                    connection.close();
//                }
//            } catch (Throwable ignore) {
//            }
//        }
//	}

}
