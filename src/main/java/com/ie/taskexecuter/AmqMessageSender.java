package com.ie.taskexecuter;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;

/**
 * Created by Administrator on 2017/10/6.
 */
public  class AmqMessageSender {
    private static Logger log = LoggerFactory.getLogger(AmqMessageSender.class);
    public static void sendMessage(JobDataMap data, String msg_name_equipment_status, String jsonMessage) {
        ConnectionFactory connectionFactory;
        Connection connection = null;
        Session session;
        Destination destination;
        MessageProducer producer;
        TextMessage message = null;
        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
                , ActiveMQConnection.DEFAULT_PASSWORD, data.getString("SysConfig.msg_url"));
        try {
//            log.info("send AMQ try" );
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
            destination = session.createTopic(msg_name_equipment_status);
            producer = session.createProducer(destination);
            //todo use json text message
            message=session.createTextMessage(jsonMessage);
            producer.send(message);
            session.commit();
//            log.debug("send AMQ " + jsonMessage);
//            log.info("send AMQ " + jsonMessage.length());
        } catch (JMSException e) {
            log.info("send AMQ Error "  +e.getMessage());
            e.printStackTrace();
        }finally {
//            log.info("send AMQ finally" );
            try {
                if (null != connection){
                    connection.close();
                }
            } catch (Throwable ignore) {
            }
        }
    }
}
