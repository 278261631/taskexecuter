package com.ie.taskexecuter;

import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.ImgBase64Encoder;
import com.ie.sysmessage.MessageColor;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.python.util.PythonInterpreter;
import org.quartz.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.nio.file.StandardWatchEventKinds.*;


@PersistJobDataAfterExecution
public class FitsConvertDeamonJob implements Job{
	
	private Log log = LogFactory.getLog(FitsConvertDeamonJob.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();

		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();
		String fitsSrcPath=	       data.get("fitsSrcPath").toString();

		log.info("本设备 "+device_name+" 监视文件 ");
		DeviceStatus deviceStatus = new DeviceStatus("WebCamera" , device_name ,device_name ,"更新摄像头影像" , sdf.format(new Date()),device_color , MessageColor.OK);

		File fileRoot = new File(fitsSrcPath);
		if (!fileRoot.exists()){
			log.error("dir not exists   目录不存在  "+fitsSrcPath );
			fileRoot.mkdirs();
		}
		if (fileRoot.isFile()){
			log.error(" statusImageRoot 应该是路径而不是文件  "+fitsSrcPath );
		}

		try (WatchService ws = FileSystems.getDefault().newWatchService()) {
			Path dirToWatch = Paths.get(fitsSrcPath);
			dirToWatch.register(ws, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
			while (true) {
				WatchKey key = ws.take();
				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> eventKind = event.kind();
					if (eventKind == OVERFLOW) {
						System.out.println("Event  overflow occurred");
						continue;
					}
					if (eventKind == ENTRY_CREATE) {
						System.out.println("------------------     Convert Fits   -----------------------");
						PythonInterpreter interpreter = new PythonInterpreter();
						InputStream filepy;
						try {
							String scriptPath="D:/gitroot/gitlab/OpenObservatory/fit2png.py";
							filepy = new FileInputStream(scriptPath);
//							interpreter.p
							interpreter.execfile(filepy);
							filepy.close();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("------------------  End Convert Fits   -----------------------");
						continue;
					}
					WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
					Path dirEntry = currEvent.context();
					System.out.println(eventKind + "  occurred on  " + dirEntry);
				}
				boolean isKeyValid = key.reset();
				if (!isKeyValid) {
					System.out.println("No  longer  watching " + dirToWatch);
					break;
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}


	   
	   
		
	}

}
