package com.ie.taskexecuter;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;


import org.springframework.util.Base64Utils;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.win32.W32APIOptions;


import static com.ie.taskexecuter.AcpControl.MyUser32.*;

public class AcpControl {

	public static final int WM_LBUTTONUP = 514;
	public static final int WM_LBUTTONDOWN = 513;
	public static final int WM_LBUTTONDBLCLK = 0x203;

	interface MyUser32 extends User32 {

		MyUser32 INSTANCE = (MyUser32) Native.loadLibrary("user32", MyUser32.class, W32APIOptions.DEFAULT_OPTIONS);

		int VK_A = 0x61;

		public abstract HWND FindWindowEx(HWND hwndParent, HWND hwndChildAfter, String lpszClass, String lpszWindow);
		public abstract LRESULT SendMessage(HWND hWnd, int msg, WPARAM wParam, LPARAM lParam);

	}


	public static  String runPlan(String filePath,String url,String userName,String userPass) {
		PrintWriter out = null;
	        BufferedReader in = null;
	        String result = "";
	        try {
	        	System.out.println( getNowTimeString()+ "准备启动 ");
	            URL realUrl = new URL(url+"/ac/aacqplan.asp");
	            URLConnection conn = realUrl.openConnection();
	            
	            conn.setRequestProperty("accept", "*/*");
	            conn.setRequestProperty("connection", "Keep-Alive");
	            String input = userName + ":" + userPass;
	            String encoding =  Base64Utils.encodeToString(input.getBytes());
	            conn.setRequestProperty("Authorization", "Basic " + encoding);
	            conn.setRequestProperty("connection", "Keep-Alive");
//	            conn.setRequestProperty("user-agent",
//	                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	            conn.setDoOutput(true);
	            conn.setDoInput(true);
	            out = new PrintWriter(conn.getOutputStream());
	            out.print("plan="+filePath);
	            out.flush();
	            in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String line;
	            while ((line = in.readLine()) != null) {
	                result += line;
	            }
	        } catch (Exception e) {
	            System.out.println("发送 POST 请求出现异常！"+e);
	            e.printStackTrace();
	        }
	        finally{
	            try{
	                if(out!=null){out.close();}
	                if(in!=null){in.close();}
	            }
	            catch(IOException ex){ex.printStackTrace();}
	        }
	       return result;
	        
	        
	}    
	

	

	static SimpleDateFormat longDateFormatFile = new SimpleDateFormat("yyyy-MM-dd HH mm ss ");
	public static String getNowTimeString (){
		return longDateFormatFile.format(new Date()) +" :\t\t";
	}
	public static String stopRunPlan(String url,String userName,String userPass) {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			System.out.println( getNowTimeString()+ "尝试停止 ");
			URL realUrl = new URL(url+"/ac/astopscript.asp");
			URLConnection conn = realUrl.openConnection();
			
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			String input = userName + ":" + userPass;
			String encoding =  Base64Utils.encodeToString(input.getBytes());
			conn.setRequestProperty("Authorization", "Basic " + encoding);
			conn.setRequestProperty("connection", "Keep-Alive");
//	            conn.setRequestProperty("user-agent",
//	                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			out = new PrintWriter(conn.getOutputStream());
			out.print("Command=StopScript");
			out.flush();
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！"+e);
			e.printStackTrace();
		}
		finally{
			try{
				if(out!=null){
					out.close();
				}
				if(in!=null){
					in.close();
				}
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}

		// 本地强制点击停止按钮
		//acp6发现有网页端无法在曝光时强制停止的情况，这里加一个打开ACP页面点击Abort按钮，需要在本机环境
		HWND parent = INSTANCE.FindWindow("ThunderRT6FormDC", "ACP Observatory Control Software");
//	        ACP Observatory Control Software
//	        ThunderRT6CommandButton
//	        Abort

		System.out.println("parent " + (parent == null ? parent : parent.getPointer()));

		if (parent != null) {

//	            HWND child = INSTANCE.FindWindowEx(parent, null, "Edit", null);
			HWND consoleFrame = INSTANCE.FindWindowEx(parent, null, "ThunderRT6Frame", "Console");
			if(consoleFrame != null){
				System.out.println("consoleFrame " + (consoleFrame == null ? consoleFrame : consoleFrame.getPointer()));

//				HWND btn_run = INSTANCE.FindWindowEx(consoleFrame, null, "ThunderRT6CommandButton", "Run");
//				System.out.println("btn_run" + (btn_run == null ? btn_run : btn_run.getPointer()));

				HWND btn_abort = INSTANCE.FindWindowEx(consoleFrame, null, "ThunderRT6CommandButton", "Abort");
				System.out.println("btn_abort " + (btn_abort == null ? btn_abort : btn_abort.getPointer()));

				INSTANCE.ShowWindow(parent, SW_RESTORE);
				INSTANCE.SetForegroundWindow(parent);


//				if (btn_run != null) {
//					INSTANCE.SetFocus(btn_run);
//					WinUser.WINDOWINFO windowInfo = new WinUser.WINDOWINFO();
//					boolean result = INSTANCE.GetWindowInfo(btn_run, windowInfo);
//					System.out.println("btn_run info:" + windowInfo.dwStyle + "-" +windowInfo.dwExStyle +"-" + windowInfo.dwWindowStatus);
//
//					WPARAM wPARAM = new WPARAM(VK_A);
//					LPARAM lPARAM = new LPARAM(0);
//
//					long y = 77 + (22 << 16);//x + (y << 16)
//					LPARAM l = new LPARAM(5);
//					WPARAM w = new WPARAM(5);
////	            		INSTANCE.SendMessage(btn_run,WM_LBUTTONDOWN , w, l);
////	            		INSTANCE.SendMessage(btn_run,WM_LBUTTONUP , w, l);
//				}

				if (btn_abort != null) {
					WinUser.WINDOWINFO windowInfo = new WinUser.WINDOWINFO();
					boolean infoResult = INSTANCE.GetWindowInfo(btn_abort, windowInfo);
					System.out.println("btn_abort info:" + windowInfo.dwStyle + "-" +windowInfo.dwExStyle +"-" + windowInfo.dwWindowStatus);

					INSTANCE.SetFocus(btn_abort);

//	            		WPARAM wPARAM = new WPARAM(VK_A);
//	            		LPARAM lPARAM = new LPARAM(0);

					if(windowInfo.dwStyle == 1543577600){
						//不可用状态
					}
					if(windowInfo.dwStyle == 1409359872){
						//可用状态
						long y = 77 + (22 << 16);//x + (y << 16)
						LPARAM l = new LPARAM(5);
						WPARAM w = new WPARAM(5);
						INSTANCE.SendMessage(btn_abort,WM_LBUTTONDOWN , w, l);
						INSTANCE.SendMessage(btn_abort,WM_LBUTTONUP , w, l);
					}

				}
			}

		}


		return  result;
	}    
	
	


}
