package com.ie.taskexecuter;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Base64Utils;


public class AcpStatusUpdater {

//	private static Log log = LogFactory.getLog(AcpStatusUpdater.class);
	private static Logger log = LoggerFactory.getLogger(AcpStatusUpdater.class);
	private static  String updateSystemStatus(String url,String userName,String userPass) {
		PrintWriter out = null;
	        BufferedReader in = null;
	        StringBuffer result = new StringBuffer();
	        try {
	        	log.info( AcpControl.getNowTimeString()+ "update acp status  ");
	            URL realUrl = new URL(url+"/ac/asystemstatus.asp");
	            URLConnection conn = realUrl.openConnection();
	            conn.setRequestProperty("accept", "*/*");
	            conn.setRequestProperty("connection", "Keep-Alive");
	            String input = userName + ":" + userPass;
	            String encoding =  Base64Utils.encodeToString(input.getBytes());
	            conn.setRequestProperty("Authorization", "Basic " + encoding);
	            conn.setRequestProperty("connection", "Keep-Alive");
//	            conn.setRequestProperty("user-agent",
//	                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
	            conn.setDoOutput(true);
	            conn.setDoInput(true);
	            out = new PrintWriter(conn.getOutputStream());
	            out.flush();
	            in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String line;
	            while ((line = in.readLine()) != null) {
	                result.append(line);
	            }
	        } catch (Exception e) {
	            log.info("updateSystemStatus   " ,e);
//	            e.printStackTrace();
	        }
	        finally{
	            try{
	                if(out!=null){
	                    out.close();
	                }
	                if(in!=null){
	                    in.close();
	                }
	            }
	            catch(IOException ex){
	                ex.printStackTrace();
	            }
	        }
	       return result.toString();
	        
	        
	} 
	
	public static Map<String,String> acpResultToMap(String resultString){
		Map<String, String> resultMap= new HashMap<String,String>();
//	    log.debug(resultString);
	        String regexp_p = "_p\\(.+?\\)";
	        String regexp_s = "_s\\(.+?\\)";
	        String regexp_v = "_v\\(.+?\\)";
	        String regexp_c = "_c\\(.+?\\)";
	        String regexp_a = "_a\\(.+?\\)";
	        
	        Pattern pattern_p = Pattern.compile(regexp_p); 
	        Pattern pattern_s = Pattern.compile(regexp_s); 
	        Pattern pattern_v = Pattern.compile(regexp_v); 
	        Pattern pattern_c = Pattern.compile(regexp_c); 
	        Pattern pattern_a = Pattern.compile(regexp_c); 
	        Matcher matcher_p = pattern_p.matcher(resultString); 
	        Matcher matcher_s = pattern_s.matcher(resultString); 
	        Matcher matcher_v = pattern_v.matcher(resultString); 
	        Matcher matcher_c = pattern_c.matcher(resultString); 
	        Matcher matcher_a = pattern_a.matcher(resultString); 

	        while (matcher_s.find()) 
	        {
	        	String r_string = matcher_s.group(0);
//	        	log.debug(r_string);
//	        	log.debug(r_string.substring(3,r_string.length()-1));
				 String [] itemArray=r_string.substring(3,r_string.length()-1).split(",");
				 if (itemArray.length>1) {
					 resultMap.put("s_"+itemArray[0].replace("\'", ""), itemArray[1].replace("\'", ""));
				}
//				log.debug("---------"+resultMap);
	        } 
	        
	        while (matcher_v.find()) 
	        { 
	        	String r_string = matcher_v.group(0);
//	        	log.debug(r_string.substring(3,r_string.length()));
				 String [] itemArray=r_string.substring(3,r_string.length()-1).split(",");
				 if (itemArray.length>1) {
					 resultMap.put("v_"+itemArray[0].replace("\'", ""), itemArray[1].replace("\'", ""));
				}
	        } 
	        
	        while (matcher_p.find()) 
	        { 
	        	String r_string = matcher_p.group(0);
//	        	log.debug(r_string.substring(3,r_string.length()));
				 String [] itemArray=r_string.substring(3,r_string.length()-1).split(",");
				 if (itemArray.length>1) {
					 resultMap.put("p_"+itemArray[0].replace("\'", ""), itemArray[1].replace("\'", ""));
				}
	        } 
	        
	        while (matcher_a.find()) 
	        { 
	        	String r_string = matcher_a.group(0);
//	        	log.debug(r_string.substring(3,r_string.length()));
	        	String [] itemArray=r_string.substring(3,r_string.length()-1).split(",");
	        	if (itemArray.length>1) {
	        		resultMap.put("a_"+itemArray[0].replace("\'", ""), itemArray[1].replace("\'", ""));
	        	}
	        } 
	        
	        while (matcher_c.find()) 
	        { 
	        	String r_string = matcher_c.group(0);
//	        	log.debug(r_string.substring(3,r_string.length()-1));
				resultMap.put("c_value",r_string.substring(3,r_string.length()-1).replace("\'", ""));
				
	        } 
		
//		if (resultString!=null && resultString.indexOf(";")>0) {
//			resultString=resultString.replace("_s(", "");
//			resultString=resultString.replace("_c(", "");
//			resultString=resultString.replace("_p(", "");
//			resultString=resultString.replace("_v(", "");
//			resultString=resultString.replace(")", "");
//		 String []	resultStringArray = resultString.split(";");
//		 for (String string : resultStringArray) {
//			 String stringItem=string.replace("\'", "");
//			 String [] itemArray=stringItem.split(",");
//			 if (itemArray.length>1) {
//				 resultMap.put(itemArray[0], itemArray[1]);
//			}
//		}
//		}
		
		return resultMap;
	}
	
	
	public static  Map<String,String> getSystemStatus(String url,String userName,String userPass) {
		String resultString = AcpStatusUpdater.updateSystemStatus(url,userName,userPass);
		if (resultString .length()<2){return null;}
		return  AcpStatusUpdater.acpResultToMap(resultString);
//		return resultMap;
	}


	public static boolean isReadyToNext(Map<String,String> resultMap ){
		return "@anReady".equals(resultMap.get("s_sm_obsStat")) || "@anAvailable".equals(resultMap.get("s_sm_obsStat"))  ;
	}
	public static boolean isObsOffLine(Map<String,String> resultMap ){
		return "@wnOffline".equals(resultMap.get("s_sm_obsStat"));
	}
	
	
	

}
