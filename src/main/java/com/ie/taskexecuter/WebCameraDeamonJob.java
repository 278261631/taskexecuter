package com.ie.taskexecuter;

import com.github.sarxos.webcam.Webcam;
import com.ie.plangen.remoteobs.asputil.plangen.PlanCheckExeption;
import com.ie.plangen.remoteobs.asputil.plangen.PlanFileContent;
import com.ie.plangen.remoteobs.asputil.plangen.plantarget.NormalPlan;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.ImgBase64Encoder;
import com.ie.sysmessage.MessageColor;
import com.ie.sysmessage.TaskData;
import com.ie.taskexecuter.util.Equatorial_To_Horizontal;
import com.ie.taskexecuter.util.Horizontal_To_Equatorial;
import com.ie.taskexecuter.util.dms;
import com.ie.taskexecuter.util.hmsm;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.format.ISODateTimeFormat;
import org.quartz.*;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@PersistJobDataAfterExecution
public class WebCameraDeamonJob implements Job{
	
	private Log log = LogFactory.getLog(WebCameraDeamonJob.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();

		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();

		log.info("本设备 "+device_name+" 读取摄像头信息 ");
		DeviceStatus deviceStatus = new DeviceStatus("WebCamera" , device_name ,device_name ,"更新摄像头影像" , sdf.format(new Date()),device_color , MessageColor.OK);
//		Webcam webcam = Webcam.getDefault();
//		List<Webcam> cameras = Webcam.getWebcams();
		Webcam webcam        = (Webcam) data.get("webcam.default");
		List<Webcam> cameras = (List<Webcam>) data.get("webcam.webcams");
		if (webcam == null || (!webcam.isOpen())){
			log.info("未找到摄像头，重新查找默认摄像头  Reload Default Camera ");
			webcam = Webcam.getDefault();
			cameras = Webcam.getWebcams();
			try {
				webcam.open();
			}catch (Exception ex){
				log.info("默认摄像头 无法找到 Can not find default camera");
				DeviceStatus deviceStatusError = new DeviceStatus("摄像头错误" , device_name , device_name , " 无法找到默认摄像头  " , sdf.format(new Date()),device_color , MessageColor.Warning);
				AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatusError).toString());
				return;
			}
		}else{
			log.info("跳过打开摄像头操作，因为已经打开。 Skip Telescope Control Daemon , open already");
		}
		data.put("webcam.default",webcam);
		data.put("webcam.webcams",cameras);

		log.info("相机数量 :" + cameras.size());
//		webcam. //todo 相机列表  //不同时段使用不同相机 webCam 需要提取到主程序 一直打开
//		http://www.j-interop.org/sampleusage.html  用j-interop调用ascom 读取夜间摄像头
		//假设夜间摄像头不能用普通方式读取
		String logImageFileName = "D:/hello-world.png";
		try {
//			ImageIO.write(webcam.getImage(), "PNG", new File(logImageFileName));
//			deviceStatus.setCameraImage(ImgBase64Encoder.encodeImage(logImageFileName));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(webcam.getImage(), "png", baos);
            byte[] bytes = baos.toByteArray();
			deviceStatus.setCameraImage(ImgBase64Encoder.encode(bytes));
//			webcam.close();
			AmqMessageSender.sendMessage(data, msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	   
	   
		
	}

}
