package com.ie.taskexecuter;

import com.ie.sysmessage.TaskData;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.text.SimpleDateFormat;
import java.util.Map;

/***
 * 在linux系统应用之前  先使用这种方式来保证GRB任务会被执行吧  当收到一个GRB  就每隔一段时间回响一次(注意不要引发风暴)
 * 直到当前运行了GRB任务
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class GRBEcho implements Job{
	
	private Logger log = LoggerFactory.getLogger(GRBEcho.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat sdf_sss=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss-SSS");

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();
	    
	    data.put("taskIDFromTime" ,"[[[[[   ECHO    ]]]]]");
		String equipment_User=	           data.get("equipment_User").toString();
		String equipment_Pass=	           data.get("equipment_Pass").toString();
		String equipment_Url=              data.get("equipment_Url").toString();
		String device_name=	       data.get("device_name").toString();

		TaskData grbTaskData = (TaskData) data.get("GRB_task_data");



		Map<String, String> resultMap = AcpStatusUpdater.getSystemStatus(equipment_Url,equipment_User,equipment_Pass);

		if (resultMap == null){
			log.info(device_name +" 无法获取到设备状态 " +equipment_Url+"@" +equipment_User  );
			return;
		}
		String targetName = "";
		//acp 5
		String s_sm_actTgt_acp5=resultMap.get("s_sm_actTgt");
		//acp 6+
		String s_sm_plnTgt_acp6=resultMap.get("s_sm_plnTgt");
		if (s_sm_plnTgt_acp6 != null ){
			targetName = s_sm_plnTgt_acp6;
			log.info(data.get("taskIDFromTime")+ " "+"ACP6 Plan Name    "+ s_sm_plnTgt_acp6);
		}else {
			log.info(data.get("taskIDFromTime")+ " "+"ACP5 Plan Name    "+ s_sm_actTgt_acp5);
			targetName = s_sm_actTgt_acp5;
		}

		boolean isObsOffLine = AcpStatusUpdater.isObsOffLine(resultMap );
		log.info("--" + resultMap.get("s_sm_obsStat"));
		log.info("--" + isObsOffLine);


		targetName     = targetName    .replace("@an","").replace("@wn","").replace("@in","").replace("@af","")
				.replace("%3A","/").replace("%20"," ").replace("%28","(").replace("%29",")");

		TaskData taskData = (TaskData) data.get("task_data");
		if(taskData == null){
			taskData = new TaskData();
			taskData.setTaskName("");
			taskData.setTask_status("stopped");
		}
		log.info(targetName);
		if(targetName != null && targetName.contains(" (")){
			targetName = targetName.substring(0,targetName.indexOf(" ("));
			log.info("ACP6 Plan Name    "+ targetName);
		}

		if(targetName.toUpperCase().startsWith("GRB")){
			try {
				log.info("GRB 已经启动  echo暂停"  +targetName);
				context.getScheduler().pauseJob(context.getJobDetail().getKey());
				context.getScheduler().deleteJob(context.getJobDetail().getKey());
				log.info("GRB 已经启动  echo退出"  +targetName);
			} catch (SchedulerException e) {
				log.debug(e.toString());
			}

		}else {
			log.info("GRB Echo ...");
			ConnectionFactory connectionFactory;
			Connection connection = null;
			Session session;
			Destination destination;
			MessageProducer producer;
			TextMessage message = null;
			connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
					, ActiveMQConnection.DEFAULT_PASSWORD, data.getString("SysConfig.msg_url"));
			try {
				connection = connectionFactory.createConnection();
				connection.start();
				session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
				destination = session.createQueue(grbTaskData.getTarget_eqp().toUpperCase());
				producer = session.createProducer(destination);

				message=session.createTextMessage(JSONObject.fromObject(grbTaskData).toString());
				log.info(message.getText());
				producer.send(message);
				session.commit();

			} catch (JMSException e) {
				log.info("send AMQ Error "  +e.getMessage());
				e.printStackTrace();
			}finally {
				try {
					if (null != connection){
						connection.close();
					}
				} catch (Throwable ignore) {
				}
			}

		}


	}

}
