package com.ie.taskexecuter;

import com.ie.plangen.remoteobs.asputil.plangen.PlanFileContent;
import com.ie.plangen.remoteobs.asputil.plangen.plantarget.NormalPlan;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.ie.sysmessage.TaskTarget;
import com.ie.taskexecuter.util.*;
//import org.apache.log4j.Logger;
//import org.apache.log4j.spi.LoggerFactory;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinUser;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.commons.io.FileUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.Stat;
import org.joda.time.format.ISODateTimeFormat;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.configuration2.AbstractConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.quartz.impl.StdSchedulerFactory;

import javax.jms.*;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Calendar;

import org.springframework.stereotype.*;

import com.ie.sysmessage.TaskData;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.*;
import org.unix4j.Unix4j;
import org.unix4j.builder.Unix4jCommandBuilder;
import util.SendEmail;

import static com.ie.taskexecuter.AcpControl.MyUser32.INSTANCE;
import static com.sun.jna.platform.win32.WinUser.SW_RESTORE;

@ConfigurationProperties
@Component
public class TaskReceiver implements CommandLineRunner{

    private ConnectionFactory connectionFactory;
    private Connection connection = null;
    private Session session;
    private Destination destination;
    private MessageConsumer consumer;
//	@Value("${msg_url:failover://(tcp://msg.xastro.site:61616)?randomize=false}")
	@Value("${msg_url:failover://(tcp://127.0.0.1:61616)?randomize=false}")
	private String  msg_url;
	@Value("${msg_name:GRB_TASK,TaskTopic}")
	private String  msg_name;
	@Value("${amqLoginUser:amqLoginUser}")
	private String  amqLoginUser;
	@Value("${amqLoginPass:amqLoginPass}")
	private String  amqLoginPass;
	@Value("${isListenQueue:false}")
	private boolean  isListenQueue;
	@Value("${isAutoRestart:true}")
	private boolean  isAutoRestart;
	@Value("${autoRestartTime:0 0/1 * * * ?}")
	private String  autoRestartTime;
	@Value("${isZooKeeperTask:false}")
	private boolean  isZooKeeperTask;
	@Value("${encrypt_key:}")
	private String  encrypt_key;

	@Value("${Equipment.lastPlanDir}")
	private String equipment_lastPlanDir ;
	@Value("${Equipment.isChainLast:false}")
	private Boolean equipment_isChainLast ;
	@Value("${Equipment.latitude:43.4710372}")
	private String equipment_latitude ;
	@Value("${Equipment.longitude:87.1775888}")
	private String equipment_longitude ;
	@Value("${Equipment.User:test}")
	private String equipment_User ;
	@Value("${Equipment.Pass:test}")
	private String equipment_Pass ;
	@Value("${Equipment.LimitAngle_all:10}")
	private String equipment_LimitAngle_all ;
	@Value("${Equipment.Url:http://192.168.4.124:80}")
	private String equipment_Url ;
	@Value("${Equipment.PlanPath}")
	private String equipment_PlanPath ;
	@Value("${Equipment.ScriptPath:C:/Program Files (x86)/ACP Obs Control/Scripts/AcquireImages.js}")
	private String equipment_ScriptPath ;
	@Value("${Equipment.filter}")
	private String equipment_filter ;
	@Value("${Equipment_safe_east_alt:45}")
	private String equipment_safe_east_alt ;
	@Value("${Equipment_safe_east_az:170}")
	private String equipment_safe_east_az ;
	@Value("${Equipment_safe_west_alt:45}")
	private String equipment_safe_west_alt ;
	@Value("${Equipment_safe_west_az:190}")
	private String equipment_safe_west_az ;
	@Value("${Equipment_isHMT_SafePoint:false}")
	private Boolean Equipment_isHMT_SafePoint ;
	
	@Value("${msg_name_equipment_status:equipment_status}")
	private String msg_name_equipment_status ;
	@Value("${device_name}")
	private String device_name ;
	@Value("${device_color:black}")
	private String device_color ;
	@Value("${listen_device_list}")
	private String listen_device_list ;
//	@Value("${isStartWebCamera}")
//	private boolean isStartWebCamera ;
//	@Value("${isStartStatusImage}")
//	private boolean isStartStatusImage ;
//	@Value("${isDeleteStatusImage}")
//	private boolean isDeleteStatusImage ;
//	@Value("${isConvertFits}")
//	private boolean isConvertFits ;
//	@Value("${statusImageRoot}")
//	private String statusImageRoot ;
//	@Value("${fitsSrcPath}")
//	private String fitsSrcPath ;
	@Value("${acpFitsRoot}")
	private String acpFitsRoot ;
	@Value("${isStartAcpControl}")
	private boolean isStartAcpControl ;
	@Value("${isStartAcpParkMonitor:false}")
	private boolean isStartAcpParkMonitor ;


//    @Value("${isStartGravitationalWave:false}")
    private boolean isStartGravitationalWave ;
//    @Value("${GravitationalWaveACPFileDirectory:d:/}")
    private String GravitationalWaveACPFileDirectory;
//    @Value("${GravitationalWaveAmqTopic:GravitationalWave}")
    private String GravitationalWaveAmqTopic;

//    @Value("${zookeeperUrl:remo.xtelescope.net:2181}")
    private String zookeeperUrl;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public void start() throws JMSException  {

	}
	
    @Autowired
    public TaskReceiver(
//            @Value("${msg_url:failover://(tcp://msg.xastro.site:61616)?randomize=false}") String msg_url,
            @Value("${msg_url:failover://(tcp://127.0.0.1:61616)?randomize=false}") String msg_url,
            @Value("${msg_name:GRB_TASK,TaskTopic}") String msg_name,
    		@Value("${amqLoginUser:amqLoginUser}") String amqLoginUser,@Value("${amqLoginPass:amqLoginPass}") String amqLoginPass,
    		@Value("${isListenQueue:false}") boolean isListenQueue,
    		@Value("${isAutoRestart:true}") boolean isAutoRestart,
//    		0/10 * * * * ?
    		@Value("${autoRestartTime:0/10 * * * * ?}") String autoRestartTime,
    		@Value("${isZooKeeperTask:false}") boolean isZooKeeperTask,
    		@Value("${encrypt_key:}") String encrypt_key,
    		@Value("${Equipment.lastPlanDir}") String equipment_lastPlanDir,
    		@Value("${Equipment.isChainLast:false}") Boolean equipment_isChainLast,
    		@Value("${Equipment.latitude:43.4710372}") String equipment_latitude,
    		@Value("${Equipment.longitude:87.1775888}") String equipment_longitude,
    		@Value("${Equipment.User:test}") String equipment_User,
    		@Value("${Equipment.Pass:test}") String equipment_Pass,
    		@Value("${Equipment.LimitAngle_all:10}") String equipment_LimitAngle_all,
    		@Value("${Equipment.Url:http://192.168.4.124:80}") String equipment_Url,
    		@Value("${Equipment.PlanPath}") String equipment_PlanPath,
    		@Value("${Equipment.filter}") String equipment_filter,
    		@Value("${msg_name_equipment_status:equipment_status}") String msg_name_equipment_status,
    		@Value("${device_name}") String device_name,
    		@Value("${device_color}") String device_color,
    		@Value("${listen_device_list}") String listen_device_list,
//    		@Value("${isStartWebCamera}") boolean isStartWebCamera,
//    		@Value("${isDeleteStatusImage}") boolean isDeleteStatusImage,
//    		@Value("${isConvertFits}") boolean isConvertFits,
//    		@Value("${isStartStatusImage}") boolean isStartStatusImage,
//    		@Value("${statusImageRoot}") String statusImageRoot,
//    		@Value("${fitsSrcPath}") String fitsSrcPath,
    		@Value("${acpFitsRoot}") String acpFitsRoot,
    		@Value("${isStartAcpControl}") boolean isStartAcpControl,
    		@Value("${Equipment_safe_east_alt:45}") String equipment_safe_east_alt,
    		@Value("${Equipment_safe_east_az:170}") String equipment_safe_east_az,
    		@Value("${Equipment_safe_west_alt:45}") String equipment_safe_west_alt,
    		@Value("${Equipment_safe_west_az:190}") String equipment_safe_west_az,
            @Value("${isStartGravitationalWave:false}") boolean isStartGravitationalWave ,
            @Value("${GravitationalWaveACPFileDirectory:d:/}")String GravitationalWaveACPFileDirectory,
            @Value("${GravitationalWaveAmqTopic:GravitationalWave}") String GravitationalWaveAmqTopic,
            @Value("${zookeeperUrl:remo.xtelescope.net:2181}") String zookeeperUrl,
    		@Value("${Equipment_isHMT_SafePoint:false}") Boolean Equipment_isHMT_SafePoint
    		) throws IOException {
        this.msg_url = msg_url;
        this.msg_name = msg_name;
        this.amqLoginUser = amqLoginUser;
        this.amqLoginPass = amqLoginPass;
        this.isListenQueue = isListenQueue;
        this.isAutoRestart = isAutoRestart;
        this.autoRestartTime = autoRestartTime;
        this.isZooKeeperTask = isZooKeeperTask;
        this.encrypt_key = encrypt_key;

        this.equipment_lastPlanDir = equipment_lastPlanDir;
        this.equipment_isChainLast = equipment_isChainLast;
        this.equipment_latitude =equipment_latitude ;
        this.equipment_longitude = equipment_longitude;
        this.equipment_User = equipment_User;
        this.equipment_Pass = equipment_Pass;
        this.equipment_LimitAngle_all = equipment_LimitAngle_all;
        this.equipment_Url = equipment_Url;
        this.equipment_PlanPath = equipment_PlanPath;
        this.equipment_filter = equipment_filter;
        this.equipment_safe_east_alt=equipment_safe_east_alt;
        this.equipment_safe_east_az=equipment_safe_east_az;
        this.equipment_safe_west_alt=equipment_safe_west_alt;
        this.equipment_safe_west_az=equipment_safe_west_az;
        this.Equipment_isHMT_SafePoint=Equipment_isHMT_SafePoint;

        this.msg_name_equipment_status=msg_name_equipment_status;
        this.device_name=device_name;
        this.device_color=device_color;
        this.listen_device_list=listen_device_list;
//        this.isStartWebCamera=isStartWebCamera;
//        this.isStartStatusImage=isStartStatusImage;
//        this.isDeleteStatusImage=isDeleteStatusImage;
//        this.isConvertFits=isConvertFits;
//        this.statusImageRoot=statusImageRoot;
//        this.fitsSrcPath=fitsSrcPath;
        this.acpFitsRoot=acpFitsRoot;
        this.isStartAcpControl=isStartAcpControl;
        this.isStartGravitationalWave=isStartGravitationalWave;
        this.GravitationalWaveACPFileDirectory=GravitationalWaveACPFileDirectory;
        this.GravitationalWaveAmqTopic=GravitationalWaveAmqTopic;

        File directory = new File("");//参数为空
        String courseFile = directory.getCanonicalPath() ;
        String restartCmd = "choice /t 3 /d y /n || start \""+courseFile+"/runJar.bat"+"\"";

        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++ 20220219");
        log.info("device_name                " + device_name );
        log.info("listen_device_list                " + listen_device_list );
        log.info("msg_url                " + msg_url );
        log.info("msg_name               " + msg_name );
        log.info("amqLoginUser           " + amqLoginUser );
        log.info("amqLoginPass           " + amqLoginPass );
        log.info("encrypt_key            " + encrypt_key );
        log.info("equipment_Url          " + equipment_Url );
        log.info("equipment_User         " + equipment_User );
        log.info("equipment_Pass         " + equipment_Pass );
        log.info("equipment_isChainLast         " + equipment_isChainLast );
        log.info("isListenQueue         " + isListenQueue );
        log.info("isAutoRestart         " + isAutoRestart + "    " + autoRestartTime);
        log.info("isZooKeeperTask         " + isZooKeeperTask );
        log.info("isStartGravitationalWave         " + isStartGravitationalWave );
        log.info("GravitationalWaveACPFileDirectory         " + GravitationalWaveACPFileDirectory );
        log.info("GravitationalWaveAmqTopic         " + GravitationalWaveAmqTopic );
        log.info(" !!!!!!!  Run this Program in 'administrator' Mode  due to acp install position in win7/10 使用管理员权限运行此程序 ");
        log.info(" !!!!!!!   # jacob-***.dll 放入 java/bin ");
        log.info(" !!!!!!!   you may need reg64.bat and run it under administrator   due to acp install position in win7/10  ");
        log.info(" restart Command     "+restartCmd);
        log.info(" restart Time   BeijingTime 14:00  LocalTime  12:00:00");
        log.info(System.getProperty("java.library.path"));
        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++");
    }
   

    Logger log = LoggerFactory.getLogger(TaskReceiver.class);
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH mm ss SSS");


	@Override
	public void run(String... arg0) throws Exception {



//        org.joda.time.format.DateTimeFormatter parserISO = ISODateTimeFormat.dateTimeNoMillis();
//        Date end_time = parserISO.parseDateTime("2017-10-06T01:27:00+08:00").toDate();
//        Date start_time = parserISO.parseDateTime("2017-10-06T01:38:00+08:00").toDate();
//
//        log.debug("---------" +(parserISO.parseDateTime("2017-10-06T01:27:00+08:00").getMillis()-parserISO.parseDateTime("2017-10-06T01:38:00+08:00").getMillis()));
//        log.debug("---------" +end_time.ge);
		log.info("Server Start           "+simpleDateFormat.format(new Date()));
		log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		log.info("Start EquipmentDeamon Schedule...");
		Scheduler sched = new StdSchedulerFactory().getScheduler();
        sched.start();



//        CuratorFramework curtatorClient = CuratorFrameworkFactory.newClient(zookeeperUrl, new RetryNTimes(10, 5000));
//        if(isZooKeeperTask){curtatorClient.start();}

        //-------------------------------------------
//        if (isConvertFits){
//
//            log.debug("  fits Converter   启动fits文件转换");
//            JobDetail fitsConvertJob = JobBuilder.newJob(FitsConvertDeamonJob.class).storeDurably()
//                    .withIdentity("FitsConverter_Job", "EquipmentDeamonJob_JobGroup").build();
//
//            fitsConvertJob.getJobDataMap().put("SysConfig.msg_url",this.msg_url);
//            fitsConvertJob.getJobDataMap().put("SysConfig.msg_name",this.msg_name);
//            fitsConvertJob.getJobDataMap().put("msg_name_equipment_status",msg_name_equipment_status);
//            fitsConvertJob.getJobDataMap().put("device_name",device_name);
//            fitsConvertJob.getJobDataMap().put("device_color",device_color);
//
//            fitsConvertJob.getJobDataMap().put("fitsSrcPath",fitsSrcPath);
//
//            DeviceStatus deviceStatus = new DeviceStatus("摄像头错误" , device_name , " 无法找到默认摄像头  " , sdf.format(new Date()),device_color , MessageColor.Warning);
//            AmqMessageSender.sendMessage(fitsConvertJob.getJobDataMap(), msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//
//            Trigger triggerFitsConverter = TriggerBuilder.newTrigger().withIdentity("FitsConverter_Deamon_Trigger", "EquipmentDeamonTrigger_TriggerGroup")
//                    .startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(cameraUpdateSecond)
//                            .withRepeatCount(0)).build();
//            sched.scheduleJob(fitsConvertJob, triggerFitsConverter);
//        }

        //-------------------------------------------


//        if(isStartWebCamera){
//            log.debug("启动网络摄像头定时发图");
//
//            JobDetail initJobWebcamera = JobBuilder.newJob(WebCameraDeamonJob.class).storeDurably()
//                    .withIdentity("WebCam_Deamon_Job", "EquipmentDeamonJob_JobGroup").build();
//
//
//            initJobWebcamera.getJobDataMap().put("SysConfig.msg_url",this.msg_url);
//            initJobWebcamera.getJobDataMap().put("SysConfig.msg_name",this.msg_name);
//            initJobWebcamera.getJobDataMap().put("msg_name_equipment_status",msg_name_equipment_status);
//            initJobWebcamera.getJobDataMap().put("device_name",device_name);
//            initJobWebcamera.getJobDataMap().put("device_color",device_color);
//
//            Webcam webcam = Webcam.getDefault();
//            List<Webcam> cameras = Webcam.getWebcams();
//            if(webcam != null){
//                webcam.open();
//            }else {
//                log.debug("默认摄像头 无法找到 Can not find default camera");
//                DeviceStatus deviceStatus = new DeviceStatus("摄像头错误" , device_name , " 无法找到默认摄像头  " , sdf.format(new Date()),device_color , MessageColor.Warning);
//                AmqMessageSender.sendMessage(initJobWebcamera.getJobDataMap(), msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//            }
//            initJobWebcamera.getJobDataMap().put("webcam.default",webcam);
//            initJobWebcamera.getJobDataMap().put("webcam.webcams",cameras);
//
//            Trigger triggerWebCamera = TriggerBuilder.newTrigger().withIdentity("WebCam_Deamon_Trigger", "EquipmentDeamonTrigger_TriggerGroup")
//                    .startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(cameraUpdateSecond)
//                            .repeatForever()).build();
//            sched.scheduleJob(initJobWebcamera, triggerWebCamera);
//        }

//        if(isStartStatusImage){
//            log.debug("启动定时发状态图");
//            JobDetail initJobWeImage = JobBuilder.newJob(WebStatusImageDeamonJob.class).storeDurably()
//                    .withIdentity("StatusImage_Job", "EquipmentDeamonJob_JobGroup").build();
//
//            initJobWeImage.getJobDataMap().put("SysConfig.msg_url",this.msg_url);
//            initJobWeImage.getJobDataMap().put("SysConfig.msg_name",this.msg_name);
//            initJobWeImage.getJobDataMap().put("msg_name_equipment_status",msg_name_equipment_status);
//            initJobWeImage.getJobDataMap().put("device_name",device_name);
//            initJobWeImage.getJobDataMap().put("device_color",device_color);
//            initJobWeImage.getJobDataMap().put("statusImageRoot",statusImageRoot);
//            initJobWeImage.getJobDataMap().put("isDeleteStatusImage",isDeleteStatusImage);
//
//                DeviceStatus deviceStatus = new DeviceStatus("摄像头错误" , device_name , " 无法找到默认摄像头  " , sdf.format(new Date()),device_color , MessageColor.Warning);
//                AmqMessageSender.sendMessage(initJobWeImage.getJobDataMap(), msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());
//
//            Trigger triggerWebImage = TriggerBuilder.newTrigger().withIdentity("WebStatusImage_Deamon_Trigger", "EquipmentDeamonTrigger_TriggerGroup")
//                    .startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(cameraUpdateSecond)
//                            .repeatForever()).build();
//            sched.scheduleJob(initJobWeImage, triggerWebImage);
//        }

        log.info("                               isStartAcpParkMonitor                " + isStartAcpParkMonitor);
        if (isStartAcpParkMonitor){
            log.info("                               isStartAcpParkMonitor                " + isStartAcpParkMonitor);
            JobDetail acpStatusJob = JobBuilder.newJob(AcpStatusJob.class).storeDurably()
                    .withIdentity("AcpStatusJob_Job", "AcpStatusJob_JobGroup").build();

            acpStatusJob.getJobDataMap().put("SysConfig.msg_url",this.msg_url);
            acpStatusJob.getJobDataMap().put("SysConfig.msg_name",this.msg_name);
            acpStatusJob.getJobDataMap().put("msg_name_equipment_status",msg_name_equipment_status);
            acpStatusJob.getJobDataMap().put("device_name",device_name);
            acpStatusJob.getJobDataMap().put("device_color",device_color);

            Trigger triggerAcpStatusConverter = TriggerBuilder.newTrigger().withIdentity("AcpStatus_Deamon_Trigger", "AcpStatusDeamonTrigger_TriggerGroup")
                    .startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(30).repeatForever()).build();
            sched.scheduleJob(acpStatusJob, triggerAcpStatusConverter);
        }
            log.info("                               isAutoRestart                " + isAutoRestart + "   :  "+autoRestartTime);
        if (isAutoRestart){
            log.info("                               isAutoRestart                " + isAutoRestart + "   :  "+autoRestartTime);
//            SCHTASKS /Create /S system /U administrator  /P   /SC ONLOGON  /TN GW_GRB_Task /TR "D:\gitroot\taskexecuter\target\runJar.bat" /f
//            SCHTASKS /Run  /S system /U administrator  /P password /TN GW_GRB_Task
            JobDetail acpStatusJob = JobBuilder.newJob(AutoRestart.class).storeDurably()
                    .withIdentity("AutoRestart_Job", "AutoRestart_JobGroup").build();
//            Trigger triggerAcpStatusConverter = TriggerBuilder.newTrigger().withIdentity("AutoRestart_Trigger", "AutoRestart_TriggerGroup")
//                    .startAt(new Date(System.currentTimeMillis() + 5000)).withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(6).repeatForever()).build();
//            Trigger triggerAcpStatusConverter = TriggerBuilder.newTrigger().withIdentity("AutoRestart_Trigger", "AutoRestart_TriggerGroup")
//                    .withSchedule(CronScheduleBuilder.cronSchedule("0/10 * * * * ?")).build();
            Trigger triggerAcpStatusConverter = TriggerBuilder.newTrigger().withIdentity("AutoRestart_Trigger", "AutoRestart_TriggerGroup")
                    .withSchedule(CronScheduleBuilder.cronSchedule(autoRestartTime)).build();
//            Trigger triggerAcpStatusConverter = TriggerBuilder.newTrigger().withIdentity("AutoRestart_Trigger", "AutoRestart_TriggerGroup")
//                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(30).repeatForever()).build();
            sched.scheduleJob(acpStatusJob, triggerAcpStatusConverter);
            log.info("                               isAutoRestart                " + isAutoRestart + "   :  "+autoRestartTime);
        }

        if (!isStartAcpControl){
            log.info("跳过设备控制");
            return;
        }


//        Date runTime =   DateBuilder.futureDate(10, DateBuilder.IntervalUnit.SECOND);
//        JobDetail initJob = JobBuilder.newJob(EquipmentDeamonJob.class).storeDurably()
//        		.withIdentity("HMT_Deamon_Job", "EquipmentDeamonJob_JobGroup").build();
        JobDetail initJob = JobBuilder.newJob(EquipmentDeamonJobNormal.class).storeDurably()
        		.withIdentity("HMT_Deamon_Job", "EquipmentDeamonJob_JobGroup").build();
//        JobDetail initJob = JobBuilder.newJob(EquipmentDeamonJobNormal.class).storeDurably()
//        		.withIdentity(this.device_name+"_Deamon_Job", "EquipmentDeamonJob_JobGroup").build();
        initJob.getJobDataMap().put("SysConfig.msg_url",this.msg_url);
        initJob.getJobDataMap().put("SysConfig.msg_name",this.msg_name);
//        initJob.getJobDataMap().put("SysConfig.task_name",TaskCommandEnum.ReportStatus);

        initJob.getJobDataMap().put("equipment_lastPlanDir",equipment_lastPlanDir);
        initJob.getJobDataMap().put("equipment_isChainLast",equipment_isChainLast);
        initJob.getJobDataMap().put("equipment_latitude",equipment_latitude);
        initJob.getJobDataMap().put("equipment_longitude",equipment_longitude);
        initJob.getJobDataMap().put("equipment_User",equipment_User);
        initJob.getJobDataMap().put("equipment_Pass",equipment_Pass);
        initJob.getJobDataMap().put("equipment_LimitAngle_all",equipment_LimitAngle_all);
        initJob.getJobDataMap().put("equipment_Url",equipment_Url);
        initJob.getJobDataMap().put("equipment_PlanPath",equipment_PlanPath);
        initJob.getJobDataMap().put("equipment_filter",equipment_filter);
        initJob.getJobDataMap().put("msg_name_equipment_status",msg_name_equipment_status);
        initJob.getJobDataMap().put("device_name",device_name);
        initJob.getJobDataMap().put("device_color",device_color);
        initJob.getJobDataMap().put("listen_device_list", Arrays.asList(listen_device_list.toUpperCase().split(",")));

        initJob.getJobDataMap().put("equipment_safe_east_alt",equipment_safe_east_alt);
        initJob.getJobDataMap().put("equipment_safe_east_az",equipment_safe_east_az);
        initJob.getJobDataMap().put("equipment_safe_west_alt",equipment_safe_west_alt);
        initJob.getJobDataMap().put("equipment_safe_west_az",equipment_safe_west_az);
        initJob.getJobDataMap().put("Equipment_isHMT_SafePoint",Equipment_isHMT_SafePoint);

        initJob.getJobDataMap().put("pre_task_name","default");
        initJob.getJobDataMap().put("pre_task_level","0");
        
        initJob.getJobDataMap().put("next_task_name","");
        initJob.getJobDataMap().put("next_task_level","0");
        initJob.getJobDataMap().put("acpFitsRoot",acpFitsRoot);

        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("HMT_Deamon_Trigger", "EquipmentDeamonTrigger_TriggerGroup")
        		.startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(7)
        				.repeatForever()).build();

        sched.scheduleJob(initJob, trigger);



		
		
		Configurations configs = new Configurations();
        Configuration config = null;
        try {
            config = configs.properties(new File("sys_config.properties"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        ((AbstractConfiguration) config).setListDelimiterHandler(new DefaultListDelimiterHandler(','));


        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, msg_url);
        connection = connectionFactory.createConnection();
        connection.setClientID(device_name + "-" + simpleDateFormat.format(new Date()));
        log.info("******************** ClientID =     "+connection.getClientID()+"************************");
        connection.start();
        session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
        if(isListenQueue){
            destination = session.createQueue(msg_name);
        }else {
            destination = session.createTopic(msg_name);
        }

        consumer = session.createConsumer(destination);

        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {


                //因Go 语言中暂时无法发送TextMessage类型的消息 暂时只能这样写
                if (message instanceof TextMessage || message instanceof ActiveMQMessage) {
                    String textMessage = null;
                	//TODO sky light calculate no running in daylight
                	
                	//TODO  no running when object not rise
                    if (message instanceof ActiveMQMessage) {
                        try {
                            log.info("****************** MSG  GO   ******************");
                            textMessage = new String(((ActiveMQMessage)message).getContent().getData(),"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    if(message instanceof TextMessage){
                        try {
                            log.info("****************** MSG ******************");
                            textMessage = ((TextMessage) message).getText();
                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }

                    try {

                        //System.out.println(textMessage);
                        log.info(textMessage);
                        JSONObject jsonObj = JSONObject.fromObject(textMessage);

                        Map<String, Class> classMap = new HashMap<String, Class>();
                        classMap.put("task_targets", TaskTarget.class);
                        TaskData taskData = (TaskData) JSONObject.toBean(jsonObj, TaskData.class,classMap);

//                        String rootPath = "/task/"+taskData.getTaskName()+"/status";
//                        if(isZooKeeperTask){
//                            Stat taskExists = curtatorClient.checkExists().forPath(rootPath);
//                            if(null != taskExists){
//                                log.info(taskData.getTaskName() + "    exits " + taskExists);
//                            }else {
//                                String result = curtatorClient.create().withMode(CreateMode.PERSISTENT).withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE).forPath(rootPath, "01".getBytes());
//                                System.out.println(result);
//                            }
//                        }


                        JobDetail jobDetail = sched.getJobDetail(JobKey.jobKey("HMT_Deamon_Job", "EquipmentDeamonJob_JobGroup"));
                        List<String> listen_device_list = (List<String>) jobDetail.getJobDataMap().get("listen_device_list");
                        if (!listen_device_list.contains(taskData.getTarget_eqp().toUpperCase())) {
                            log.info("本设备 " + device_name + " 忽略目标为 " + taskData.getTarget_eqp() + " 的任务 ");
                            DeviceStatus deviceStatus = new DeviceStatus("忽略执行", device_name,device_name, "  " + "忽略  " + taskData.getTarget_eqp() + "   " + taskData.getTaskName(), sdf.format(new Date()), device_color, MessageColor.Warning);
                            AmqMessageSender.sendMessage(jobDetail.getJobDataMap(), msg_name_equipment_status, JSONObject.fromObject(deviceStatus).toString());
                            return;
                        } else {
                        }
                        log.info("本设备 " + device_name + " 接收到目标为 " + taskData.getTarget_eqp() + " 任务 ");
                        DeviceStatus deviceStatus = new DeviceStatus("接受执行", device_name,device_name, "  " + "接受  " + taskData.getTarget_eqp() + "   " + taskData.getTaskName(), sdf.format(new Date()), device_color, MessageColor.OK);
                        deviceStatus.setTaskName(taskData.getTaskName());
                        AmqMessageSender.sendMessage(jobDetail.getJobDataMap(), msg_name_equipment_status, JSONObject.fromObject(deviceStatus).toString());

                        if(taskData.getTaskName() != null && taskData.getTaskName().contains(" (")){
                            log.info("本设备 " + device_name + " 忽略名称为 " + taskData.getTaskName() + " 的任务 名称不能包含 空格" );
                            DeviceStatus deviceStatusE = new DeviceStatus("忽略执行", device_name,device_name, "  " + "忽略  " + taskData.getTarget_eqp() + "   " + taskData.getTaskName(), sdf.format(new Date()), device_color, MessageColor.Warning);
                            deviceStatus.setTaskName(taskData.getTaskName());
                            AmqMessageSender.sendMessage(jobDetail.getJobDataMap(), msg_name_equipment_status, JSONObject.fromObject(deviceStatusE).toString());
                            return;
                        }



//                        if(statusResultMap == null){
//                            log.info("设备可能不在线");
//                            return;
//                        }
                        //todo 如果报错 等待2秒 继续
                        AcpStatus acpStatus ;
                        while(true){
                            try{
                                acpStatus = AcpComControl.GetStatus(device_name);
                                break;
                            }catch (Exception ex){
                                log.info("Error In Receive Message  收到消息期间读取acp状态错误,常见于Script执行初期 ");
                                log.info("---" + ex);
                                ex.printStackTrace();
//                                SendEmail.sendToEmails("Error TaskExecutror   " + device_name,new String[]{"278261631@qq.com"}, ex.getMessage() );
                            }

                        }

                        
//                        String span_scope_status_ = jobDetail.getJobDataMap().get("span_scope_status_").toString();
//                        String span_ccd_status_ = jobDetail.getJobDataMap().get("span_ccd_status_").toString();
//                        String s_sm_obsStat = jobDetail.getJobDataMap().get("s_sm_obsStat").toString();
//                        //targetName
//                        String acp_target_name = jobDetail.getJobDataMap().get("acp_target_name").toString();

//                        TaskData taskData_running = (TaskData) jobDetail.getJobDataMap().get("task_data");
//                        if(taskData_running == null){
//                            taskData_running = new TaskData();
//                            taskData_running.setTaskName("");
//                            taskData_running.setTask_level("0");
//                        }

//                      jobDetail.getJobDataMap().put("task_ra_h", task_ra_h);
//                		jobDetail.getJobDataMap().put("task_ra_m", task_ra_m);
//                		jobDetail.getJobDataMap().put("task_ra_s", task_ra_s);
//                		jobDetail.getJobDataMap().put("task_dec_d", task_dec_d);
//                		jobDetail.getJobDataMap().put("task_dec_m", task_dec_m);
//                		jobDetail.getJobDataMap().put("task_dec_s", task_dec_s);
                        //todo  判定 HMT是否在可执行范围内


                        hmsm hmsm_ra=new hmsm(Math.toRadians(taskData.getTask_Ra_deg()));
                        dms dms_dec=new  dms(Math.toRadians(taskData.getTask_Dec_deg()));
                        Double lat=Double.parseDouble(equipment_latitude),longt=Double.parseDouble(equipment_longitude);
                        Calendar calendar=new GregorianCalendar();
//                        log.info(statusResultMap.get("taskIDFromTime") +calendar.toString());
//                        log.info(statusResultMap.get("taskIDFromTime") +hmsm_ra.toString());
//                        log.info(statusResultMap.get("taskIDFromTime") +dms_dec.toString());
//                        log.info(statusResultMap.get("taskIDFromTime") +lat.toString());
//                        log.info(statusResultMap.get("taskIDFromTime") +longt.toString());

                        double[] altAz_rad= Equatorial_To_Horizontal.Convert_E2H(hmsm_ra, dms_dec,lat,longt,calendar);
                        double[] altAz_deg=new double[]{Math.toDegrees(altAz_rad[0]),Math.toDegrees(altAz_rad[1])};
//                        log.info(statusResultMap.get("taskIDFromTime") +"-------"+altAz_deg[0]);
//                        log.info(statusResultMap.get("taskIDFromTime") +"-------"+altAz_deg[1]);
                        if(altAz_deg[0]< Double.parseDouble(equipment_LimitAngle_all)){
                            log.info( "当前目标高度不足 "+altAz_deg[0]+" 方向角  " +altAz_deg[1] +"  "+ taskData.getTarget_eqp() + "   " + taskData.getTaskName());
                            DeviceStatus deviceStatusH = new DeviceStatus("忽略执行", device_name,device_name, "  " + "当前目标高度不足 "+altAz_deg[0]+" 方向角  " +altAz_deg[1] +"  "+ taskData.getTarget_eqp() + "   " + taskData.getTaskName(), sdf.format(new Date()), device_color, MessageColor.Warning);
                            AmqMessageSender.sendMessage(jobDetail.getJobDataMap(), msg_name_equipment_status, JSONObject.fromObject(deviceStatusH).toString());
                            log.info("忽略高度不足");
//                            return;
                        }
                        //这里暂时抛弃了级别 后面如果要引入级别 可以放到一个更容易保存的地方 比如 plannName GRB-L-10001  GRB-L-9999
                        String task_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(((taskData.getTask_Ra_deg()%360)+360)%360));
                        String task_dec_string=String.valueOf(taskData.getTask_Dec_deg());
                        if(acpStatus.getObsOffLine()){
                            log.info("本设备 " + device_name + " 拒绝名称为 " + taskData.getTaskName() + " 的任务 设备不在线" );
                            if(taskData.getTaskName().toUpperCase().startsWith("GRB")){
//                                PlanFileContent planFileGRB;
//                                planFileGRB = PlanGeneratePlanFile_HMT(Double.parseDouble(task_ra_string) , Double.parseDouble(task_dec_string),
//                                        equipment_filter ,equipment_isChainLast ,"--");
                                String taskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
//                                planFileGRB.toFile(taskPlanFilePath);
                                PlanGeneratePlanFile_HMT_from_template(task_ra_string,task_dec_string, taskData.getTaskName(),equipment_filter,equipment_isChainLast,acpStatus.getPlanPath(),taskPlanFilePath);
                                log.info(taskPlanFilePath);
                            }
                            if(taskData.getTaskName().toUpperCase().startsWith("GW")){
                                String taskPlanFilePath=new File(equipment_PlanPath,windowsFileNameReplace(taskData.getTaskName())+".txt").getPath();
                                FileUtils.writeStringToFile(new File(taskPlanFilePath) ,taskData.getTask_plan_text(),"UtF-8");
                                log.info(taskPlanFilePath);
                            }
                            DeviceStatus deviceStatusE = new DeviceStatus("拒绝执行", device_name,device_name, "  " + "拒绝  " + taskData.getTarget_eqp() + "   " + taskData.getTaskName(), sdf.format(new Date()), device_color, MessageColor.Warning);
                            deviceStatus.setTaskName(taskData.getTaskName());
                            AmqMessageSender.sendMessage(jobDetail.getJobDataMap(), msg_name_equipment_status, JSONObject.fromObject(deviceStatusE).toString());
                            return;
                        }

//                        //如果必要 启动echo
//                        if(taskData.getTaskName().toUpperCase().startsWith("GRB")){
//                            GroupMatcher<JobKey> matcher = GroupMatcher.groupEquals("GRB_ECHO_GROUP");
//                            Set<JobKey> jobKeys = sched.getJobKeys(matcher);
////                            JobKey.jobKey("GRB_ECHO", "GRB_ECHO_GROUP"));
//
//
//                            if(jobKeys.size()<1){
//                                JobDetail grbEchoJob = JobBuilder.newJob(GRBEcho.class).storeDurably()
//                                        .withIdentity("GRB_ECHO", "GRB_ECHO_GROUP").build();
//                                grbEchoJob.getJobDataMap().put("SysConfig.msg_url",msg_url);
//                                grbEchoJob.getJobDataMap().put("device_name",device_name);
//                                grbEchoJob.getJobDataMap().put("device_color",device_color);
//                                grbEchoJob.getJobDataMap().put("equipment_User",equipment_User);
//                                grbEchoJob.getJobDataMap().put("equipment_Pass",equipment_Pass);
//                                grbEchoJob.getJobDataMap().put("equipment_Url",equipment_Url);
//                                grbEchoJob.getJobDataMap().put("GRB_task_data",taskData);
//                                Trigger grbEchoTrigger = TriggerBuilder.newTrigger().withIdentity("GRB_ECHO_Trigger", "GRB_ECHO_TriggerGroup")
//                                        .startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(15)
//                                                .withRepeatCount(30)).build();
//                                sched.scheduleJob(grbEchoJob, grbEchoTrigger);
//                            }
//                        }

//                        //这里暂时抛弃了级别 后面如果要引入级别 可以放到一个更容易保存的地方 比如 plannName GRB-L-10001  GRB-L-9999
//                        String task_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(((taskData.getTask_Ra_deg()%360)+360)%360));
//                        String task_dec_string=String.valueOf(taskData.getTask_Dec_deg());

                        if(!acpStatus.getAcpRunning()){
                            log.info("Acp.exe is not Running or Count error acp.exe进程有0个或者多个" );
                            return;
                        }

                        if("stop".equalsIgnoreCase(taskData.getTask_command())){
                            log.info("需要停止任务" + acpStatus.getPlanName());
                            while (true){
                                try{
                                    log.info("停止普通任务" + acpStatus.getPlanName());
                                    if(acpStatus.getScriptVERSION().startsWith("8")){
                                        log.info("acp version 8");
                                        AcpComControl.StopPlan();
                                    }else {
                                        log.info("acp version not 8");
                                        AcpComControl.StopPlanHack();
                                    }
                                    //防止出现abort失败提示框
                                    Thread.sleep(6000);
                                    acpStatus = AcpComControl.GetStatus(device_name);
                                    if(!acpStatus.getAcpRunning()){
                                        log.info("Acp.exe is not Running or Count error acp.exe进程有0个或者多个" );
                                        return;
                                    }
                                    if(!acpStatus.getAcpScriptActive()){
                                        log.info("已经停止任务");
                                        break;
                                    }

                                }
                                catch (Exception ex){
                                    ex.printStackTrace();
                                    log.info("停止时发生错误");
                                    log.info(ex.toString());
                                }
                            }
                        }


                        if(taskData.getTaskName().toUpperCase().startsWith("GRB")){
                            log.info("GRB任务");
                            //GRB 或者重要任务来了
                            if(acpStatus.getAcpScriptActive()){
                                String lastPlanPath = acpStatus.getPlanPath();
                                if(acpStatus.getPlanName().toUpperCase().startsWith("GRB") || acpStatus.getPlanName().toUpperCase().startsWith("GW")){
                                    log.info("已经有--任务在运行 " + acpStatus.getPlanName());
                                    AcpComControl.ConsoleLog("Last GRB Running "+ acpStatus.getScriptName());
                                }else {
                                    String chainPlanPath = acpStatus.getPlanPath();
                                    log.info("需要停止普通任务 name:" + acpStatus.getPlanName());
                                    log.info("需要停止普通任务 path:" + acpStatus.getPlanPath());

                                    //stop  and run GRB
                                    while (true){
                                        try{
                                            log.info("停止普通任务" + acpStatus.getPlanName());
                                            if(acpStatus.getScriptVERSION().startsWith("8")){
                                                log.info("acp version 8");
                                                AcpComControl.StopPlan();
                                            }else {
                                                log.info("acp version not 8");
                                                AcpComControl.StopPlanHack();
                                            }
                                            //防止出现abort失败提示框
                                            Thread.sleep(6000);
                                            acpStatus = AcpComControl.GetStatus(device_name);
                                            if(!acpStatus.getAcpRunning()){
                                                log.info("Acp.exe is not Running or Count error acp.exe进程有0个或者多个" );
                                                return;
                                            }
                                            if(!acpStatus.getAcpScriptActive()){
                                                log.info("已经停止普通任务");
                                                break;
                                            }

                                        }
                                        catch (Exception ex){
                                            ex.printStackTrace();
                                            log.info("停止时发生错误");
                                            log.info(ex.toString());
                                        }
                                    }
                                    log.info("执行GRB任务");
//                                    PlanFileContent planFileGRB;
//                                    planFileGRB = PlanGeneratePlanFile_HMT(Double.parseDouble(task_ra_string) , Double.parseDouble(task_dec_string),
//                                            equipment_filter ,equipment_isChainLast ,lastPlanPath);
                                    String taskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
//                                    planFileGRB.toFile(taskPlanFilePath);
                                    PlanGeneratePlanFile_HMT_from_template(task_ra_string,task_dec_string, taskData.getTaskName(),equipment_filter,equipment_isChainLast,chainPlanPath,taskPlanFilePath);
                                    AcpComControl.ConsoleLog(taskPlanFilePath);
                                    AcpComControl.RunPlan(taskPlanFilePath ,equipment_ScriptPath);
                                    log.info("GRB任务已经提交给ACP");
                                }
                            }else{
                                log.info("准备执行GRB任务");
                             //检查是否正常连接并直接运行
                                if(!acpStatus.getObsOffLine()){
//                                    PlanFileContent planFileGRB;
//                                    planFileGRB = PlanGeneratePlanFile_HMT(Double.parseDouble(task_ra_string) , Double.parseDouble(task_dec_string),
//                                            equipment_filter ,equipment_isChainLast ,"");
                                    String taskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
//                                    planFileGRB.toFile(taskPlanFilePath);
                                    PlanGeneratePlanFile_HMT_from_template(task_ra_string,task_dec_string, taskData.getTaskName(),equipment_filter,equipment_isChainLast,acpStatus.getPlanPath(),taskPlanFilePath);
                                    Unix4jCommandBuilder grbPlanFileSedBuilder = Unix4j.cat("GRB_Template_V02.txt").sed("s/task_ra/"+task_ra_string+"/g").sed("s/task_dec/"+task_dec_string+"/g").sed("s/task_name/"+taskData.getTaskName()+"/g");
                                    grbPlanFileSedBuilder.toFile(taskPlanFilePath);
                                    AcpComControl.ConsoleLog(taskPlanFilePath);
                                    AcpComControl.RunPlan(taskPlanFilePath ,equipment_ScriptPath);
                                    log.info("GRB任务已经提交给ACP");
                                }else{
                                    log.info("ACP 设备不在线");
                                }
                            }

                        }else if(taskData.getTaskName().toUpperCase().startsWith("GW")){

                            log.info("GW任务");
                            if(acpStatus.getAcpScriptActive()){
                                String lastPlanPath = acpStatus.getPlanPath();
                                if(acpStatus.getPlanName().toUpperCase().startsWith("GW") || acpStatus.getPlanName().toUpperCase().startsWith("GRB")){
                                    log.info("已经有任务在运行 " + acpStatus.getPlanName());
                                    AcpComControl.ConsoleLog("Last GW Running "+ acpStatus.getScriptName());
                                }else {
                                    log.info("需要停止普通任务" + acpStatus.getPlanName());
                                    while (true){
                                        try{
                                            log.info("停止普通任务" + acpStatus.getPlanName());
                                            if(acpStatus.getScriptVERSION().startsWith("8")){
                                                log.info("acp version 8");
                                                AcpComControl.StopPlan();
                                            }else {
                                                log.info("acp version not 8");
                                                AcpComControl.StopPlanHack();
                                            }
                                            //防止出现abort失败提示框
                                            Thread.sleep(6000);
                                            acpStatus = AcpComControl.GetStatus(device_name);
                                            if(!acpStatus.getAcpRunning()){
                                                log.info("Acp.exe is not Running or Count error acp.exe进程有0个或者多个" );
                                                return;
                                            }
                                            if(!acpStatus.getAcpScriptActive()){
                                                log.info("已经停止普通任务");
                                                break;
                                            }

                                        }
                                        catch (Exception ex){
                                            ex.printStackTrace();
                                            log.info("停止时发生错误");
                                            log.info(ex.toString());
                                        }
                                    }
                                    log.info("执行GW任务");
//                                    planFileGW = PlanGeneratePlanFile_HMT(Double.parseDouble(task_ra_string) , Double.parseDouble(task_dec_string),
//                                            equipment_filter ,equipment_isChainLast ,lastPlanPath);
                                    String taskPlanFilePath=new File(equipment_PlanPath,windowsFileNameReplace(taskData.getTaskName())+".txt").getPath();
                                    //todo add chain

//                                    FileUtils.writeStringToFile(new File(taskPlanFilePath) ,taskData.getTask_plan_text(),"UtF-8");
                                    FileUtils.writeStringToFile(new File(taskPlanFilePath) ,taskData.getTask_plan_text()+PlanFileContent.NewLine + "#CHAIN " +lastPlanPath,"UtF-8");
//                                    planFileGRB.toFile(taskPlanFilePath);
                                    AcpComControl.ConsoleLog(taskPlanFilePath);
                                    AcpComControl.RunPlan(taskPlanFilePath ,equipment_ScriptPath);
                                    log.info("GW任务已经提交给ACP");
                                }
                            }else{
                                log.info("准备执行GW任务");
                                //检查是否正常连接并直接运行
                                if(!acpStatus.getObsOffLine()){
//                                    PlanFileContent planFileGRB;
//                                    planFileGRB = PlanGeneratePlanFile_HMT(Double.parseDouble(task_ra_string) , Double.parseDouble(task_dec_string),
//                                            equipment_filter ,equipment_isChainLast ,"");
                                    String taskPlanFilePath=new File(equipment_PlanPath,windowsFileNameReplace(taskData.getTaskName())+".txt").getPath();
//                                    planFileGRB.toFile(taskPlanFilePath);
                                    FileUtils.writeStringToFile(new File(taskPlanFilePath) ,taskData.getTask_plan_text(),"UtF-8");
                                    AcpComControl.ConsoleLog(taskPlanFilePath);
                                    AcpComControl.RunPlan(taskPlanFilePath ,equipment_ScriptPath);
                                    log.info("GW任务已经提交给ACP");
                                }else{
                                    log.info("ACP 设备不在线");
                                }
                            }

                        }else {
                            log.info("普通任务");
                            //普通任务来了
                            if(acpStatus.getAcpScriptActive()){
                                log.info("忽略普通任务");
                                AcpComControl.ConsoleLog("Last Plan Running "+ acpStatus.getScriptName());
                            }else{
                                log.info("执行普通任务");
                                PlanFileContent planFile = new PlanFileContent();
                                List<TaskTarget> targets = taskData.getTask_targets();

                                for (TaskTarget targetItem: targets) {

//                                    NormalPlan plan_4_1=new NormalPlan(taskData.getTaskName() ,task_ra_string,task_dec_string);
                                    String target_ra_string=String.valueOf(Horizontal_To_Equatorial.degrees_to_hours(((targetItem.getTask_Ra_deg()%360)+360)%360));
                                    String target_dec_string=String.valueOf(targetItem.getTask_Dec_deg());
                                    NormalPlan plan_4_1=new NormalPlan(taskData.getTaskName() ,target_ra_string,target_dec_string);
                                    org.joda.time.format.DateTimeFormatter parserISO = ISODateTimeFormat.dateTimeNoMillis();
//                                    log.info(targetItem.getFilterBinningIntervalCount().getClass().toString());
                                    List<String> filterBinExpCount = targetItem.getFilterBinningIntervalCount();
                                    String param_filters[] = new String[filterBinExpCount.size()];
                                    int param_bins[] =new int[filterBinExpCount.size()];
                                    double param_exp[] =new double[filterBinExpCount.size()];
                                    int param_count[] =new int[filterBinExpCount.size()];
                                    for (int i_param =0 ;i_param<filterBinExpCount.size() ;i_param++) {
//                                        log.info(filterBinExpCount.get(i_param).getClass().toString());
                                        String[] filterItem = filterBinExpCount.get(i_param).split(",");
                                        param_filters[i_param] = filterItem[0];
                                        param_bins[i_param] = Integer.parseInt(filterItem[1]);
                                        param_exp[i_param] = Double.parseDouble(filterItem[2]);
                                        param_count[i_param] = Integer.parseInt(filterItem[3]);
                                    }

                                    if (param_filters.length>0 && param_filters[0].length()>0){
                                        plan_4_1.setSubSequenTar_FILTER(param_filters);
                                    }
                                    plan_4_1.setSubSequenTar_BINNING(param_bins);
                                    plan_4_1.setSubSequenTar_COUNT(param_count);
                                    plan_4_1.setSubSequenTar_INTERVAL(param_exp);
                                    plan_4_1.setNextTar_REPEAT(targetItem.getTask_repeat());

                                    plan_4_1.setSubSequenTar_DIR(acpFitsRoot);


                                    planFile.setPlan_sets(taskData.getTask_sets());
                                    planFile.getTargetList().add(plan_4_1);

                                }
                                String taskPlanFilePath=new File(equipment_PlanPath,taskData.getTaskName()+".txt").getPath();
                                planFile.toFile(taskPlanFilePath);
                                AcpComControl.RunPlan(taskPlanFilePath ,equipment_ScriptPath);
                            }
                        }


//                        //todo 判断任务级别 如果需要强制执行 需要执行 停止旧任务 直到obs avilable
//                        if (!AcpStatusUpdater.isReadyToNext(statusResultMap)) {
//                            sched.pauseAll();
//                            log.info(statusResultMap.get("taskIDFromTime") +"not ready");
//                            if(acp_target_name!=null && (!"".equals(acp_target_name))&& (!acp_target_name.equals("n/a" ))){
//                                log.info(statusResultMap.get("taskIDFromTime") +"检查正在执行的目标");
//                                if(Integer.parseInt(taskData.getTask_level()) > Integer.parseInt(taskData_running.getTask_level())) {
//                                    log.info(statusResultMap.get("taskIDFromTime") +"目标覆盖执行   删除update 操作");
//                                    sched.deleteJob(JobKey.jobKey("HMT_Deamon_Job", "EquipmentDeamonJob_JobGroup"));
//                                    log.info(statusResultMap.get("taskIDFromTime") +taskData_running.getTaskName() +"["+taskData_running.getTask_level()+"] < "+taskData.getTaskName() +"["+taskData.getTask_level()+"]");
//                                    taskData.setTask_status("init");
//                                    jobDetail.getJobDataMap().put("task_data", taskData);
////                                    sched.addJob(jobDetail, true);
//                                    sched.scheduleJob(jobDetail,trigger);
//                                    sched.resumeAll();
//                                    // todo 立刻执行
//                                   DeviceStatus deviceStatusL = new DeviceStatus("任务消息" , device_name ,"任务覆盖 "+taskData_running.getTaskName()+" < "+taskData.getTaskName() , sdf.format(new Date()),device_color , MessageColor.OK, "", "", "", "", "", "", "", span_ccd_status_, "", "",equipment_filter);
//                                   AmqMessageSender.sendMessage(initJob.getJobDataMap(), msg_name_equipment_status,JSONObject.fromObject(deviceStatusL).toString());
//                                    sched.resumeAll();
//                                    return;
//                                }else if(Integer.parseInt(taskData.getTask_level()) == Integer.parseInt(taskData_running.getTask_level())){
//                                    log.info(statusResultMap.get("taskIDFromTime") +taskData_running.getTaskName() +"["+taskData_running.getTask_level()+"] == "+taskData.getTaskName() +"["+taskData.getTask_level()+"]");
//                                    sched.resumeAll();
//                                    return;
//                                }else if(Integer.parseInt(taskData.getTask_level()) < Integer.parseInt(taskData_running.getTask_level())){
//                                    log.info(statusResultMap.get("taskIDFromTime") +taskData_running.getTaskName() +"["+taskData_running.getTask_level()+"] > "+taskData.getTaskName() +"["+taskData.getTask_level()+"]");
//                                    sched.resumeAll();
//                                    return;
//                                }
//
//                            }else{sched.resumeAll();}
//                            //in use GRB-开头的可以挤掉 其他的任务 命名规则 GRB-ID 只要不是同名task 就执行stop
////                            Normal 只要执行就一定会执行  如果是inuse  就stop 尝试 直到执行
////                            reciever 决定需要执行哪一个任务
//
//                            String errorMessage = "设备未准备好 :赤道仪["+span_scope_status_+"] 相机["+span_ccd_status_+"] 天文台["+s_sm_obsStat+"]";
//                            log.info(statusResultMap.get("taskIDFromTime") +errorMessage);
//                            DeviceStatus deviceStatusM = new DeviceStatus("忽略执行", device_name, "  " + "执行失败  "+ errorMessage + taskData.getTarget_eqp() + "   " + taskData.getTaskName(), sdf.format(new Date()), device_color, MessageColor.Warning);
//                            AmqMessageSender.sendMessage(jobDetail.getJobDataMap(), msg_name_equipment_status, JSONObject.fromObject(deviceStatusM).toString());
//                         }else {
//                            sched.pauseAll();
//                            log.info(statusResultMap.get("taskIDFromTime") +"准备好执行");
//                            taskData.setTask_status("init");
//                            jobDetail.getJobDataMap().put("task_data", taskData);
////                            sched.addJob(jobDetail, true);
////                            sched.triggerJob(JobKey.jobKey("HMT_Deamon_Job", "EquipmentDeamonJob_JobGroup"));
//                            sched.deleteJob(JobKey.jobKey("HMT_Deamon_Job", "EquipmentDeamonJob_JobGroup"));
//                            sched.scheduleJob(jobDetail,trigger);
//                            sched.resumeAll();
//                            // todo 立刻执行
//                         }
//                        if (next_task_name.equals("")) {
//                            log.debug("next_task_name =  \"\"");
//                            log.debug("taskData.getTask_level" + Integer.parseInt(taskData.getTask_level()));
//                            log.debug("pre_task_level" + Integer.parseInt(pre_task_level));
//                        	if (Integer.parseInt(taskData.getTask_level())>Integer.parseInt(pre_task_level)) {
////                        	Trigger trigger = sched.getTrigger(TriggerKey.triggerKey("HMT_Deamon_Trigger", "EquipmentDeamonTrigger_TriggerGroup"));
//                        		log.debug("cover task [remote task] [level] [running task] [level]"+taskData.getTaskName()+""+pre_task_name);
////                        		jobDetail.getJobDataMap().put("next_task_name", "GCN GRB");
////                        		jobDetail.getJobDataMap().put("next_task_level", "100");
//                        		jobDetail.getJobDataMap().put("next_task_name", taskData.getTaskName());
//                        		jobDetail.getJobDataMap().put("next_task_level", taskData.getTask_level());
//                        		sched.addJob(jobDetail, true);
//
//							}else if(Integer.parseInt(taskData.getTask_level())==Integer.parseInt(pre_task_level)){
//								log.debug("conflict task [remote task] [level] [running task] [level]"+taskData.getTaskName()+""+pre_task_name);
//							}else {
//								log.debug("ignore task [remote task] [level] [running task] [level]"+taskData.getTaskName()+""+pre_task_name);
//							}
//
//						}else {
//                            log.debug("next_task_name =  " + next_task_name);
//                            log.debug("next_task_level " + Integer.parseInt(next_task_level));
//                            log.debug("pre_task_level " + Integer.parseInt(pre_task_level));
//							if(Integer.parseInt(next_task_level)>Integer.parseInt(pre_task_level)){
//								log.debug("cover task [remote task] [level] [next run task] [level] "+taskData.getTaskName()+""+next_task_name);
////                        		jobDetail.getJobDataMap().put("next_task_name", "GCN GRB");
////                        		jobDetail.getJobDataMap().put("next_task_level", "100");
//                                jobDetail.getJobDataMap().put("next_task_name", taskData.getTaskName());
//                                jobDetail.getJobDataMap().put("next_task_level", taskData.getTask_level());
//                        		sched.addJob(jobDetail, true);
//							}else if(Integer.parseInt(next_task_level)==Integer.parseInt(pre_task_level)){
//								log.debug("conflict task [remote task] [level] [next run task] [level] "+taskData.getTaskName()+""+next_task_name);
//							}else{
//								log.debug("ignore task [remote task] [level] [next run task] [level] "+taskData.getTaskName()+""+next_task_name);
//							}
//						}
                        

//                    } catch (JMSException e) {
//                        e.printStackTrace();
                    } catch (SchedulerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }
        });
		
	}

    String STR_FORMAT = "00";
    DecimalFormat fileNameNumberFormat = new DecimalFormat(STR_FORMAT);
    PlanFileContent PlanGeneratePlanFile_HMT(Double ra_ha , Double dec_deg , String equipment_filter , boolean is_chain_stoped_plan , String HMTlastPlanFilePath){

        PlanFileContent planFile=new PlanFileContent();


//        todo 这部分对GRB的滤镜处理需要重写
        NormalPlan plan_4_1=new NormalPlan("GRB-40S-01"+"-time",ra_ha,dec_deg);
        plan_4_1.setSubSequenTar_DIR("test");
        plan_4_1.setSubSequenTar_BINNING(new int []{1});
        plan_4_1.setSubSequenTar_INTERVAL(new double[]{40});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_4_1.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_4_1.setSubSequenTar_COUNT(new int []{1});
        }
        NormalPlan plan_4_2=new NormalPlan("GRB-40S-02"+"-time",ra_ha,dec_deg);
        plan_4_2.setSubSequenTar_BINNING(new int []{1});
        plan_4_2.setSubSequenTar_INTERVAL(new double[]{40});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_4_2.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_4_2.setSubSequenTar_COUNT(new int []{1});
        }
        NormalPlan plan_4_3=new NormalPlan("GRB-40S-03"+"-time",ra_ha,dec_deg);
        plan_4_3.setSubSequenTar_BINNING(new int []{1});
        plan_4_3.setSubSequenTar_INTERVAL(new double[]{40});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_4_3.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_4_3.setSubSequenTar_COUNT(new int []{1});
        }

        NormalPlan plan_6_1=new NormalPlan("GRB-60S-01"+"-time",ra_ha,dec_deg);
        plan_6_1.setSubSequenTar_INTERVAL(new double[]{60});
        plan_6_1.setSubSequenTar_BINNING(new int []{1});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_6_1.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_6_1.setSubSequenTar_COUNT(new int []{1});
        }
        NormalPlan plan_6_2=new NormalPlan("GRB-60S-02"+"-time",ra_ha,dec_deg);
        plan_6_2.setSubSequenTar_BINNING(new int []{1});
        plan_6_2.setSubSequenTar_INTERVAL(new double[]{60});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_6_2.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_6_2.setSubSequenTar_COUNT(new int []{1});
        }
        NormalPlan plan_6_3=new NormalPlan("GRB-60S-03"+"-time",ra_ha,dec_deg);
        plan_6_3.setSubSequenTar_INTERVAL(new double[]{60});
        plan_6_3.setSubSequenTar_BINNING(new int []{1});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_6_3.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_6_3.setSubSequenTar_COUNT(new int []{1});
        }
        NormalPlan plan_6_4=new NormalPlan("GRB-60S-04"+"-time",ra_ha,dec_deg);
        plan_6_4.setSubSequenTar_INTERVAL(new double[]{60});
        plan_6_4.setSubSequenTar_BINNING(new int []{1});
        if (null != equipment_filter && equipment_filter.length() > 0){
            plan_6_4.setSubSequenTar_FILTER(new String []{equipment_filter});
            plan_6_4.setSubSequenTar_COUNT(new int []{1});
        }

        planFile.getTargetList().add(plan_4_1);
        planFile.getTargetList().add(plan_4_2);
        planFile.getTargetList().add(plan_4_3);
        planFile.getTargetList().add(plan_6_1);
        planFile.getTargetList().add(plan_6_2);
        planFile.getTargetList().add(plan_6_3);
        planFile.getTargetList().add(plan_6_4);

        for(int i=0;i<12;i++){
            NormalPlan plan_9s=new NormalPlan("GRB-90S-"+fileNameNumberFormat.format(i+1)+"-time",ra_ha,dec_deg);
            plan_9s.setSubSequenTar_INTERVAL(new double[]{90});
            plan_9s.setSubSequenTar_BINNING(new int []{1});
            if (null != equipment_filter && equipment_filter.length() > 0){
                plan_9s.setSubSequenTar_FILTER(new String []{equipment_filter});
                plan_9s.setSubSequenTar_COUNT(new int []{1});
            }

            planFile.getTargetList().add(plan_9s);
        }

        File stopedPlan=new File(HMTlastPlanFilePath);
        if (HMTlastPlanFilePath!=null&&HMTlastPlanFilePath.length()>0&&stopedPlan.exists()&&stopedPlan.isFile() && is_chain_stoped_plan) {
            planFile.setPaln_Chian(HMTlastPlanFilePath);
            log.info("#chain : last stoped plan file ="+HMTlastPlanFilePath);
        }
        return  planFile;
    }


    void PlanGeneratePlanFile_HMT_from_template(String task_ra_string , String task_dec_string , String task_name, String equipment_filter , boolean is_chain_stoped_plan , String HMTlastPlanFilePath,String writeToFilePath){
        Unix4jCommandBuilder grbPlanFileSedBuilder;
        File stopedPlan=new File(HMTlastPlanFilePath);
        HMTlastPlanFilePath = HMTlastPlanFilePath.replace("\\","/").replace("/","\\/");
        log.info("---length:"+HMTlastPlanFilePath);
        log.info("---length:"+HMTlastPlanFilePath.length());
        log.info("---file exists:"+stopedPlan.exists());
        log.info("---is file:"+stopedPlan.isFile());
        log.info("---is chain mark:"+is_chain_stoped_plan);
        if (HMTlastPlanFilePath.length() > 0 && stopedPlan.exists() && stopedPlan.isFile() && is_chain_stoped_plan) {
            grbPlanFileSedBuilder = Unix4j.cat("GRB_Template_V02_chain.txt").sed("s/task_ra/"+task_ra_string+"/g").sed("s/task_dec/"+task_dec_string+"/g").sed("s/task_name/"+task_name+"/g").sed("s/chain_path/"+HMTlastPlanFilePath+"/g");
            log.info("#chain : last stoped plan file ="+HMTlastPlanFilePath);
        }else{
            grbPlanFileSedBuilder = Unix4j.cat("GRB_Template_V02.txt").sed("s/task_ra/"+task_ra_string+"/g").sed("s/task_dec/"+task_dec_string+"/g").sed("s/task_name/"+task_name+"/g").sed("s/chain_path/"+HMTlastPlanFilePath+"/g");
            log.info("# skip chain : last stoped plan file ="+HMTlastPlanFilePath);
        }
        grbPlanFileSedBuilder.toFile(writeToFilePath);
    }







    String windowsFileNameReplace(String windowsFleNameSrc){
        return windowsFleNameSrc.replace(":","").replace("\\","").replace("/","")
                .replace("*","").replace("?","").replace("\"","")
                .replace(">","").replace("<","").replace("|","");
    }


}
