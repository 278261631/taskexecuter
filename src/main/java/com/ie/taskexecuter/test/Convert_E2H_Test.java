package com.ie.taskexecuter.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.ie.taskexecuter.util.Equatorial_To_Horizontal;
import com.ie.taskexecuter.util.dms;
import com.ie.taskexecuter.util.hmsm;

public class Convert_E2H_Test {

	
	public static void main(String[] args) {
		hmsm hmsm_ra=new hmsm(5,35,17,0);
		dms dms_dec=new  dms(-5,23,28);
		Double lat=43.4710372,longt=87.1775888;

	  	
//        String dateStr = "2017-05-07 12:00:00 ";    
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    
//        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));    
//        Date dateTmp =null ;
//        try {    
//        	dateTmp = dateFormat.parse(dateStr);    
//            System.out.println(dateTmp);    
//         } catch (ParseException e) {    
//            e.printStackTrace();    
//        }    
        
        Calendar calendar=new GregorianCalendar();
//        calendar.setTime(dateTmp);
	  	System.out.println(calendar.getTime());
//	  	System.out.println(calendar.getTimeZone());
		double[] altAz= Equatorial_To_Horizontal.Convert_E2H(hmsm_ra, dms_dec,lat,longt,calendar);
		System.out.println(Math.toDegrees(altAz[0]));
		System.out.println(Math.toDegrees(altAz[1]));
		
	}

}
