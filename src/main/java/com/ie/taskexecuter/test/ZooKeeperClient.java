package com.ie.taskexecuter.test;

import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import net.sf.json.JSONObject;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;

public class ZooKeeperClient {
    public  static void main(String args[]) throws Exception {


        String zookeeperUrl = "remo.xtelescope.net:2181";
        CuratorFramework curtatorClient = CuratorFrameworkFactory.newClient(zookeeperUrl, new RetryNTimes(10, 5000));
        curtatorClient.start();
//        String rootPath = "/task/"+"abc"+"/status";
        String rootPath = "/task/abcd";
        Stat taskExists = curtatorClient.checkExists().forPath(rootPath);
        if(null != taskExists){
            System.out.println("abcd" + "    exits " + taskExists);
            System.out.println(new String (curtatorClient.getData().forPath(rootPath)));
        }else {
//            String result = curtatorClient.create().withMode(CreateMode.PERSISTENT).withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE).forPath("/task", "abc".getBytes());
            String result = curtatorClient.create().withMode(CreateMode.PERSISTENT).withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE).forPath(rootPath, "abcd".getBytes());
            System.out.println(result);
//            curtatorClient.setData().forPath(rootPath, "status".getBytes());
        }
    }
}
