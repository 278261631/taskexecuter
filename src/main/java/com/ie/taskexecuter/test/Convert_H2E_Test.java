package com.ie.taskexecuter.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.ie.taskexecuter.util.Equatorial_To_Horizontal;
import com.ie.taskexecuter.util.Horizontal_To_Equatorial;
import com.ie.taskexecuter.util.dms;
import com.ie.taskexecuter.util.hmsm;

public class Convert_H2E_Test {

	
	public static void main(String[] args) {
		Double lat=43.4710372,longt=87.1775888;

      String dateStr = "2017-05-07 12:00:00 ";    
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    
      dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));    
      Date dateTmp =null ;
      try {    
      	dateTmp = dateFormat.parse(dateStr);    
          System.out.println(dateTmp);    
       } catch (ParseException e) {    
          e.printStackTrace();    
      }   
		
		Calendar calendar=new GregorianCalendar();
		calendar.setTime(dateTmp);
	  	double [] radec_e=Horizontal_To_Equatorial.Convert_H2E(40, 175, lat, longt, calendar);
	  	double [] radec_w=Horizontal_To_Equatorial.Convert_H2E(40, 185, lat, longt, calendar);	
	  	hmsm hmsm_ra=new hmsm(radec_e[0]);
	  	dms dms_dec=new  dms(radec_e[1]);
	  	double[] altAz_e =Equatorial_To_Horizontal.Convert_E2H(hmsm_ra, dms_dec, lat, longt, calendar);
	  	System.out.println("alt   "+Math.toDegrees(altAz_e[0]));
	  	System.out.println("az   "+Math.toDegrees(altAz_e[1]));
	  	
		hmsm_ra=new hmsm(radec_w[0]);
	  	dms_dec=new  dms(radec_w[1]);
	  	double[] altAz_w =Equatorial_To_Horizontal.Convert_E2H(hmsm_ra, dms_dec, lat, longt, calendar);
	  	System.out.println("alt   "+Math.toDegrees(altAz_w[0]));
	  	System.out.println("az   "+Math.toDegrees(altAz_w[1]));
	}

}
