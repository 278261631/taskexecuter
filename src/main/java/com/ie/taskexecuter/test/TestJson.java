package com.ie.taskexecuter.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.ie.sysmessage.TaskData;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class TestJson {

	public static void main(String[] args) {
		SimpleDateFormat longDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		String stringRa="2.1";
		String stringDec="1.2";
		TaskData task=new TaskData("HMT_"+longDateFormat.format(new Date()), Double.parseDouble(stringRa), Double.parseDouble(stringDec), "1000");
		JSONObject jsonObj=JSONObject.fromObject(task);
		
		String jsonString =jsonObj.toString();
		System.out.println(jsonString);
		
		JSONObject taskObj = JSONObject.fromObject(jsonString);
		TaskData taskConvert=(TaskData) JSONObject.toBean(taskObj, TaskData.class);
		
		 
		System.out.println(taskConvert);
		System.out.println(taskConvert.getTask_Dec_deg());
		System.out.println(JSONObject.fromObject(taskConvert).toString());
		
		
	}
}
