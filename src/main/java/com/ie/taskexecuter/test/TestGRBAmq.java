package com.ie.taskexecuter.test;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2017/10/6.
 */
public  class TestGRBAmq {
    public static void main(String args[]){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String testTaskName=sdf.format(new Date());
//        String GRBMessage="{\"task_Dec_deg\":51.1236,\"task_Ra_deg\":3.0954,\"task_status\":\"\",\"filterBinningIntervalCount\":[],\"task_command\":\"\",\"task_sets\":0,\"task_start_time\":\"\",\"task_plan_text\":\"\",\"target_eqp\":\"GCN\",\"task_targets\":[],\"task_end_time\":\"\",\"taskName\":\"GRB_2020-02-01_06-41-39-901"+testTaskName+"\",\"task_type\":\"\",\"task_level\":\"1000\"}";
//        String GRBMessage="{\"task_Dec_deg\":51.1236,\"task_Ra_deg\":3.0954,\"task_status\":\"\",\"filterBinningIntervalCount\":[],\"task_command\":\"\",\"task_sets\":0,\"task_start_time\":\"\",\"task_plan_text\":\"\",\"target_eqp\":\"HMT\",\"task_targets\":[],\"task_end_time\":\"\",\"taskName\":\"GRB_2020-02-01_06-41-39-901"+testTaskName+"\",\"task_type\":\"\",\"task_level\":\"1000\"}";
        String GRBMessage="{\"task_Dec_deg\":51.1236,\"task_Ra_deg\":3.0954,\"task_status\":\"\",\"filterBinningIntervalCount\":[],\"task_command\":\"\",\"task_sets\":0,\"task_start_time\":\"\",\"task_plan_text\":\"\",\"target_eqp\":\"TEST_HMT\",\"task_targets\":[],\"task_end_time\":\"\",\"taskName\":\"GRB_2020-02-01_06-41-39-901"+testTaskName+"\",\"task_type\":\"\",\"task_level\":\"1000\"}";
//        AmqTestSender.sendMessage("tcp://127.0.0.1:61616","GRB_TASK",GRBMessage);
//        AmqTestSender.sendMessage("tcp://msg.xtelescope.net:61616","GRB_TASK",GRBMessage);
        AmqTestSender.sendMessage("tcp://msg.xastro.site:61616","GRB_TASK",GRBMessage);
    }
}
