package com.ie.taskexecuter.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ie.sysmessage.TaskData;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.unix4j.Unix4j;
import org.unix4j.builder.Unix4jCommandBuilder;
import org.unix4j.unix.Sed;

public class TestRegex {

	public static void main(String[] args) throws IOException {
		SimpleDateFormat longDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
//        String resultString = "@xoiewr12.8123?x";
//        String resultString = "@xoiewr12..8123?x";
//        String resultString = "@xoiewr#$^%&*(?x";
//        String resultString = "@xoiewr#$^%&*(?x";
		String resultString = "@xoiewr12..81s23?x";

		String regexp_double = "[\\d\\.]+";
        Pattern pattern_double = Pattern.compile(regexp_double); 
		Matcher matcher_double = pattern_double.matcher(resultString);


		String filename	="C:\\Users\\mars\\Documents\\ACP Astronomy\\Plans\\S191222n_next_20191222_114119.txt";
		Path path = Paths.get(filename);
		String read = Files.readAllLines(path).get(1);

//		String acpProcessRegex = "[completionstate]\\.*[Plan completion status added by ACP]";
		String acpProcessRegex = "^#completionstate (.*) ; Plan completion status added by ACP$";
		Pattern acpPattern = Pattern.compile(acpProcessRegex);
		Matcher acpMatcher = acpPattern.matcher(read);



        if (acpMatcher.find()) {
			System.out.println("---");
			String r_string = acpMatcher.group(1);
			System.out.println(r_string);
			System.out.println(acpMatcher.toString());
			System.out.println(acpMatcher.start());
			System.out.println(acpMatcher.end());
		}

        if (matcher_double.find()) {
			System.out.println("---");
			String r_string = matcher_double.group(0);
			System.out.println(r_string);
		}


		Unix4j.fromString("hello world").grep("hello").wc().toStdOut();
//		String result = Unix4j.fromString("hello world").grep("hello").wc().toStringResult();
		Unix4j.cat("planTest.txt").sed("s/task_ra/123/g").sed("s/task_dec/46.6/g").sed("s/task_name/GRB---/g").toStdOut();
//		Unix4j.cat("planTest.txt").sed("s/task_ra/123/g").sed("s/task_dec/46.6/g").sed("s/task_name/GRB---/g").toFile("xxx.txt");
		Unix4jCommandBuilder grbSedBuilder = Unix4j.cat("planTest.txt").sed("s/task_ra/123/g").sed("s/task_dec/46.6/g");
		grbSedBuilder.sed("s/task_name/GRB---/g");

		grbSedBuilder.toFile("xxx.txt");
		
	}
}
