package com.ie.taskexecuter.test;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.configuration2.AbstractConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;

import javax.jms.*;
import java.io.File;

/**
 * Created by mars on 2017/4/28.
 */
public class AmqTest {

//    private static String AMQUrl ;
//    private static String amqName ;
//    private static String amqLoginPass ;
//    private static String amqLoginUser  ;
    static ConnectionFactory connectionFactory;
    static Connection connection = null;
    static Session session;
    static Destination destination;
    static MessageConsumer consumer;

    public  static void main(String a[]) throws JMSException {
        Configurations configs = new Configurations();
        Configuration config = null;
        try {
            config = configs.properties(new File("sys_config.properties"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        ((AbstractConfiguration) config).setListDelimiterHandler(new DefaultListDelimiterHandler(','));
        String msg_url = config.getString("msg_url","tcp://msg.xtelescope.net:61616");
//        String msg_url = config.getString("msg_url","tcp://127.0.0.1:61616");
        String msg_name = config.getString("msg_name","loginTopic,equipment_status,destTest");
        String encrypt_key = config.getString("encrypt_key","");
//        log.debug("msg_url : "  + msg_url);
//        log.debug("msg_name : "+msg_name);
//        log.debug("encrypt_key : "+encrypt_key);

//        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, "failover://("+AMQUrl+")?randomize=false");
        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER, ActiveMQConnection.DEFAULT_PASSWORD, msg_url);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
        destination = session.createTopic(msg_name);

        consumer = session.createConsumer(destination);

        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {

                try {
                    Topic msgTopic = (Topic) message.getJMSDestination();
                    System.out.println("--------接收到消息-------       "+msgTopic.getTopicName()+ message);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
//            System.out.println("--------接收到消息-------       "+ message);
            ;
                if (message instanceof TextMessage ) {
                    try {
                        String textMessage=((TextMessage) message).getText();

//                        System.out.println(textMessage);
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }


                }
            }
        });
    }


//
//    public void runReceiver() {
//
//        try {
//            while (true) {
////	                Message message =  consumer.receive(1000);
//                Message message =  consumer.receive();
//
//                if (null != message) {
//
//                    if (message instanceof TextMessage ) {
//                        Logger.getLogger("").debug("--------接收到消息-------"+ message);
//                        String textMessage=((TextMessage) message).getText();
//                        LogManager.getLogger().debug(textMessage);
//                        String filePath="C:/planb.txt";
//                        if (!filePath.endsWith(".txt")) {
//                            filePath+=".txt";
//                        }
//                        File file = new File(filePath);
//                        FileUtils.writeStringToFile(file, textMessage, Charset.forName("UTF-8"), false);
//
//                        LogManager.getLogger().debug(file.getName()+"-"+file.getPath());
//                        Logger.getLogger("").debug("--------执行-------" + filePath );
//                        AcpControl.runPlan(filePath,acpUrl,acpUser,acpPass);
//                    }
//                } else {
////	                    break;
////	                    System.out.println(".");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                if (null != connection) connection.close();
//            } catch (Throwable ignore) {
//            }
//        }
//    }

}
