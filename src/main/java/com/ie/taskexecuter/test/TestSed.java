package com.ie.taskexecuter.test;

import com.ie.taskexecuter.TaskReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unix4j.Unix4j;
import org.unix4j.builder.Unix4jCommandBuilder;

import java.io.File;
import java.text.SimpleDateFormat;

public class TestSed {
    public static void main(String args[]){

        String HMTlastPlanFilePath = "C:\\/Users\\/Administrator\\/Documents\\/ACP Astronomy\\/Plans\\/1.txt";
        String writeToFilePath = "C:/Users/Administrator/Documents/ACP Astronomy/Plans/1xxx.txt";

        Logger log = LoggerFactory.getLogger(TaskReceiver.class);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH mm ss SSS");
        Unix4jCommandBuilder grbPlanFileSedBuilder;
        File stopedPlan=new File(HMTlastPlanFilePath);
        log.info("---length:"+HMTlastPlanFilePath);
        log.info("---length:"+HMTlastPlanFilePath.length());
        log.info("---file exists:"+stopedPlan.exists());
        log.info("---is file:"+stopedPlan.isFile());

        String task_ra_string = "raaa";
        String task_dec_string = "dedcc";
        String task_name = "nnnn";

            grbPlanFileSedBuilder = Unix4j.cat("GRB_Template_V02_chain.txt").sed("s/task_ra/"+task_ra_string+"/g")
                    .sed("s/task_dec/"+task_dec_string+"/g").sed("s/task_name/"+task_name+"/g")
                    .sed("s/chain_path/"+HMTlastPlanFilePath+"/g");
            log.info("#chain : last stoped plan file ="+HMTlastPlanFilePath);

        grbPlanFileSedBuilder.toFile(writeToFilePath);
    }
}
