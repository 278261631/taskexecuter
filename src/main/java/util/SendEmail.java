package util;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.mail.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendEmail {

	public static void main(String[] args) {

		
	}
	public static SimpleDateFormat longDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
	public static boolean sendToEmails(String subjectString, String[] emails, String contentString) {
		Email email = new SimpleEmail();
		email.setCharset("UTF-8");
		email.setHostName("smtp.163.com");
		email.setSmtpPort(465);
		//密码是pai(10)a
		email.setAuthenticator(new DefaultAuthenticator("c42email", "3141592654a")); //这里是163的客户端登录授权码 不是登录163邮箱的密码
		email.setSSLOnConnect(true);
		try {
			email.setFrom("c42email@163.com");
			email.setSubject(subjectString);
			email.setMsg(contentString);
			if (emails==null||emails.length<1) {
				email.addTo("278261631@qq.com");
				email.setMsg(" : email 地址为空"+emails);
			}else {				
				email.addTo(emails);
			}
			
			System.out.println(longDateFormat.format(new Date())+"   sending email :"+email.getToAddresses()+"\t"+email.getSubject());
			email.send();
			
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	public static boolean sendToHtmlEmails(String subjectString, String[] emails, String contentString) {
		HtmlEmail email = new HtmlEmail();
		email.setCharset("UTF-8");
		email.setHostName("smtp.163.com");
		email.setSmtpPort(465);
		//密码是pai(10)a
		email.setAuthenticator(new DefaultAuthenticator("c42email", "3141592654a")); //这里是163的客户端登录授权码 不是登录163邮箱的密码
		email.setSSLOnConnect(true);
		try {
			email.setFrom("c42email@163.com");
			email.setSubject(subjectString);
			email.setHtmlMsg(contentString);
			if (emails==null||emails.length<1) {
				email.addTo("278261631@qq.com");
				email.setMsg("C42 : email 地址为空"+emails);
			}else {				
				email.addTo(emails);
			}

			
			System.out.println(longDateFormat.format(new Date())+"   sending email :"+email.getToAddresses()+"\t"+email.getSubject());
			email.send();
			
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	public static String emailCheck(String[] emails){
		String msg="";
		String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";  
		 Pattern regex = Pattern.compile(check);  
		 for (String emailString : emails) {
			 Matcher matcher = regex.matcher(emailString);  
			 boolean isMatched = matcher.matches();  
			 System.out.println(isMatched+"\t\t"+emailString);  
			if (!isMatched) {
				msg+="\t"+emailString+"\t"+"Email地址不对";
			}
		}
		 return msg;
	}
}
